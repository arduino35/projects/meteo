#Script de compilation du projet meteo

source tools/build_tools/config/build_conf.sh

ok=1
nogen=0
clean=0

echo 

# Parameter analysis
if [ $# -ge 3 ]
then
	echo "build.sh : wrong number of parameters"
	echo "Use : build.sh [-nogen] [-clean]"
	exit 1
fi
	
if [ $# -ge 1 ]
then
	if [ $1 = "-nogen" ] || [ $1 = "-clean" ]
	then
		if [ $1 = "-nogen" ]
		then
			echo "Building executable without BITE code generation"
			echo
			nogen=1
		elif [ $1 = "-clean" ]
		then
			clean=1
		fi
	else
		echo "build.sh : wrong script parameters"
		echo "Use : build.sh [-nogen] [-clean]"
		exit 1
	fi

	if [ $# -ge 2 ]
	then
		if [ $2 = "-nogen" ] || [ $2 = "-clean" ]
		then
			if [ $2 = "-nogen" ]
			then
				echo "Building executable without BITE code generation"
				echo
				nogen=1
			elif [ $2 = "-clean" ]
			then
				clean=1
			fi
		else
			echo "build.sh : wrong script parameters"
			echo "Use : build.sh [-nogen] [-clean]"
			exit 1
		fi
	fi
fi
	
	
	

# BITE code generation
if [ $nogen -eq 0 ]
then
	java -jar ../arduinoLinkInterface/exe/arduinoLinkInterface.jar -genfile $xmlfile
	
	echo 
	echo
		
	if [ $? -ne 0 ]
	then
		ok=0
		echo "Errors occured during code generation"
	else
		cp ../arduinoLinkInterface/exe/generated/* src/asw/bite_manager/config
	fi
fi




# Build with make
if [ $ok -eq 1 ]
then
	cd Release

	# Clean before build
	if [ $clean -eq 1 ]
	then
		echo
		echo Cleaning...
		echo
		make clean > /dev/null
	fi

	make -j16 all
fi
