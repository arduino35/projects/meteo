/*!
 *  @file main.cpp
 *  @brief Background task file
 *
 *  @date Tue Apr  5 20:18:22     2022
 *  @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:22     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Apr  5 12:06:06     2022 : 63-rendre-generique-le-formatage-des-donnees-periodiques 
 * Tue Feb 22 14:48:46     2022 : 59-passage-en-allocation-statique 
 * Tue Jan 25 11:39:25     2022 : 57-corrections-bugs-bite 
 * Fri Jan 21 11:28:37     2022 : 54-correction-plantage-quand-bite-actif 
 * Fri Jan  7 20:47:58     2022 : 51-optimisation-des-typages 
 * Wed Jan  5 17:01:33     2022 : 50-corrections-sonar 
 * 
 */

#include <stdlib.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "main.h"

#include "os/service/generic/PeriodicService.h"
#include "bsw/bsw_manager/generic/bsw_manager.h"
#include "asw/asw_manager/generic/asw_manager.h"

#include "os/scheduler/generic/Scheduler.h"


const T_ASW_manager_service_list ASW_services_init_list[] =
{
 BITE, /* BITE */
DATE_TIME, /* DATE */
SENSORS, /* Sensor management */
        LCD_DISPLAY, /* Display */
LED, /* LED */
};



/*!
 * @brief Background task of program
 * @details This function initializes all the software and then goes into an infinite loop.
 *          Periodic interrupt will wake up the software to perform application
 */
int main( void )
{
	bool status;

	/* Initialize BSW */
	status = p_global_BSW_manager.initBSW();

	/* Initialize scheduler */
	if (status)
		status = p_global_scheduler.initScheduler();

	/* Initialize ASW */
	if (status)
		status = p_global_ASW_manager.initASW();
	if (status)
		status = p_global_ASW_manager.initNewServicesList(ASW_services_init_list,
		        sizeof(ASW_services_init_list) / sizeof(T_ASW_manager_service_list));

	/* Configure and start main timer for periodic interrupt at 500ms */
	if (status)
	{
		/* Enable interrupts */
		sei();

		/* BITE activation */
		if (p_global_ASW_manager.isServiceInitialized(BITE))
			((BiteManager*) (p_global_ASW_manager.getServicePointer(BITE)))->start();

		p_global_scheduler.startScheduling();
	}

	/* Go into an infinite loop */
	while (true)
	{
	}
}

