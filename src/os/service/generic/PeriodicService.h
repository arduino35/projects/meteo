/*!
 * @file PeriodicService.h
 *
 * @brief 
 *
 * @date Fri Apr 29 11:34:44     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Apr 29 11:34:44     2022 : 60-modification-gestion-periode-des-services 
 * Tue Apr  5 20:18:23     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Feb  8 17:22:55     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 20:57:35     2022 : 51-optimisation-des-typages 
 * Thu Dec 23 16:40:01     2021 : 50-corrections-sonar 
 * 
 */

#ifndef SRC_ASW_SERVICE_GENERIC_PERIODICSERVICE_H_
#define SRC_ASW_SERVICE_GENERIC_PERIODICSERVICE_H_

#include "../../../lib/common_lib/generic/common_types.h"
#include "Service.h"

/*!
 * @brief This class defines a periodic service
 * @details It is a service that implements a periodic task which is added into the scheduler.
 *          The task can be paused and stopped on demand.
 *          The class is virtual, then it is not
 * 			possible to instantiate a PeriodicService object. This class only implements
 * 			the common functions to all periodic services.
 */
class PeriodicService : public Service
{
public:
	/*!
	 * @brief Class constructor
	 * @details This function initializes a basic periodic service. and adds it into the scheduler.
	 *
	 * @param [in] a_period Period of the scheduled task
	 * @return Nothing
	 */
	PeriodicService(t_time_ms a_period);

	/*!
	 * @brief Initialization function
	 * @details This function initializes a basic periodic service.
	 *
	 * @param [in] a_period Period of the scheduled task
	 * @return Always TRUE
	 */
	bool initPeriodicService(t_time_ms a_period);

	/*!
	 * @brief Service initialization function
	 * @details This function initializes the class. It must be called before any use of the class.
	 *
	 * @return TRUE if the initialization has been performed without errors, FALSE otherwise
	 */
	virtual bool initService();

	/*!
	 * @brief Service pausing function
	 * @details This function is used to pause the service.
	 *
	 * @return True is the service has been correctly paused, false otherwise.
	 */
	bool pause();

	/*!
	 * @brief Service start function
	 * @details This function is used to start the service after it has been paused or created.
	 *
	 * @return True is the service has been correctly started, false otherwise.
	 */
	bool start();

	/*!
	 * @brief Service running flag get function
	 * @details This function returns the running status flag of the service.
	 *
	 * @return Service running status
	 */
	inline bool isServiceRunning() const
	{
		return running;
	}

	/*!
	 * @brief Periodic function called by the scheduler
	 * @details This function must be implemented by all child classes.
	 * 			It is called periodically by the scheduler, according to the period given at initialization.
	 *
	 * @return Nothing
	 *
	 */
	virtual void run() = 0;

	/*!
	 * @brief Service class destructor
	 * @details This function deletes the class from memory.
	 * 			It is defined as virtual, each service shall have its own implementation.
	 *
	 * @return Nothing
	 */
	virtual ~PeriodicService();

	/*!
	 * @brief Service period get function
	 *
	 * @return Service period
	 */
	t_time_ms getPeriod() const
	{
		return period;
	}



protected:

	/*!
	 * @brief Service running status setting function
	 *
	 * @return Nothing
	 */
	void inline setServiceRunning(bool status)
	{
		running = status;
	}

	/*!
	 * @brief Service period setting function
	 *
	 * @param period
	 */
	void setPeriod(t_time_ms l_period)
	{
		this->period = l_period;
	}

private:

	bool running; /*!< Service running flag */
	t_time_ms period; /*!< Period of the scheduled task */
};

#endif /* SRC_ASW_SERVICE_GENERIC_PERIODICSERVICE_H_ */
