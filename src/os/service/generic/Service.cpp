/*!
 * @file Service.cpp
 *
 * @brief Service class source code file
 *
 * @date Tue Apr  5 21:17:21     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 21:17:21     2022 : 63-rendre-generique-le-formatage-des-donnees-periodiques 
 * Tue Feb  8 16:02:26     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 17:01:34     2022 : 50-corrections-sonar 
 * 
 */

#include <avr/io.h>
#include "Service.h"

Service::Service()
{

}

Service::~Service()
{

}

bool Service::initService()
{
	return false;
}

extern "C" void __cxa_pure_virtual()
{
	while (true);
}
