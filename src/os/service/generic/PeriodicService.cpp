/*!
 * @file PeriodicService.cpp
 *
 * @brief PeriodicService class source code file
 *
 * @date Fri Apr 29 11:34:44     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Apr 29 11:34:44     2022 : 60-modification-gestion-periode-des-services 
 * Tue Apr  5 20:18:23     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Feb  8 17:22:55     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 20:57:35     2022 : 51-optimisation-des-typages 
 * Thu Dec 23 16:40:00     2021 : 50-corrections-sonar 
 * 
 */

#include <stdlib.h>
#include <avr/io.h>

#include "../../../lib/common_lib/generic/common_types.h"

#include "../../service/generic/Service.h"
#include "PeriodicService.h"
#include "../../scheduler/generic/Scheduler.h"

PeriodicService::PeriodicService(t_time_ms a_period)
{
	initPeriodicService(a_period);
}

PeriodicService::~PeriodicService()
{

}

bool PeriodicService::initService()
{
	return false;
}

bool PeriodicService::initPeriodicService(t_time_ms a_period)
{
	running = false;
	period = a_period;

	return true;
}

bool PeriodicService::start()
{
	if (!running)
	{
		p_global_scheduler.addPeriodicTask(this);
		running = true;
		return true;
	}
	else
		return false;

}

bool PeriodicService::pause()
{
	if (running)
	{
		if (p_global_scheduler.removePeriodicTask(this))
		{
			running = false;
			return true;
		}
		else
			return false;
	}
	else
		return false;
}
