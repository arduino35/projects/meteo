/*!
 * @file Service.h
 *
 * @brief Service class header file
 *
 * @date Fri Jul  1 18:06:59     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Jul  1 18:06:59     2022 : 74-affichage-valeurs-mix-max-capteurs 
 * Tue Apr  5 21:17:22     2022 : 63-rendre-generique-le-formatage-des-donnees-periodiques 
 * Tue Feb  8 16:02:26     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 17:01:34     2022 : 50-corrections-sonar 
 * 
 */

#ifndef WORK_ASW_SERVICE_GENERIC_SERVICE_H_
#define WORK_ASW_SERVICE_GENERIC_SERVICE_H_

/*!
 * @brief Enumeration defining which type of sensor value shall be used (Current, min, max)
 *
 */
typedef enum
{
	CURRENT,
	MIN,
	MAX,
} T_Service_value_type;

/*!
 * @brief Abstract class service
 * @details This class defines a service. The class is virtual, then it is not
 * 			possible to instantiate a Service object. This class only implements
 * 			the common functions to all services.
 */
class Service {

public:

	/*!
	 * @brief Service class constructor
	 * @details Empty constructor.
	 *
	 * @return Nothing
	 */
	Service();

	/*!
	 * @brief Service initialization function
	 * @details This function initializes the class. It must be called before any use of the class.
	 *
	 * @return TRUE if the initialization has been performed without errors, FALSE otherwise
	 */
	virtual bool initService();

	/*!
	 * @brief Service class destructor
	 * @details This function deletes the class from memory.
	 * 			It is defined as virtual, each service shall have its own implementation.
	 *
	 * @return Nothing
	 */
	virtual ~Service();


};

#endif /* WORK_ASW_SERVICE_GENERIC_SERVICE_H_ */
