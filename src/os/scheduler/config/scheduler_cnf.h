/*!
 * @file scheduler_cnf.h
 *
 * @brief scheduler configuration file
 *
 * @date Tue Feb 22 15:10:01     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Feb 22 15:10:01     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 20:57:20     2022 : 51-optimisation-des-typages 
 * Wed Dec 22 21:22:36     2021 : 50-corrections-sonar 
 * 
 */

#ifndef WORK_SCHEDULER_CONFIG_SCHEDULER_CNF_H_
#define WORK_SCHEDULER_CONFIG_SCHEDULER_CNF_H_

/*!< Software period, used to define periodic timer interrupt */
const t_time_ms SW_PERIOD_MS = 500;

/*!< Value of prescaler to use for periodic timer */
const uint16_t PRESCALER_PERIODIC_TIMER = 256;

/*!< Size of task list (i.e. maximum number of tasks that can be in scheduler */
const uint8_t TASK_LIST_SIZE = 50;

/*!< Cpu Load monitoring activation */
#define CPU_LOAD_ACTIVE


#endif /* WORK_SCHEDULER_CONFIG_SCHEDULER_CNF_H_ */
