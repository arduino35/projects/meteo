/*!
 * @file scheduler.h
 *
 * @brief Scheduler class header file
 *
 * @date Fri Apr 29 11:34:44     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Apr 29 11:34:44     2022 : 60-modification-gestion-periode-des-services 
 * Tue Apr  5 20:18:23     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Feb  8 20:24:21     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 20:57:20     2022 : 51-optimisation-des-typages 
 * Wed Jan  5 18:34:38     2022 : 50-corrections-sonar 
 * 
 */

#ifndef WORK_SCHEDULER_SCHEDULER_H_
#define WORK_SCHEDULER_SCHEDULER_H_

#include "../../../lib/common_lib/generic/common_types.h"
#include "../config/scheduler_cnf.h"

#define TIMER_CTC_VALUE ((F_CPU/PRESCALER_PERIODIC_TIMER)/(1000/SW_PERIOD_MS)) /**< Compare value for periodic timer */


/*!
 * @brief Scheduler class
 * @details This class defines the scheduler of the system. \n
 * 			It is called by the main interrupt and calls successively all applicative functions according to their recurrence time. \n
 * 			All tasks called by the scheduler shall have the following prototype : static void task();
 */
class Scheduler
{
public:

	/*!
	 * @brief scheduler class constructor
	 * @details This function initializes the class scheduler
	 *
	 * @return Nothing
	 */
	Scheduler();

	/*!
	 * @brief Scheduler initialization function
	 * @details This function initializes the scheduler
	 *
	 * @return TRUE if initialization has been correctly performed, FALSE otherwise
	 */
	bool initScheduler();

	/*!
	 * @brief Main scheduler function
	 * @details This function launches the scheduled tasks according to current software time and task configuration
	 *
	 * @return Nothing
	 */
	void launchPeriodicTasks();

	/*!
	 * @brief Starts the tasks scheduling
	 * @details This function starts the timer which will trigger an interrupt every software period.
	 * 			When the interrupt is raised the scheduler will launch applications
	 *
	 * 	@return Nothing
	 */
	void startScheduling() const;

	/*!
	 * @brief Add a task into the scheduler
	 * @details This function create a new task in the scheduler linked to the function task_ptr with a period a_period and an ID a_task_id
	 *
	 * @param [in] servicePtr Pointer to the service task which will be added
	 * @return Nothing
	 */
	void addPeriodicTask(PeriodicService *servicePtr);

	/*!
	 * @brief Remove a task from the scheduler
	 * @details This function finds the task defined by task_ptr in the scheduler and removes it.
	 * @param [in] servicePtr Pointer to the service to remove from scheduler
	 * @return TRUE if the task has been removed, FALSE if the task does not exist in the Scheduler
	 */
	bool removePeriodicTask(const PeriodicService *const servicePtr);

	/*!
	 * @brief Get function for PIT number
	 * @details This function returns the PIT number
	 * @return PIT number
	 */
	inline uint32_t getPitNumber() const
	{
		return pit_number;
	}

	/*!
	 * @brief Task count get function.
	 * @details This function returns the current number of tasks managed by scheduler.
	 *
	 * @return Number of tasks
	 */
	inline uint8_t getTaskCount() const
	{
		return task_count;
	}

private:

	uint8_t task_count; /*!< Number of task in Scheduler */

	PeriodicService *taskList[TASK_LIST_SIZE]; /*!< Pointer to the table containing the tasks */

	uint32_t pit_number; /*!< Counter of periodic interrupts */

};


extern Scheduler p_global_scheduler; /*!< Pointer to Scheduler object */

#endif /* WORK_SCHEDULER_SCHEDULER_H_ */
