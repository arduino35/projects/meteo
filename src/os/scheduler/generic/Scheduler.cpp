/*!
 * @file scheduler.cpp
 *
 * @brief Defines scheduler class
 *
 * @date Fri Apr 29 11:34:43     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Apr 29 11:34:43     2022 : 60-modification-gestion-periode-des-services 
 * Tue Apr  5 20:18:23     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Feb 22 15:10:01     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 20:57:20     2022 : 51-optimisation-des-typages 
 * Wed Jan  5 18:34:38     2022 : 50-corrections-sonar 
 * 
 */



#include <stdlib.h>
#include <avr/io.h>

#include "../../../lib/common_lib/generic/common_types.h"

#include "../../service/generic/Service.h"
#include "../../service/generic/PeriodicService.h"

#include "../../../bsw/bsw_manager/config/bsw_manager_cnf.h"
#include "../../../bsw/bsw_manager/generic/bsw_manager.h"
#include "../../../bsw/watchdog/generic/Watchdog.h"
#include "../../../bsw/timer/generic/Timer.h"

#include "Scheduler.h"

#ifdef CPU_LOAD_ACTIVE
#include "../../../bsw/cpuLoad/generic/CpuLoad.h"
#endif



Scheduler p_global_scheduler;

Scheduler::Scheduler()
{
	initScheduler();
}

bool Scheduler::initScheduler()
{
	bool status;

	/* Initialize task list */
	for (uint8_t i = 0; i < TASK_LIST_SIZE; i++)
		taskList[i] = 0;

	/* Initialize Timer and CpuLoad drivers if needed */
	status = p_global_BSW_manager.initializeDriver(TIMER);

#ifdef CPU_LOAD_ACTIVE
	status &= p_global_BSW_manager.initializeDriver(CPULOAD);
#endif

	/* Initialize counter to 1, then the tasks are not started at first PIT to avoid HW initialization issue */
	pit_number = 1;

	/* No task exists now */
	task_count = 0;

	/* Configure timer for periodic interrupt */
	if (status)
		((Timer*) p_global_BSW_manager.getDriverPointer(TIMER))->configureTimer1(PRESCALER_PERIODIC_TIMER,
		        TIMER_CTC_VALUE);

	return status;
}


void Scheduler::launchPeriodicTasks()
{

	/* Reset watchdog */
	if (p_global_BSW_manager.isDriverInitialized(WDG))
		((Watchdog*) (p_global_BSW_manager.getDriverPointer(WDG)))->reset();

	/* Parse all tasks */
	for (uint8_t task_idx = 0; task_idx < TASK_LIST_SIZE; task_idx++)
	{
		/* If the task is defined and shall be launched at the current cycle, launch it */
		if ((taskList[task_idx] != 0) && ((pit_number % (taskList[task_idx]->getPeriod() / SW_PERIOD_MS)) == 0))
			taskList[task_idx]->run();
	}

#ifdef CPU_LOAD_ACTIVE
	/* Compute CPU load */
	((CpuLoad*) (p_global_BSW_manager.getDriverPointer(CPULOAD)))->computeCPULoad();
#endif

	/* Increment counter */
	pit_number++;
}

void Scheduler::startScheduling() const
{
	((Timer*) p_global_BSW_manager.getDriverPointer(TIMER))->startTimer1();
}


void Scheduler::addPeriodicTask(PeriodicService *service)
{
	/* If scheduling table is not full */
	if (task_count < TASK_LIST_SIZE)
	{
		/* Find first empty index */
		uint8_t task_idx = 0;
		while (taskList[task_idx] != 0)
			task_idx++;

		taskList[task_idx] = service;

		task_count++;
	}
}


bool Scheduler::removePeriodicTask(const PeriodicService *const service)
{
	/* Find task into the table */
	uint8_t idx = 0;

	while ((idx < TASK_LIST_SIZE) && (taskList[idx] != service))
		idx++;

	if (idx < TASK_LIST_SIZE)
	{
		/* Remove the task */
		taskList[idx] = 0;
		task_count--;
		return true;
	}
	else
		return false;
}


