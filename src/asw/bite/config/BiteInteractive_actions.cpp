/*!
 * @file BiteInteractive_actions.cpp
 *
 * @brief Interactive BITE actions source file
 *
 * @date Thu May 26 16:57:49     2022�c. 2020
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Thu May 26 16:57:49     2022 : 71-changement-periode-de-l-autoswitch-de-l-affichage 
 * Tue May 10 10:52:53     2022 : 70-ajout-parametrage-bite-pour-affichage 
 * Fri May  6 19:22:18     2022 : 69-synchro-jour-de-la-semaine 
 * Fri May  6 18:02:04     2022 : 67-rendre-generique-affichage-ecran 
 * Tue Apr  5 20:18:05     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Apr  5 12:06:05     2022 : 63-rendre-generique-le-formatage-des-donnees-periodiques 
 * Wed Feb 16 20:05:04     2022 : 59-passage-en-allocation-statique 
 * Thu Dec 23 18:17:11     2021 : 50-corrections-sonar 
 * Wed Dec  1 16:54:54     2021 : 43-ajout-changement-valeur-wdg-dans-bite 
 * 
 */

#include <avr/io.h>

#include "../../../lib/common_lib/generic/mem_tools.h"

#include "../../../os/service/generic/PeriodicService.h"
#include "../../../bsw/bsw_manager/generic/bsw_manager.h"
#include "../../asw_manager/generic/asw_manager.h"


bool systemReset_Action(uint32_t param, uint8_t *const retval)
{
	return ((Watchdog*) (p_global_BSW_manager.getDriverPointer(WDG)))->systemReset();
}

bool switchWDG_Action(uint32_t param, uint8_t *const retval)
{
	if (((Watchdog*) (p_global_BSW_manager.getDriverPointer(WDG)))->switchWdg())
		*retval = 0xFF;
	else
		*retval = 0;

	return true;
}

bool getWDGStatus_Action(uint32_t param, uint8_t *const retval)
{
	if (((Watchdog*) (p_global_BSW_manager.getDriverPointer(WDG)))->isEnabled())
		*retval = 0xFF;
	else
		*retval = 0;

	return true;
}

bool dateSynchro_request(uint32_t param, uint8_t *const retval)
{
	T_TimeManager_time newTime;
	newTime.hours = retval[0];
	newTime.minutes = retval[1];
	newTime.seconds = retval[2];

	T_TimeManager_date newDate;
	newDate.day = retval[3];
	newDate.month = retval[4];
	newDate.year = retval[5] * 100 + retval[6];
	newDate.dayName = retval[7];

	TimeManager *timePtr = (TimeManager*) (p_global_ASW_manager.getServicePointer(DATE_TIME));

	bool status = false;

	if (timePtr != 0)
		status = timePtr->updateTime(newTime, newDate);

	return status;
}

bool setWdgTmo(uint32_t param, uint8_t *const retval)
{
	bool status;

	Watchdog *wdg_ptr = ((Watchdog*) (p_global_BSW_manager.getDriverPointer(WDG)));

	switch (param)
	{
	case 5:
	case 6:
	case 7:
	case 8:
	case 9:
		wdg_ptr->timeoutUpdate((uint8_t) param);
		status = true;
		break;
	default:
		status = false;
		break;
	}

	return status;
}

bool getWdgTmo(uint32_t param, uint8_t *const retval)
{
	uint16_t tmo = ((Watchdog*) (p_global_BSW_manager.getDriverPointer(WDG)))->getTMOValue();
	mem_copy((uint8_t*) &tmo, retval, 2);
	return true;
}

bool switchScreen_action(uint32_t param, uint8_t *const retval)
{
	return ((DisplayManagement*) (p_global_ASW_manager.getPeriodicServicePointer(LCD_DISPLAY)))->switchPage();
}

bool getAutoSwitchStatus(uint32_t param, uint8_t *const retval)
{
	if (((DisplayManagement*) (p_global_ASW_manager.getPeriodicServicePointer(LCD_DISPLAY)))->isIsAutoSwitchActive())
		*retval = 0xFF;
	else
		*retval = 0;

	return true;
}

bool setAutoSwitchStatus(uint32_t param, uint8_t *const retval)
{
	((DisplayManagement*) (p_global_ASW_manager.getPeriodicServicePointer(LCD_DISPLAY)))->setAutoSwitchStatus();
	return true;
}

bool setAutoSwitchPeriod(uint32_t param, uint8_t *const retval)
{
	return ((DisplayManagement*) (p_global_ASW_manager.getPeriodicServicePointer(LCD_DISPLAY)))->setAutoSwitchPeriod(
	        (t_time_ms) param);
}
