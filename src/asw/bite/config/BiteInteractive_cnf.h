/********************************************/
/**  AUTO-GENERATED FILE : DO NOT MODIFY   **/
/**  OR IT WON'T MATCH HOST CONFIGURATION  **/
/********************************************/


/*!
 * 
 *
 * @file BiteInteractive_cnf.h
 *
 * @brief Interactive BITE configuration file
 *
 * @date Tue Jun  7 17:05:16     2022
 *
 */


#ifndef WORK_ASW_BITE_MANAGER_CONFIG_BITEINTERACTIVE_CNF_H_
#define WORK_ASW_BITE_MANAGER_CONFIG_BITEINTERACTIVE_CNF_H_

/* External variables */
/*!< BITE interactive data configuration table */
extern const T_BITE_Interactive_data bite_Interactive_data_tab[];


/* Constants declarations */
/*!< BITE interactive data configuration table size */
const uint8_t	BITE_INTERACTIVE_DATA_TAB_SIZE		 = 10;

/*!< BITE interactive data max retval size */
const uint8_t	BITE_INTERACTIVE_DATA_RETVAL_MAX_SIZE		 = 8;

/*!< Bite Interactive command ID */
const uint8_t	BITE_INTERACTIVE_ID_SYSTEMRESET_ACTION		 = 1;

/*!< Bite Interactive command ID */
const uint8_t	BITE_INTERACTIVE_ID_SWITCHWDG_ACTION		 = 2;

/*!< Bite Interactive command ID */
const uint8_t	BITE_INTERACTIVE_ID_GETWDGSTATUS_ACTION		 = 3;

/*!< Bite Interactive command ID */
const uint8_t	BITE_INTERACTIVE_ID_DATESYNCHRO_REQUEST		 = 4;

/*!< Bite Interactive command ID */
const uint8_t	BITE_INTERACTIVE_ID_SETWDGTMO		 = 5;

/*!< Bite Interactive command ID */
const uint8_t	BITE_INTERACTIVE_ID_GETWDGTMO		 = 6;

/*!< Bite Interactive command ID */
const uint8_t	BITE_INTERACTIVE_ID_SWITCHSCREEN_ACTION		 = 7;

/*!< Bite Interactive command ID */
const uint8_t	BITE_INTERACTIVE_ID_GETAUTOSWITCHSTATUS		 = 8;

/*!< Bite Interactive command ID */
const uint8_t	BITE_INTERACTIVE_ID_SETAUTOSWITCHSTATUS		 = 9;

/*!< Bite Interactive command ID */
const uint8_t	BITE_INTERACTIVE_ID_SETAUTOSWITCHPERIOD		 = 10;



#endif /* WORK_ASW_BITE_MANAGER_CONFIG_BITEINTERACTIVE_CNF_H_ */
