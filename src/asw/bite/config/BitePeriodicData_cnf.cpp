/********************************************/
/**  AUTO-GENERATED FILE : DO NOT MODIFY   **/
/**  OR IT WON'T MATCH HOST CONFIGURATION  **/
/********************************************/


/*!
 * 
 *
 * @file BitePeriodicData_cnf.cpp
 *
 * @brief BITE periodic data configuration file
 *
 * @date Tue Jun  7 17:05:16     2022
 *
 */


/* Included files */
#include <avr/io.h>
#include "../../../os/service/generic/PeriodicService.h"
#include "../../asw_manager/generic/asw_manager.h"
#include "../generic/BitePeriodicData.h"


extern const T_BITE_Periodic_data Bite_Periodic_data_tab[] = 
{
	{1, 2, 1, 1, 5, CURRENT, SENSORS},/*!< Temperature current */

	{2, 5, 1, 1, 60, MAX, SENSORS},/*!< Temperature MAX */

	{3, 5, 1, 1, 60, MIN, SENSORS},/*!< Temperature MIN */

	{4, 2, 1, 2, 5, CURRENT, SENSORS},/*!< Humidite current */

	{5, 5, 1, 2, 60, MAX, SENSORS},/*!< Humidite MAX */

	{6, 5, 1, 2, 60, MIN, SENSORS},/*!< Humidite MIN */

	{7, 2, 1, 3, 5, CURRENT, SENSORS},/*!< Pression current */

	{8, 5, 1, 3, 60, MAX, SENSORS},/*!< Pression MAX */

	{9, 5, 1, 3, 60, MIN, SENSORS},/*!< Pression MIN */

	{10, 2, 2, 3, 10, CURRENT, SENSORS},/*!< Charge CPU Max current */

	{11, 2, 2, 2, 10, CURRENT, SENSORS},/*!< Charge CPU moyenne current */

	{12, 8, 0, 0, 5, CURRENT, DATE_TIME}/*!< Date/Heure current */

};
