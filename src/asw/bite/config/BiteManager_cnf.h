/*!
 * @file BiteManager_cnf.h
 *
 * @brief Bite manager configuration file
 *
 * @date Sat Apr 30 19:13:53     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Sat Apr 30 19:13:53     2022 : 65-detection-automatique-de-la-deconnexion-du-bite 
 * Tue Apr  5 20:18:06     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Thu Feb 10 20:26:58     2022 : 59-passage-en-allocation-statique 
 * 
 */

#ifndef WORK_ASW_BITE_MANAGER_CONFIG_BITEMANAGER_CNF_H_
#define WORK_ASW_BITE_MANAGER_CONFIG_BITEMANAGER_CNF_H_


/*!< Flag defining if the bluetooth interface or the USART is used */
const bool BITE_MGT_USE_BLE = false;

/*!< Usart bus to use in case of bluetooth transmission */
const uint8_t BITE_MGT_USART_BUS_ID_BLE = 1;

/*!< Usart bus to use in case of direct transmission to PC */
const uint8_t BITE_MGT_USART_BUS_ID_DIRECT = 0;

/*!< Usart SW interrupt pin to use */
const uint8_t BITE_MGT_SW_INT_PIN = 5;

/*!< Period of BITE_READY frame transmission in ms */
const t_time_ms BITE_MGT_BITE_PERIOD = 500;

/*!< Size of reception fifo */
const uint8_t BITE_MGT_RCV_FIFO_SIZE = 10;

/*!< Periodic data activation flag */
const bool BITE_MGT_PERIODIC_DATA_ACTIVE = true;

/*!< Port for activation LED */
const uint8_t BITE_MGT_LED_PORT = ENCODE_PORT(PORT_B, 4);

/*!< BITE active frame transmission period */
const t_time_ms BITE_MGT_READY_FRAME_TRANSMIT_PERIOD = 5000;


/****************************************************/
/*           Definition of frame fields             */
/****************************************************/
/*!< Position of STX character */
const uint8_t BITE_MGT_FRAME_POS_STX = 0;

/*!< Position of TYPE character */
const uint8_t BITE_MGT_FRAME_POS_TYPE = 1;

/*!< Position of STATUS character */
const uint8_t BITE_MGT_FRAME_POS_STATUS = 2;

/*!< Position of CMD character */
const uint8_t BITE_MGT_FRAME_POS_CMD = 3;

/*!< Position of SIZE character */
const uint8_t BITE_MGT_FRAME_POS_SIZE = 4;

/*!< Position of PARAM table */
const uint8_t BITE_MGT_FRAME_POS_PARAM = 5;

/*!< Position of CKS character */
#define BITE_MGT_FRAME_POS_CKS			(size - 3)

/*!< Position of PRE_ETX character */
#define BITE_MGT_FRAME_POS_FFETX		(size - 2)

/*!< Position of ETX character */
#define BITE_MGT_FRAME_POS_ETX			(size - 1)

/*!< Maximum frame size */
const uint8_t BITE_MGT_MAX_FRAME_SIZE = 50;

/*!< Maximum size of the PARAM field */
const uint8_t BITE_MGT_FRAME_PARAM_MAX_SIZE = (BITE_MGT_MAX_FRAME_SIZE - 8);


#endif /* WORK_ASW_BITE_MANAGER_CONFIG_BITEMANAGER_CNF_H_ */
