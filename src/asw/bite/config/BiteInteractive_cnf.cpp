/********************************************/
/**  AUTO-GENERATED FILE : DO NOT MODIFY   **/
/**  OR IT WON'T MATCH HOST CONFIGURATION  **/
/********************************************/


/*!
 * 
 *
 * @file BiteInteractive_cnf.cpp
 *
 * @brief Interactive BITE configuration file
 *
 * @date Tue Jun  7 17:05:16     2022
 *
 */


/* Included files */
#include <avr/io.h>
#include "../../../os/service/generic/PeriodicService.h"
#include "../../../bsw/usart/generic/Usart.h"
#include "../../../bsw/dio/generic/Dio.h"
#include "../generic/BiteFrame.h"
#include "../generic/BiteInteractive.h"
#include "BiteInteractive_cnf.h"
#include "BiteInteractive_actions.h"
/* @formatter:off */


extern const T_BITE_Interactive_data bite_Interactive_data_tab[] = 
{
	{BITE_INTERACTIVE_ID_SYSTEMRESET_ACTION, &systemReset_Action, 0},/*!< ID: 1, Function to call: systemReset_Action */

	{BITE_INTERACTIVE_ID_SWITCHWDG_ACTION, &switchWDG_Action, 1},/*!< ID: 2, Function to call: switchWDG_Action */

	{BITE_INTERACTIVE_ID_GETWDGSTATUS_ACTION, &getWDGStatus_Action, 1},/*!< ID: 3, Function to call: getWDGStatus_Action */

	{BITE_INTERACTIVE_ID_DATESYNCHRO_REQUEST, &dateSynchro_request, 8},/*!< ID: 4, Function to call: dateSynchro_request */

	{BITE_INTERACTIVE_ID_SETWDGTMO, &setWdgTmo, 0},/*!< ID: 5, Function to call: setWdgTmo */

	{BITE_INTERACTIVE_ID_GETWDGTMO, &getWdgTmo, 2},/*!< ID: 6, Function to call: getWdgTmo */

	{BITE_INTERACTIVE_ID_SWITCHSCREEN_ACTION, &switchScreen_action, 0},/*!< ID: 7, Function to call: switchScreen_action */

	{BITE_INTERACTIVE_ID_GETAUTOSWITCHSTATUS, &getAutoSwitchStatus, 1},/*!< ID: 8, Function to call: getAutoSwitchStatus */

	{BITE_INTERACTIVE_ID_SETAUTOSWITCHSTATUS, &setAutoSwitchStatus, 0},/*!< ID: 9, Function to call: setAutoSwitchStatus */

	{BITE_INTERACTIVE_ID_SETAUTOSWITCHPERIOD, &setAutoSwitchPeriod, 0}/*!< ID: 10, Function to call: setAutoSwitchPeriod */

};
/* @formatter:on */
