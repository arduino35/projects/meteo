/********************************************/
/**  AUTO-GENERATED FILE : DO NOT MODIFY   **/
/**  OR IT WON'T MATCH HOST CONFIGURATION  **/
/********************************************/


/*!
 * 
 *
 * @file BiteManager_frameConfig_cnf.h
 *
 * @brief BITE frame data configuration file
 *
 * @date Thu Feb 10 13:18:41     2022
 *
 */


#ifndef WORK_ASW_BITE_MANAGER_CONFIG_BITEMANAGER_FRAMECONFIG_CNF_H_
#define WORK_ASW_BITE_MANAGER_CONFIG_BITEMANAGER_FRAMECONFIG_CNF_H_

/* Constants declarations */
/*!< BITE major version */
const uint8_t	BITE_VERSION_MAJOR		 = 0;

/*!< BITE minor version */
const uint8_t	BITE_VERSION_MINOR		 = 1;



/* Constants declarations */
/*!< STX character definition */
const uint8_t	CHAR_STX		 = 160;

/*!< ETX character definition */
const uint8_t	CHAR_ETX		 = 250;

/*!< TYPE_DATA character definition */
const uint8_t	BITE_MGT_CMD_TYPE_DAT		 = 51;

/*!< TYPE_REQ character definition */
const uint8_t	BITE_MGT_CMD_TYPE_REQ		 = 17;

/*!< TYPE_ANS character definition */
const uint8_t	BITE_MGT_CMD_TYPE_ANS		 = 34;

/*!< TYPE_READY character definition */
const uint8_t	BITE_MGT_CMD_TYPE_READY		 = 68;

/*!< TYPE_DEBUG character definition */
const uint8_t	BITE_MGT_CMD_TYPE_DEBUG		 = 85;

/*!< STATUS_ACK character definition */
const uint8_t	BITE_MGT_CMD_STATUS_ACK		 = 6;

/*!< STATUS_NAK character definition */
const uint8_t	BITE_MGT_CMD_STATUS_NAK		 = 21;



/* Constants declarations */
/*!< Baudrate used for Usart communication */
const T_usart_baudrates	BITE_MGT_USART_BAUDRATE		 = BAUD_57600;

/*!< Data bits setting used for Usart communication */
const T_usart_data_bits	BITE_MGT_USART_DATABITS		 = DATA_8;

/*!< Stop bits setting used for Usart communication */
const T_usart_stop_bits	BITE_MGT_USART_STOPBITS		 = STOP_1;

/*!< Parity setting used for Usart communication */
const T_usart_parity	BITE_MGT_USART_PARITY		 = PARITY_NONE;



#endif /* WORK_ASW_BITE_MANAGER_CONFIG_BITEMANAGER_FRAMECONFIG_CNF_H_ */
