/*!
 * @file BiteInteractive_actions.h
 *
 * @brief Interactive BITE actions header file
 *
 * @date Thu May 26 16:35:29     2022�c. 2020
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Thu May 26 16:35:29     2022 : 71-changement-periode-de-l-autoswitch-de-l-affichage 
 * Tue May 10 10:52:53     2022 : 70-ajout-parametrage-bite-pour-affichage 
 * Fri May  6 18:02:04     2022 : 67-rendre-generique-affichage-ecran 
 * Wed Feb 16 20:05:04     2022 : 59-passage-en-allocation-statique 
 * Wed Dec  1 16:54:55     2021 : 43-ajout-changement-valeur-wdg-dans-bite 
 * 
 */

#ifndef SRC_ASW_BITE_MANAGER_CONFIG_BITEINTERACTIVE_ACTIONS_H_
#define SRC_ASW_BITE_MANAGER_CONFIG_BITEINTERACTIVE_ACTIONS_H_

bool systemReset_Action(uint32_t param, uint8_t *const retval);
bool switchWDG_Action(uint32_t param, uint8_t *const retval);
bool getWDGStatus_Action(uint32_t param, uint8_t *const retval);
bool dateSynchro_request(uint32_t param, uint8_t *const retval);
bool setWdgTmo(uint32_t param, uint8_t *const retval);
bool getWdgTmo(uint32_t param, uint8_t *const retval);
bool switchScreen_action(uint32_t param, uint8_t *const retval);
bool getAutoSwitchStatus(uint32_t param, uint8_t *const retval);
bool setAutoSwitchStatus(uint32_t param, uint8_t *const retval);
bool setAutoSwitchPeriod(uint32_t param, uint8_t *const retval);

#endif /* SRC_ASW_BITE_MANAGER_CONFIG_BITEINTERACTIVE_ACTIONS_H_ */
