/********************************************/
/**  AUTO-GENERATED FILE : DO NOT MODIFY   **/
/**  OR IT WON'T MATCH HOST CONFIGURATION  **/
/********************************************/


/*!
 * 
 *
 * @file BitePeriodicData_cnf.h
 *
 * @brief BITE periodic data configuration file
 *
 * @date Tue Jun  7 17:05:17     2022
 *
 */


#ifndef WORK_ASW_BITE_MANAGER_CONFIG_BITEPERIODICDATA_CNF_H_
#define WORK_ASW_BITE_MANAGER_CONFIG_BITEPERIODICDATA_CNF_H_

/* External variables */
/*!< BITE periodic data configuration table */
extern const T_BITE_Periodic_data Bite_Periodic_data_tab[];


/* Constants declarations */
/*!< Transmission period of the periodic data */
const t_time_ms	BITE_MGT_PERIODIC_DATA_TRANSMIT_PERIOD_MS		 = 1000;

/*!< Number of periodic data to send */
const uint8_t	BITE_MGT_PERIODIC_CNF_TABLE_SIZE		 = 12;

/*!< Max size for param table */
const uint8_t	BITE_MGT_PERIODIC_CNF_MAX_PARAM_SIZE		 = 8;



#endif /* WORK_ASW_BITE_MANAGER_CONFIG_BITEPERIODICDATA_CNF_H_ */
