# BitePeriodicData

BITE periodic data transmission service

---Auto-config part---
@ClassName:BitePeriodicData
@AswEnumName:BITE_PERIODIC
@Description:BITE periodic data transmission
@Nature:PERIODIC
@Name:BITE Periodic
