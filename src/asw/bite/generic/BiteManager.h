/*!
 * @file BiteManager.h
 *
 * @brief BITE manager class header file
 *
 * @date Thu May 26 23:16:35     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Thu May 26 23:16:35     2022 : 73-implementation-heritage-pour-fifo 
 * Sat Apr 30 19:13:54     2022 : 65-detection-automatique-de-la-deconnexion-du-bite 
 * Tue Apr  5 20:18:08     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Apr  1 17:20:45     2022 : 59-passage-en-allocation-statique 
 * Mon Jan 24 23:44:44     2022 : 57-corrections-bugs-bite 
 * Mon Jan 24 20:49:55     2022 : 48-synchro-host-avec-identification 
 * Fri Jan 21 20:56:39     2022 : 55-changement-chaine-tx-usart 
 * Thu Dec 23 20:44:51     2021 : 50-corrections-sonar 
 * Mon Dec 20 11:13:15     2021 : 44-compatibilite-de-version-et-identification-arduino 
 * 
 */

#ifndef WORK_ASW_BITE_MANAGER_GENERIC_BITEMANAGER_H_
#define WORK_ASW_BITE_MANAGER_GENERIC_BITEMANAGER_H_

#include "BiteFrame.h"
#include "BiteFifo.h"

/*!
 * @brief BITE manager class
 * @details This class manages the BITE communication protocol. This protocol is used to communicate with a PC using USART or Bluetooth (via USART) and display data in an HMI tool.
 */
class BiteManager : public PeriodicService
{
public:

	/*!
	 * @brief Class constructor
	 * @details This function initializes the class.
	 */
	BiteManager();

	/*!
	 * @brief Service initialization function
	 * @details This function initializes the service BiteManager. It initializes the USART driver and the reception Fifo.
	 *
	 * @return
	 */
	virtual bool initService();

	/*!
	 * @brief USART data reception function
	 * @details This function is called each time recption buffer from USART driver is full.
	 * 			Its role is to retrieve the received data from the driver.
	 *
	 * @return Nothing
	 */
	void usartReceivedDataISR();

	/*!
	 * @brief Frame builder function
	 * @details This function is in charge of building the frame to send.
	 *
	 * @param [in] type Type of frame
	 * @param [in] status Status of the frame
	 * @param [in] cmd	ID of the command
	 * @param [in] param Parameter table
	 * @param [in] param_size Size of the parameter table
	 *
	 * @return Nothing
	 */
	void buildFrame(uint8_t type, uint8_t status, uint8_t cmd, const uint8_t *const param =
			0, uint8_t param_size = 0);

	/*!
	 * @brief BITE_READY frame transmission function
	 * @details This function is called periodically by the scheduler.
	 * 			This function transmits the BITE_READY frame on usart line.
	 *
	 * @return Nothing
	 */
	virtual void run();

	/*!
	 * @brief biteStarted flag getting function
	 *
	 * @return biteStarted flag
	 */
	inline bool isBiteStarted() const
	{
		return biteStarted;
	}

	/*!
	 * @brief Overloaded service start function
	 * @details This function starts the BITE service. It will create and the BITE interactive and periodic services and start them.
	 *          This function must be called after the SW initialization phase.
	 *
	 * @return TRUE is the service has been correctly started, FALSE otherwise
	 */
	bool start();

private:

	/*!
	 * @brief Frame analysis function
	 * @details This function analyzes the received frame to check if it's correct or not.
	 *          It creates a BiteFrame object representing the received frame.
	 *
	 * @return Nothing
	 */
	void analyseFrame();

	/*!
	 * @brief Frame extraction function
	 * @details Analyse reception buffer to find new frames.
	 *    		If a new frame is found, add it in the reception FIFO.
	 *
	 * @return TRUE if a new has been extracted, FALSE otherwise
	 */
	bool extractFrame();

	/*!
	 * @brief STX char finder
	 * @details Finds the 1st STX character in the reception buffer
	 *
	 * @param [out] pos Position of STX
	 * @return TRUE of STX has been found, FALSE otherwise
	 *
	 */
	bool findSTX(uint8_t *pos) const;

	/*!
	 * @brief ETX checker function
	 * @details This function checks if the ETX character is present at the correct location, with pre_ETX character before him
	 *
	 * @param [in] pos Expected ETX position
	 * @return TRUE if check is OK, FALSE otherwise
	 *
	 */
	bool checkETX(uint8_t pos);

	/*!
	 * @brief New received data copying function
	 * @details This function copies all new data from Usart driver into the reception buffer
	 *
	 * @return Nothing
	 */
	void copyNewData();

	/*!
	 * @brief Reception buffer shifting function
	 * @details Removes the first n characters from the reception buffer and shift the remaining ones
	 *
	 * @param [in] i Number of shifts to perform
	 * @return Nothing
	 */
	void shiftReceptionBuffer(uint8_t i);

	/*!
	 * @brief BITE Ready frame processing
	 * @details This function processes the received BITE ready frame to set BITE active flag,
	 *          frame timestamp and transmits BITE ready in return.
	 *
	 * @param [in] frame Received frame
	 * @return Nothing
	 */
	void processBiteReadyFrame(BiteFrame frame);


	Usart *usartDrv_ptr; /*!< Pointer to the USART driver object */

	BiteFifo biteFifo; /*!< Fifo containing received frames */

	uint8_t receptionBuffer[256]; /*!< Reception buffer */
	uint8_t receptionBufferSize; /*!< Current reception buffer size */

	bool biteStarted; /*!< Flag indicating is the BITE has been started by the host interface */

	uint8_t bite_ready_emm_counter; /*!< Emission counter for BITE READY frame */

	BiteFrame frameToSend; /*!< Next frame to send */

	uint32_t biteActivationFrameTs; /*!< Timestamp of last BITE activation frame from host */

};

#endif /* WORK_ASW_BITE_MANAGER_GENERIC_BITEMANAGER_H_ */
