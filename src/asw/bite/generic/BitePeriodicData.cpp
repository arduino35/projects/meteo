/*!
 * @file BitePeriodicData.cpp
 *
 * @brief BITE periodic data source file
 *
 * @date Sat Apr 30 19:35:06     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Sat Apr 30 19:35:06     2022 : 66-mauvaise-emission-des-donnees-periodiques-au-demarrage 
 * Tue Apr  5 21:05:47     2022 : 63-rendre-generique-le-formatage-des-donnees-periodiques 
 * Tue Apr  5 20:18:08     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Feb 25 14:31:03     2022 : 59-passage-en-allocation-statique 
 * Fri Feb  4 08:51:55     2022 : 57-corrections-bugs-bite 
 * Fri Jan 21 18:18:49     2022 : 48-synchro-host-avec-identification 
 * Sun Jan  9 20:39:54     2022 : 53-generation-des-fichiers-bite-a-la-compilation 
 * Wed Jan  5 17:01:25     2022 : 50-corrections-sonar 
 * 
 */

#include <avr/io.h>

#include "../../../lib/common_lib/generic/mem_tools.h"
#include "../../../lib/common_lib/generic/common_types.h"

#include "../../../os/service/generic/PeriodicService.h"
#include "../../../os/scheduler/generic/Scheduler.h"
#include "../../asw_manager/generic/asw_manager.h"


#include "BitePeriodicData.h"
#include "../config/BiteManager_cnf.h"
#include "../config/BiteManager_frameConfig_cnf.h"
#include "../config/BitePeriodicData_cnf.h"


#include "../../time_manager/generic/TimeManager.h"

#include "../../sensors/sensors_mgt/generic/SensorManagement.h"





BitePeriodicData::BitePeriodicData() : PeriodicService(BITE_MGT_PERIODIC_DATA_TRANSMIT_PERIOD_MS)
{
	periodicDataIdx = 0;
	trans_counter = 0;
}

bool BitePeriodicData::initService()
{
	periodicDataIdx = 0;
	trans_counter = 0;

	if (p_global_ASW_manager.isServiceInitialized(BITE)
	        && initPeriodicService(BITE_MGT_PERIODIC_DATA_TRANSMIT_PERIOD_MS))
		return start();
	else
		return false;
}

void BitePeriodicData::run()
{
	BiteManager *bite_manager_ptr = (BiteManager*) (p_global_ASW_manager.getPeriodicServicePointer(BITE));

	/* If there is at least one periodic data to send */
	if ((BITE_MGT_PERIODIC_CNF_TABLE_SIZE > 0) && (bite_manager_ptr->isBiteStarted()))
	{
		/* For each data to transmit */
		for (uint8_t i = 0; i < BITE_MGT_PERIODIC_CNF_TABLE_SIZE; i++)
		{
			if (((trans_counter - i) % Bite_Periodic_data_tab[i].period) == 0)
			{
				/* Build and send periodic data */
				uint8_t dataPtr[BITE_MGT_PERIODIC_CNF_MAX_PARAM_SIZE];
				bool validity;

				switch (Bite_Periodic_data_tab[i].service)
				{
				case SENSORS:
					validity = ((SensorManagement*) (p_global_ASW_manager.getServicePointer(SENSORS)))->formatBiteData(
					        dataPtr, Bite_Periodic_data_tab[i].sensor_group, Bite_Periodic_data_tab[i].sensor_id,
					        Bite_Periodic_data_tab[i].type);
					break;
				case DATE_TIME:
					validity = ((TimeManager*) (p_global_ASW_manager.getServicePointer(DATE_TIME)))->formatBiteData(
					        dataPtr, 0, 0, Bite_Periodic_data_tab[i].type);
					break;
				default:
					validity = false;
					break;
				}

				if (validity)
					bite_manager_ptr->buildFrame(BITE_MGT_CMD_TYPE_DAT, 0xFF, Bite_Periodic_data_tab[i].cmd, dataPtr,
					        Bite_Periodic_data_tab[i].param_size);
				else
					bite_manager_ptr->buildFrame(BITE_MGT_CMD_TYPE_DAT, 0, Bite_Periodic_data_tab[i].cmd);
			}
		}

		/* Increment cycles counter */
		trans_counter++;
	}
}

