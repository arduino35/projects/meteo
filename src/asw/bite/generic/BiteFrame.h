/*!
 * @file BiteFrame.h
 *
 * @brief BiteFrame class header file
 *
 * @date Tue Apr  5 20:18:06     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:07     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Thu Feb 10 20:26:59     2022 : 59-passage-en-allocation-statique 
 * Thu Dec 23 18:17:13     2021 : 50-corrections-sonar 
 * 
 */

#ifndef WORK_ASW_BITE_MANAGER_GENERIC_BITEFRAME_H_
#define WORK_ASW_BITE_MANAGER_GENERIC_BITEFRAME_H_

#include "../config/BiteManager_frameConfig_cnf.h"
#include "../config/BiteManager_cnf.h"

/*!
 * @brief Class defining a BITE frame
 * @details This class defines a BITE frame sent to the receiver.
 *          It contains all useful functions to build a complete frame.
 */
class BiteFrame
{
public:

	/*!
	 * @brief BiteFrame class constructor
	 * @details This function initializes the class
	 *
	 * @return Nothing
	 */
	BiteFrame();


	/*!
	 * @brief Bite Frame initialization function
	 * @details  This function initializes a new frame TYPE, STATUS and CMD fields given in parameters.
	 * 			 STX and ETX are also set.
	 *
	 * @param [in] l_type Value of TYPE field
	 * @param [in] l_status Value of STATUS field
	 * @param [in] l_cmd Value of CMD field
	 * @return Nothing
	 */
	void initFrame(uint8_t l_type, uint8_t l_status, uint8_t l_cmd);



	/*!
	 * @brief Frame get function
	 * @details This function build the frame according to the stored parameters.
	 * 			The CKS field is updated before returning the frame pointer.
	 * 			The caller function shall unallocate the table from memory after use.
	 *
	 * @return Pointer to the frame
	 */
	uint8_t* getFrame();


	/*!
	 * @brief Frame size get function
	 * @details This function returns the frame size.
	 *
	 * @return Frame size
	 */
	inline uint8_t getSize() const
	{
		return size;
	}


	/*!
	 * @brief Parameter adding function
	 * @details This function sets the field PARAM into the frame according to the table given in parameter.
	 *
	 * @param [in] l_param PARAM table
	 * @param [in] l_param_size Size of the table
	 *
	 * @return Nothing
	 */
	void setParam(const uint8_t *l_param, uint8_t l_param_size);

	/*!
	 * @brief Checksum computation function
	 * @details This function computes the checksum of the frame.
	 *
	 * @return Frame checksum
	 */
	uint8_t getChecksum() const;


	inline uint8_t getCmd() const
	{
		return frame[BITE_MGT_FRAME_POS_CMD];
	}

	inline void setCmd(uint8_t l_cmd)
	{
		frame[BITE_MGT_FRAME_POS_CMD] = l_cmd;
	}

	inline uint8_t getParamSize() const
	{
		return paramSize;
	}

	inline uint8_t getStatus() const
	{
		return frame[BITE_MGT_FRAME_POS_STATUS];
	}

	inline void setStatus(uint8_t l_status)
	{
		frame[BITE_MGT_FRAME_POS_STATUS] = l_status;
	}

	inline uint8_t getType() const
	{
		return frame[BITE_MGT_FRAME_POS_TYPE];
	}

	inline void setType(uint8_t l_type)
	{
		frame[BITE_MGT_FRAME_POS_TYPE] = l_type;
	}

	/*!
	 * @brief PARAM field get function
	 * @details This function returns a pointer to the start of the PARAM field. If no PARAM field is defined, it returns 0
	 *
	 * @return Address of PARAM field
	 */
	uint8_t* getParamPtr();


private:

	uint8_t size; /*!< Frame size */
	uint8_t paramSize; /*!< Size of the PARAM field */

	uint8_t frame[BITE_MGT_MAX_FRAME_SIZE]; /*!< Frame table */
};

#endif /* WORK_ASW_BITE_MANAGER_GENERIC_BITEFRAME_H_ */
