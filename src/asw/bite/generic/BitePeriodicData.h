/*!
 * @file BitePeriodicData.h
 *
 * @brief BITE periodic data header file
 *
 * @date Fri Jul  1 18:06:58     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Jul  1 18:06:58     2022 : 74-affichage-valeurs-mix-max-capteurs 
 * Tue Apr  5 20:18:08     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Apr  5 14:23:25     2022 : 63-rendre-generique-le-formatage-des-donnees-periodiques 
 * Fri Apr  1 17:20:45     2022 : 59-passage-en-allocation-statique 
 * Thu Dec 23 18:17:14     2021 : 50-corrections-sonar 
 * 
 */

#ifndef SRC_ASW_BITE_MANAGER_GENERIC_BITEPERIODICDATA_H_
#define SRC_ASW_BITE_MANAGER_GENERIC_BITEPERIODICDATA_H_

/*!
 * @brief Type defining a pointer to function
 */
typedef uint8_t (*BitePeriodic_funcPtr)(uint8_t *dataPtr);

/*!
 * @brief Structure defining a BITE periodic data
 * @details This structure defines a BITE periodic data.
 */
typedef struct
{
	uint8_t cmd; /*!< CMD field of the associated data frame */
	uint8_t param_size; /*!< Size of the PARAM field **/
	uint8_t sensor_group; /*!< Group of the selected sensor */
	uint8_t sensor_id; /*!< ID of the selected sensor */
	uint8_t period; /*!< Period of transmission */
	T_Service_value_type type; /*!< Type of sensor value to use */
	T_ASW_manager_service_list service; /*!< Service to use to get the data */
} T_BITE_Periodic_data;


#include "../config/BitePeriodicData_cnf.h"


/*!
 * @brief Class managing periodic data sending
 * @details This class is a service sending periodically the configured data.
 *          It uses BiteManager class to configure transmission and to send data.
 */
class BitePeriodicData: public PeriodicService
{
public:

	/*!
	 * @brief Class constructor
	 * @details This function initializes the class and starts periodic transmission
	 *
	 * @return Nothing
	 */
	BitePeriodicData();

	/*!
	 * @brief Service initialization function
	 * @details This function initializes the class and starts periodic transmission
	 *
	 * @return True if the service has been correctly initialized, False otherwise
	 */
	virtual bool initService();

	/*!
	 * @brief Periodic data transmission task
	 * @details This function is called periodically by the scheduler.
	 *          This function transmits on usart line all periodic data.
	 *          It also requests the transmission of the BITE_READY if no periodic data has to be sent.
	 *
	 * @return Nothing
	 */
	virtual void run();


private:

	uint8_t periodicDataIdx; /*!< Index of the current periodic data sent */
	uint8_t trans_counter; /*!< Counter for data transmission */
};

#endif /* SRC_ASW_BITE_MANAGER_GENERIC_BITEPERIODICDATA_H_ */
