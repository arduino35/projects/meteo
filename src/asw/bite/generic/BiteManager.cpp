/*!
 * @file BiteManager.cpp
 *
 * @brief BITE manager class source code file
 *
 * @date Thu May 26 23:16:34     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Thu May 26 23:16:34     2022 : 73-implementation-heritage-pour-fifo 
 * Sat Apr 30 19:13:54     2022 : 65-detection-automatique-de-la-deconnexion-du-bite 
 * Fri Apr 29 11:34:42     2022 : 60-modification-gestion-periode-des-services 
 * Tue Apr  5 20:18:07     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Apr  1 17:20:44     2022 : 59-passage-en-allocation-statique 
 * Mon Jan 24 23:44:44     2022 : 57-corrections-bugs-bite 
 * Mon Jan 24 20:49:55     2022 : 48-synchro-host-avec-identification 
 * Fri Jan 21 20:56:38     2022 : 55-changement-chaine-tx-usart 
 * Fri Jan 21 11:28:37     2022 : 54-correction-plantage-quand-bite-actif 
 * Thu Dec 23 20:44:51     2021 : 50-corrections-sonar 
 * Mon Dec 20 11:13:14     2021 : 44-compatibilite-de-version-et-identification-arduino 
 * 
 */

#include <avr/io.h>

#include "../../../lib/common_lib/generic/mem_tools.h"

#include "../../../os/service/generic/PeriodicService.h"
#include "../../../os/scheduler/generic/Scheduler.h"

#include "../../../bsw/bsw_manager/generic/bsw_manager.h"

#include "../../asw_manager/generic/asw_manager.h"
#include "BiteFrame.h"
#include "BiteInteractive.h"
#include "../config/BiteManager_cnf.h"
#include "../config/BiteManager_frameConfig_cnf.h"

#include "BiteManager.h"

/*!< BITE active frame transmission period in cycles */
const uint8_t BITE_MGT_READY_FRAME_TRANSMIT_PERIOD_CYCLES = (uint8_t) (BITE_MGT_READY_FRAME_TRANSMIT_PERIOD
        / BITE_MGT_BITE_PERIOD);

/*!< Time before BITE automatic disconnection */
const uint8_t BITE_MGT_DISCONNECT_TMO = BITE_MGT_READY_FRAME_TRANSMIT_PERIOD_CYCLES * 2 + 2;


BiteManager::BiteManager() : PeriodicService(BITE_MGT_BITE_PERIOD)
{
	usartDrv_ptr = 0;
	biteStarted = false;
	bite_ready_emm_counter = 0;
	receptionBufferSize = 0;
	biteActivationFrameTs = 0xFFFFFFFF;
}

bool BiteManager::initService()
{
	bool status = initPeriodicService(BITE_MGT_BITE_PERIOD);
	
	uint8_t bus_id;
	if (BITE_MGT_USE_BLE)
		bus_id = BITE_MGT_USART_BUS_ID_BLE;
	else
		bus_id = BITE_MGT_USART_BUS_ID_DIRECT;

	status &= p_global_BSW_manager.initializeDriver(USART_COMM, bus_id);

	if (status)
	{
		usartDrv_ptr =
		        ((Usart*) (p_global_BSW_manager.getDriverPointer(USART_COMM)))->configure(BITE_MGT_USART_BAUDRATE,
		                BITE_MGT_USART_DATABITS, BITE_MGT_USART_PARITY, BITE_MGT_USART_STOPBITS)->setCallback(SW_INT,
		                BITE_MGT_SW_INT_PIN);

		biteFifo.initFifo(BITE_MGT_RCV_FIFO_SIZE);
		receptionBufferSize = 0;

		biteStarted = false;
		bite_ready_emm_counter = 0;
		biteActivationFrameTs = 0xFFFFFFFF;
	}

	return status;
}

void BiteManager::usartReceivedDataISR()
{
	copyNewData();
}

void BiteManager::buildFrame(uint8_t type, uint8_t status, uint8_t cmd, const uint8_t *const param, uint8_t param_size)
{
	frameToSend.initFrame(type, status, cmd);

	if(param_size > 0)
		frameToSend.setParam(param, param_size);

	/* Send frame */
	usartDrv_ptr->sendFrame(frameToSend.getFrame(), frameToSend.getSize());
}

void BiteManager::analyseFrame()
{
	/* Extract latest frame */
	BiteFrame frame;

	/* If the fifo is not empty */
	while (biteFifo.readElement(&frame) != FIFO_EMPTY)
	{
		/* If frame is a BITE_READY frame, start or stop BITE */
		if (frame.getType() == BITE_MGT_CMD_TYPE_READY)
			processBiteReadyFrame(frame);
		else if (biteStarted)
		{
			/* Send the frame to BITE interactive service */
			((BiteInteractive*) (p_global_ASW_manager.getServicePointer(BITE_INTERACTIVE)))->processReceivedFrame(
			        &frame);
		}
	}
}

void BiteManager::run()
{
	while (extractFrame());

	analyseFrame();

	if (bite_ready_emm_counter == 0)
	{
		if (biteStarted)
			buildFrame(BITE_MGT_CMD_TYPE_READY, 1, 0);
		else
			buildFrame(BITE_MGT_CMD_TYPE_READY, 0, 0);
	}

	/* Emission de la trame BITE Ready */
	bite_ready_emm_counter = (bite_ready_emm_counter + 1) % BITE_MGT_READY_FRAME_TRANSMIT_PERIOD_CYCLES;

	/* Détection de connexion */
	if ((p_global_scheduler.getPitNumber() - biteActivationFrameTs) > BITE_MGT_DISCONNECT_TMO)
		biteStarted = false;

	/* Update activation LED */
	((Dio*) (p_global_BSW_manager.getDriverPointer(DIO)))->dio_setPort(BITE_MGT_LED_PORT, biteStarted);
}

void BiteManager::processBiteReadyFrame(BiteFrame frame)
{
	/* if Bite activation is requested */
	/* Check table size (must be 2) */
	/* check version */
	if ((frame.getCmd() == 1) && (frame.getParamSize() == 2) && ((frame.getParamPtr())[0] == BITE_VERSION_MAJOR)
	        && ((frame.getParamPtr())[1] == BITE_VERSION_MINOR))
	{
		if (!biteStarted)
		{
			buildFrame(BITE_MGT_CMD_TYPE_READY, 1, 0);
			biteStarted = true;
		}

		biteActivationFrameTs = p_global_scheduler.getPitNumber();
	}

	else if (frame.getCmd() == 0)
		biteStarted = false;
}


bool BiteManager::start()
{
	bool status;

	/* Start of BITE interactive and periodic */
	status = p_global_ASW_manager.initNewService(BITE_INTERACTIVE);

	if (BITE_MGT_PERIODIC_DATA_ACTIVE && status)
		status &= p_global_ASW_manager.initNewService(BITE_PERIODIC);

	if (!isServiceRunning() && status)
	{
		p_global_scheduler.addPeriodicTask(this);
		setServiceRunning(true);
		status = true;
	}
	else
		status = false;

	return status;
}

void BiteManager::copyNewData()
{
	const T_usart_reception_buffer *usartBuf = usartDrv_ptr->getReceptionBuffer();

	/* If there is still enough space in buffer, copy new data into it, if full, empty the buffer */
	if (receptionBufferSize + usartBuf->size < 256)
	{
		mem_copy(&(usartBuf->buffer[0]), &(receptionBuffer[receptionBufferSize]), usartBuf->size);
		receptionBufferSize += usartBuf->size;
	}
	else
	{
		mem_copy(&(usartBuf->buffer[0]), &(receptionBuffer[0]), usartBuf->size);
		receptionBufferSize = usartBuf->size;
	}
}

bool BiteManager::extractFrame()
{
	copyNewData();

	/* Find first available STX */
	uint8_t posSTX;
	if (!findSTX(&posSTX))
		return false;

	/* STX is found, looking for ETX
	 *
	 * ETX must be here : STX + 4 (TYPE, STATUS, CMD, SIZE) + SIZE + 3 (CKS + PRE_ETX + ETX)
	 *
	 */

	/* If the end of buffer is reached before the SIZE char, exit the function */
	uint8_t paramSize;
	uint8_t posETX;
	if (posSTX + BITE_MGT_FRAME_POS_SIZE < receptionBufferSize)
	{
		paramSize = receptionBuffer[posSTX + BITE_MGT_FRAME_POS_SIZE];
		posETX = posSTX + 7 + paramSize;

		/* If the end of buffer is reached before the ETX char, exit the function */
		if (posETX < receptionBufferSize)
		{
			if (!checkETX(posETX))
				return false;
		}
		else
			return false;
	}
	else
		return false;

	/* New frame is available, remove everything before the STX */
	if (posSTX != 0)
	{
		shiftReceptionBuffer(posSTX);
		receptionBufferSize -= posSTX;
		posSTX = 0;
		posETX -= posSTX;
	}

	/* Create new BiteFrame object */
	BiteFrame frame;
	frame.initFrame(receptionBuffer[BITE_MGT_FRAME_POS_TYPE],
	        receptionBuffer[BITE_MGT_FRAME_POS_STATUS], receptionBuffer[BITE_MGT_FRAME_POS_CMD]);

	/* If PARAM field is not null, add it into the frame */
	uint8_t param_size = receptionBuffer[BITE_MGT_FRAME_POS_SIZE];
	if (param_size != 0)
	{
		uint8_t tmp_param[BITE_MGT_FRAME_PARAM_MAX_SIZE];

		mem_copy(&(receptionBuffer[BITE_MGT_FRAME_POS_PARAM]), tmp_param, param_size);

		frame.setParam(tmp_param, param_size);
	}

	/* Compute checksum */
	/* If frame is valid, add it into the fifo */
	uint8_t size = posETX + 1;
	if (frame.getChecksum() == receptionBuffer[BITE_MGT_FRAME_POS_CKS])
		biteFifo.addNewElement(frame);

	/* Remove frame from buffer */
	shiftReceptionBuffer(size);

	return true;
}

bool BiteManager::findSTX(uint8_t *pos) const
{
	*pos = 0;
	while ((*pos < receptionBufferSize) && (receptionBuffer[*pos] != CHAR_STX))
		(*pos)++;

	if (*pos == receptionBufferSize)
		return false;
	else
		return true;

}

bool BiteManager::checkETX(uint8_t pos)
{
	if ((receptionBuffer[pos] == CHAR_ETX) && (receptionBuffer[pos - 1] == 0xFF))
		return true;
	else
	{
		receptionBufferSize = 0;
		return false;
	}
}

void BiteManager::shiftReceptionBuffer(uint8_t i)
{
	mem_copy(&receptionBuffer[i], &receptionBuffer[0], receptionBufferSize - i);
	receptionBufferSize -= i;
}
