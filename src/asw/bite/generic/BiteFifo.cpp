/*!
 * @file BiteFifo.cpp
 *
 * @brief 
 *
 * @date Thu May 26 23:16:34     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Thu May 26 23:16:34     2022 : 73-implementation-heritage-pour-fifo 
 * 
 */

#include <avr/io.h>

#include "../../../lib/common_lib/generic/common_types.h"

#include "../../../os/service/generic/Service.h"

#include "../../../bsw/dio/generic/Dio.h"
#include "../../../bsw/usart/generic/Usart.h"

#include "BiteFrame.h"
#include "BiteFifo.h"

BiteFifo::BiteFifo()
{
	initFifo(BITE_MGT_RCV_FIFO_SIZE);
}

T_Fifo_Status BiteFifo::addNewElement(BiteFrame frame)
{
	uint8_t idx;
	T_Fifo_Status status = addNewElementIndex(&idx);
	if (status == FIFO_OK)
		fifo_table[idx] = frame;

	return status;
}

T_Fifo_Status BiteFifo::readElement(BiteFrame *frame)
{
	uint8_t idx;
	T_Fifo_Status status = readElementIndex(&idx);
	if (status != FIFO_EMPTY)
		*frame = fifo_table[idx];

	return status;
}
