/*!
 * @file BiteFifo.h
 *
 * @brief 
 *
 * @date Thu May 26 23:16:34     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Thu May 26 23:16:34     2022 : 73-implementation-heritage-pour-fifo 
 * 
 */

#ifndef SRC_ASW_BITE_GENERIC_BITEFIFO_H_
#define SRC_ASW_BITE_GENERIC_BITEFIFO_H_

#include "../../../lib/fifo/generic/Fifo.h"

/*!
 * @brief FIFO implementation for Bite
 *
 */
class BiteFifo : public Fifo
{
public:

	/*!
	 * @brief Class constructor
	 *
	 */
	BiteFifo();

	/*!
	 * @brief Adds a new Bite Frame in the FIFO
	 * @details An element is added in the FIFO.
	 *          If the FIFO was not full before the adding, the returned status is FIFO_OK,
	 *          else the returned status is FIFO_FULL and the element can not be added.
	 *          After the operation, the new status is computed.
	 *
	 * @param frame [in] BiteFrame to add
	 * @return FIFO status
	 */
	T_Fifo_Status addNewElement(BiteFrame frame);

	/*!
	 * @brief Read a BITE Frame from the FIFO
	 * @details This function returns the element to read if the status is not FIFO_EMPTY.
	 * 			If the status was FIFO_EMPTY, no read operation can be done.
	 * 			After the operation, the new status is computed.
	 *
	 * @param frame [out] Read frame
	 * @return FIFO status
	 */
	T_Fifo_Status readElement(BiteFrame *frame);

private:

	BiteFrame fifo_table[BITE_MGT_RCV_FIFO_SIZE];
};

#endif /* SRC_ASW_BITE_GENERIC_BITEFIFO_H_ */
