/*!
 * @file BiteInteractive.h
 *
 * @brief Interactive BITE header file
 *
 * @date Tue Jun  7 17:05:17     2022�c. 2020
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Jun  7 17:05:17     2022 : 61-definition-de-la-valeur-max-de-la-retval-dans-biteinteractive 
 * Thu May 26 16:35:16     2022 : 71-changement-periode-de-l-autoswitch-de-l-affichage 
 * Tue Apr  5 20:18:07     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Thu Feb 10 21:05:48     2022 : 59-passage-en-allocation-statique 
 * Thu Dec 23 18:17:13     2021 : 50-corrections-sonar 
 * Wed Dec  1 16:54:56     2021 : 43-ajout-changement-valeur-wdg-dans-bite 
 * 
 */

#ifndef SRC_ASW_BITE_MANAGER_GENERIC_BITEINTERACTIVE_H_
#define SRC_ASW_BITE_MANAGER_GENERIC_BITEINTERACTIVE_H_


/*!
 * @brief Type defining a pointer to function
 */
typedef bool (*BiteInteractive_funcPtr)(uint32_t param, uint8_t *const retval);

/*!
 * @brief BITE Interactive configuration element
 * @details This structure defines a BITE Interactive command element
 *
 */
typedef struct
{
	uint8_t cmd; /*!< Command ID */
	BiteInteractive_funcPtr fct_ptr; /*!< Pointer to the function executing the associated action */
	uint8_t retsize; /*!< Size in bytes of the returned data */
} T_BITE_Interactive_data;

#include "../config/BiteInteractive_cnf.h"

/*!
 * @brief Interactive BITE class
 * @details This class manages the interactive BITE Service.
 *          It inherits from Service abstract class.
 *          It uses functions from BiteManager class to send and receive BITE frames.
 */
class BiteInteractive : public Service
{
public:

	/*!
	 * @brief Class constructor
	 * @details This class initializes the class BiteInteractive.
	 *
	 * @return Nothing
	 */
	BiteInteractive();

	/*!
	 * @brief Service initialization function
	 * @details This function initializes the service BiteInteractive
	 *
	 * @return True if BiteManager service is ready, false otherwise
	 */
	virtual bool initService();

	/*!
	 * @brief Frame processing function.
	 * @details This function processes the action requested by the received frame.
	 *          If the action has been executed, an ACK frame is sent, if not, an NAK frame is sent.
	 *
	 * @param [in] frame Received frame object
	 * @return Nothing
	 */
	void processReceivedFrame(BiteFrame *frame);

	/*!
	 * @brief Sends a request to the host interface
	 * @details This function sends a request frame to the host interface
	 *
	 * @param [in] cmd_id ID of the command to send
	 * @return Nothing
	 */
	void sendRequest(uint8_t cmd_id);

private:

	/*!
	 * @brief Structure representing a request sent to host computer
	 *
	 */
	typedef struct
	{
		uint8_t id; /*!< ID of the request */
		uint32_t ts; /*!< Timestamp of the request */
		bool isActive; /*!< request active flag */
	} T_BiteInteractive_Request;

	T_BiteInteractive_Request savedRequest; /*!< Last request saved */

	uint8_t retval[BITE_INTERACTIVE_DATA_RETVAL_MAX_SIZE]; /*!< Interactive actions return value */
};

#endif /* SRC_ASW_BITE_MANAGER_GENERIC_BITEINTERACTIVE_H_ */
