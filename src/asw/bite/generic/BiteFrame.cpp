/*!
 * @file BiteFrame.cpp
 *
 * @brief BiteFrame class source code file
 *
 * @date Tue Apr  5 20:18:06     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:06     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Thu Feb 10 20:26:59     2022 : 59-passage-en-allocation-statique 
 * Mon Jan 24 23:44:44     2022 : 57-corrections-bugs-bite 
 * Mon Jan 24 20:49:54     2022 : 48-synchro-host-avec-identification 
 * Wed Jan  5 20:55:30     2022 : 51-optimisation-des-typages 
 * Wed Jan  5 17:01:25     2022 : 50-corrections-sonar 
 * 
 */

#include <avr/io.h>

#include "../../../lib/common_lib/generic/common_types.h"
#include "../../../lib/common_lib/generic/mem_tools.h"
#include "../../../lib/fifo/generic/Fifo.h"

#include "../../../os/service/generic/PeriodicService.h"
#include "../../../bsw/usart/generic/Usart.h"
#include "../../../bsw/dio/generic/Dio.h"
#include "BiteFrame.h"
#include "BiteManager.h"
#include "../config/BiteManager_cnf.h"
#include "../config/BiteManager_frameConfig_cnf.h"



BiteFrame::BiteFrame()
{
	paramSize = 0;
	size = 8;
}

void BiteFrame::initFrame(uint8_t l_type, uint8_t l_status, uint8_t l_cmd)
{
	frame[BITE_MGT_FRAME_POS_STX] = CHAR_STX;
	frame[BITE_MGT_FRAME_POS_TYPE] = l_type;
	frame[BITE_MGT_FRAME_POS_STATUS] = l_status;
	frame[BITE_MGT_FRAME_POS_CMD] = l_cmd;

	frame[BITE_MGT_FRAME_POS_SIZE] = 0;
	paramSize = 0;

	size = 8;

	frame[BITE_MGT_FRAME_POS_FFETX] = 0xff;
	frame[BITE_MGT_FRAME_POS_ETX] = CHAR_ETX;
}


uint8_t* BiteFrame::getFrame()
{
	/* Compute Checksum */
	frame[BITE_MGT_FRAME_POS_CKS] = getChecksum();

	return frame;
}

uint8_t BiteFrame::getChecksum() const
{
	uint8_t cks = (uint8_t) (CHAR_ETX + CHAR_STX + ((uint8_t) 0xFF) + frame[BITE_MGT_FRAME_POS_TYPE]
	        + frame[BITE_MGT_FRAME_POS_STATUS] + frame[BITE_MGT_FRAME_POS_CMD] + paramSize);

	if (paramSize > 0)
	{
		for (uint8_t i = 0; i < paramSize; i++)
			cks += frame[BITE_MGT_FRAME_POS_PARAM + i];
	}

	return cks;
}

uint8_t* BiteFrame::getParamPtr()
{
	if (paramSize > 0)
		return &(frame[BITE_MGT_FRAME_POS_PARAM]);
	else
		return 0;
}

void BiteFrame::setParam(const uint8_t *l_param, uint8_t l_param_size)
{
	/* If the PARAM table is too big, exit the function */
	if (l_param_size > BITE_MGT_FRAME_PARAM_MAX_SIZE)
		return;

	/* If the PARAM table has already been set, exit the function */
	if (paramSize != 0)
		return;

	/* Update frame size */
	paramSize = l_param_size;
	size += paramSize;
	frame[BITE_MGT_FRAME_POS_SIZE] = paramSize;

	/* Replace ETX at the correct location */
	frame[BITE_MGT_FRAME_POS_FFETX] = 0xff;
	frame[BITE_MGT_FRAME_POS_ETX] = CHAR_ETX;

	/* Copy data into the PARAM table */
	mem_copy(l_param, &(frame[BITE_MGT_FRAME_POS_PARAM]), paramSize);
}

