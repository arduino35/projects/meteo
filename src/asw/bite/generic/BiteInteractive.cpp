/*!
 * @file BiteInteractive.cpp
 *
 * @brief Interactive BITE source file
 *
 * @date Thu May 26 16:57:49     2022�c. 2020
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Thu May 26 16:57:49     2022 : 71-changement-periode-de-l-autoswitch-de-l-affichage 
 * Tue Apr  5 20:18:07     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Wed Feb 16 20:06:48     2022 : 59-passage-en-allocation-statique 
 * Tue Jan 25 11:35:43     2022 : 57-corrections-bugs-bite 
 * Thu Dec 23 18:17:13     2021 : 50-corrections-sonar 
 * Mon Dec 20 16:04:31     2021 : 49-emission-des-requetes-bite-interactif-seulement-si-le-bite-est-ready 
 * Wed Dec  1 16:54:56     2021 : 43-ajout-changement-valeur-wdg-dans-bite 
 * 
 */

#include <avr/io.h>

#include "../../../lib/common_lib/generic/mem_tools.h"
#include "../../../os/service/generic/PeriodicService.h"
#include "../../../os/scheduler/generic/Scheduler.h"

#include "../../asw_manager/generic/asw_manager.h"
#include "BiteInteractive.h"

#include "../config/BiteManager_frameConfig_cnf.h"
#include "../config/BiteInteractive_cnf.h"
#include "../config/BiteManager_cnf.h"






BiteInteractive::BiteInteractive()
{
	savedRequest.id = 0;
	savedRequest.ts = 0;
	savedRequest.isActive = false;
}

bool BiteInteractive::initService()
{
	savedRequest.id = 0;
	savedRequest.ts = 0;
	savedRequest.isActive = false;

	/* Check that BiteManager is ready */
	if (p_global_ASW_manager.isServiceInitialized(BITE))
		return true;
	else
		return false;
}

void BiteInteractive::processReceivedFrame(BiteFrame *frame)
{
	/* If frame TYPE is REQ, perform an action */
	if (frame->getType() == BITE_MGT_CMD_TYPE_REQ)
	{

		/* Check if the ID exists in the configuration table */
		uint8_t idx = 0;
		while ((bite_Interactive_data_tab[idx].cmd != frame->getCmd()) && (idx < BITE_INTERACTIVE_DATA_TAB_SIZE))
			idx++;

		/* If the ID has been found in the table */
		if (idx < BITE_INTERACTIVE_DATA_TAB_SIZE)
		{
			/* Execute action */
			uint32_t param = 0;

			/* Get param value if it exists */
			if (frame->getParamSize() == 1)
				param = (frame->getParamPtr())[0];
			else if (frame->getParamSize() == 4)
				invertEndianness(frame->getParamPtr(), (uint8_t*) (&param), 4);

			if (bite_Interactive_data_tab[idx].fct_ptr(param, retval))
			{
				((BiteManager*) p_global_ASW_manager.getPeriodicServicePointer(BITE))->buildFrame(BITE_MGT_CMD_TYPE_ANS,
				        BITE_MGT_CMD_STATUS_ACK, frame->getCmd(), retval,
				        bite_Interactive_data_tab[idx].retsize);
			}
			else
			{
				((BiteManager*) p_global_ASW_manager.getPeriodicServicePointer(BITE))->buildFrame(BITE_MGT_CMD_TYPE_ANS,
				        BITE_MGT_CMD_STATUS_NAK, frame->getCmd());
			}
		}
		/* Else send NAK frame */
		else
		{
			((BiteManager*) p_global_ASW_manager.getPeriodicServicePointer(BITE))->buildFrame(BITE_MGT_CMD_TYPE_ANS,
			        BITE_MGT_CMD_STATUS_NAK, frame->getCmd());
		}
	}
	/* If frame TYPE is ANS, check the coherency with the pending request and execute action */
	else if ((frame->getType() == BITE_MGT_CMD_TYPE_ANS) && (frame->getStatus() == BITE_MGT_CMD_STATUS_ACK)
	        && ((frame->getCmd() == savedRequest.id) && (savedRequest.isActive == true)))
	{
		/* Check if the ID exists in the configuration table */
		uint8_t idx = 0;
		while ((bite_Interactive_data_tab[idx].cmd != savedRequest.id) && (idx < BITE_INTERACTIVE_DATA_TAB_SIZE))
			idx++;

		/* If the ID has been found in the table */
		/* Check that PARAM table has the expected size */
		if ((idx < BITE_INTERACTIVE_DATA_TAB_SIZE) && (frame->getParamSize() == bite_Interactive_data_tab[idx].retsize))
		{
			/* Execute action */
			uint8_t param = 0;
			mem_copy(frame->getParamPtr(), retval, bite_Interactive_data_tab[idx].retsize);

			bite_Interactive_data_tab[idx].fct_ptr(param, retval);
			savedRequest.isActive = false;
		}
	}
	/* Else send NAK frame */
	else
		((BiteManager*) p_global_ASW_manager.getPeriodicServicePointer(BITE))->buildFrame(BITE_MGT_CMD_TYPE_ANS,
		        BITE_MGT_CMD_STATUS_NAK, frame->getCmd());
}

void BiteInteractive::sendRequest(uint8_t cmd_id)
{
	/* Request are sent only if BITE status is READY */
	/* New request is sent only if there is no pending request, or if it is the same id than the pending request */
	if ((((BiteManager*) p_global_ASW_manager.getPeriodicServicePointer(BITE))->isBiteStarted())
	        && ((savedRequest.isActive == false) || (savedRequest.id == cmd_id)))
	{
		savedRequest.id = cmd_id;
		savedRequest.ts = p_global_scheduler.getPitNumber();
		savedRequest.isActive = true;

		((BiteManager*) p_global_ASW_manager.getPeriodicServicePointer(BITE))->buildFrame(BITE_MGT_CMD_TYPE_REQ, 0,
		        cmd_id);
	}
}

