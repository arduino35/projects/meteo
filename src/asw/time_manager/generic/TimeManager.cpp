/*!
 * @file TimeManager.cpp
 *
 * @brief Class TimeManager source file
 *
 * @date Fri Jul  1 18:06:58     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Jul  1 18:06:58     2022 : 74-affichage-valeurs-mix-max-capteurs 
 * Fri May  6 19:10:51     2022 : 69-synchro-jour-de-la-semaine 
 * Fri May  6 17:34:33     2022 : 67-rendre-generique-affichage-ecran 
 * Fri Apr 29 11:34:43     2022 : 60-modification-gestion-periode-des-services 
 * Tue Apr 12 18:30:42     2022 : 63-rendre-generique-le-formatage-des-donnees-periodiques 
 * Tue Apr  5 20:18:15     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Wed Feb 16 20:05:05     2022 : 59-passage-en-allocation-statique 
 * Fri Feb  4 08:51:55     2022 : 57-corrections-bugs-bite 
 * Thu Dec 23 20:44:56     2021 : 50-corrections-sonar 
 * Mon Dec 20 11:30:57     2021 : 46-supression-de-la-synchro-date-dans-timemanager-getvalidity 
 * 
 */

#include <avr/io.h>

#include "../../../lib/string/generic/String.h"

#include "../../../os/service/generic/PeriodicService.h"

#include "../../../bsw/bsw_manager/config/bsw_manager_cnf.h"
#include "../../../bsw/bsw_manager/generic/bsw_manager.h"

#include "../../../os/scheduler/generic/Scheduler.h"

#include "../../asw_manager/generic/asw_manager.h"
#include "../../bite/generic/BiteInteractive.h"
#include "../../bite/generic/BiteManager.h"
#include "../../bite/config/BiteInteractive_cnf.h"

#include "../config/TimeManager_cnf.h"
#include "TimeManager.h"

#define TIMEMANAGER_COUNTER_TICK_DURATION_US	(uint32_t)(TIMEMANAGER_TIMER_PRESCALER_VALUE*1000000/F_CPU)	/*!< Duration of one clock tick in �s */
#define TIMEMANAGER_DATE_SYNCHRO_CYCLES			(TIMEMANAGER_DATE_SYNCHRO_PERIOD/TIMEMANAGER_TASK_PERIOD)	/*!< Period of date synchro in cycles */
#define TIMEMANAGER_UNSYNC_DELAY_CYCLES			(uint32_t)((uint32_t)TIMEMANAGER_UNSYNC_DELAY_HOURS*(uint32_t)60*(uint32_t)60*(uint32_t)1000/(uint32_t)SW_PERIOD_MS)	/*!< Delay in cycles after which date becomes invalid if no sync has been done with host */


const uint8_t *monthsStr[] = { (const uint8_t*) "Janvier", (const uint8_t*) "Fevrier", (const uint8_t*) "Mars",
        (const uint8_t*) "Avril", (const uint8_t*) "Mai", (const uint8_t*) "Juin", (const uint8_t*) "Juillet",
        (const uint8_t*) "Aout", (const uint8_t*) "Septembre", (const uint8_t*) "Octobre", (const uint8_t*) "Novembre",
        (const uint8_t*) "Decembre" };

const uint8_t *daysStr[] = { (const uint8_t*) "Lundi", (const uint8_t*) "Mardi", (const uint8_t*) "Mercredi",
        (const uint8_t*) "Jeudi", (const uint8_t*) "Vendredi", (const uint8_t*) "Samedi", (const uint8_t*) "Dimanche" };



TimeManager::TimeManager() : PeriodicService(TIMEMANAGER_TASK_PERIOD)
{
	/* Initialize current time */
	current_time.hours = 0;
	current_time.minutes = 0;
	current_time.seconds = 0;
	current_date.day = 1;
	current_date.month = 1;
	current_date.year = 1990;
	current_date.dayName = 0;

	synchro_counter = TIMEMANAGER_DATE_SYNCHRO_CYCLES - 1;
	synchro_done = false;
	last_sync_ts = 0;
	invalidCounter = 10;
	elapsed_us = 0;
}

bool TimeManager::initService()
{
	bool status;

	/* Driver initialization */
	status = initPeriodicService(TIMEMANAGER_TASK_PERIOD);
	status &= p_global_BSW_manager.initializeDriver(TIMER);

	if (status)
	{
		/* Timer #4 configuration */
		((Timer*) (p_global_BSW_manager.getDriverPointer(TIMER)))->configureTimer4(TIMEMANAGER_TIMER_PRESCALER_VALUE,
		        0);

		/* Initialize current time */
		current_time.hours = 0;
		current_time.minutes = 0;
		current_time.seconds = 0;
		current_date.day = 1;
		current_date.month = 1;
		current_date.year = 1990;
		current_date.dayName = 0;

		synchro_counter = TIMEMANAGER_DATE_SYNCHRO_CYCLES - 1;
		synchro_done = false;
		last_sync_ts = 0;
		invalidCounter = 10;
		elapsed_us = 0;

		/* Start periodic task */
		status = start();

		return status;
	}
	return false;
}

void TimeManager::run()
{
	const Timer *timer_drv_ptr = (Timer*) (p_global_BSW_manager.getDriverPointer(TIMER));

	/* Get timer value and reset counter */
	uint16_t counter = timer_drv_ptr->getTimer4Value();
	timer_drv_ptr->resetTimer4();

	/* Compute the number of microseconds elapsed since last call */
	elapsed_us += (counter * TIMEMANAGER_COUNTER_TICK_DURATION_US );

	/* Update time structure */
	while (elapsed_us >= 1000000)
	{
		current_time.seconds++;
		elapsed_us -= 1000000;
	}
	while (current_time.seconds >= 60)
	{
		current_time.minutes++;
		current_time.seconds -= 60;
	}
	while (current_time.minutes >= 60)
	{
		current_time.hours++;
		current_time.minutes -= 60;
	}
	while (current_time.hours >= 24)
	{
		current_date.day++;
		current_date.dayName = (current_date.dayName + 1) % 7;
		current_time.hours -= 24;
	}

	/* Compute current month days count */
	uint8_t day_count;
	switch (current_date.month)
	{
	case 1:
	case 3:
	case 5:
	case 7:
	case 8:
	case 10:
	case 12:
		day_count = 31;
		break;
	case 2:
		day_count = 28;
		break;
	default:
		day_count = 30;
		break;
	}

	while (current_date.day > day_count)
	{
		current_date.month++;
		current_date.day -= day_count;
	}
	while (current_date.month > 12)
	{
		current_date.year++;
		current_date.month -= 12;
	}

	/* Synchro with host */
	if (((BiteManager*) (p_global_ASW_manager.getServicePointer(BITE)))->isBiteStarted())
	{
		if (synchro_done)
		{
			if (synchro_counter >= TIMEMANAGER_DATE_SYNCHRO_CYCLES)
				sendBiteSynchroRequest();
			else
				synchro_counter++;
		}
		else
			sendBiteSynchroRequest();
	}
}

void TimeManager::sendBiteSynchroRequest()
{
	((BiteInteractive*) (p_global_ASW_manager.getServicePointer(BITE_INTERACTIVE)))->sendRequest(
	        BITE_INTERACTIVE_ID_DATESYNCHRO_REQUEST);
	synchro_counter = 0;
}

bool TimeManager::start()
{
	const Timer *timer_drv_ptr = (Timer*) (p_global_BSW_manager.getDriverPointer(TIMER));

	if (!isServiceRunning())
	{
		/* Reset and start timer */
		timer_drv_ptr->resetTimer4();
		timer_drv_ptr->startTimer4();

		/* Add periodic task in scheduler */
		p_global_scheduler.addPeriodicTask(this);

		setServiceRunning(true);
		return true;
	}
	else
		return false;
}

bool TimeManager::pause()
{
	const Timer *timer_drv_ptr = (Timer*) (p_global_BSW_manager.getDriverPointer(TIMER));

	if (isServiceRunning())
	{
		/* Remove task from scheduler */
		if (p_global_scheduler.removePeriodicTask(this))
		{
			/* Stop timer */
			timer_drv_ptr->stopTimer4();

			setServiceRunning(false);
			synchro_done = false;
			return true;
		}
		else
			return false;
	}
	else
		return false;
}

bool TimeManager::getValidity()
{
	bool status;

	if (isServiceRunning() && synchro_done)
	{
		uint32_t pit_number = p_global_scheduler.getPitNumber();
		if ((last_sync_ts <= pit_number)
			&& ((pit_number - last_sync_ts) < TIMEMANAGER_UNSYNC_DELAY_CYCLES ))
			status = true;

		else
		{
			if (pit_number > (TIMEMANAGER_DATE_SYNCHRO_PERIOD / SW_PERIOD_MS))
				status = false;
			else
				status = true;
		}
	}
	else
		status = false;

	if (status == true)
	{
		invalidCounter = 0;
		return true;
	}
	else
	{
		/* Force a synchro request if time has been invalid for less than 3 cycles and initial synchro is done */
		if (invalidCounter > 5)
			return false;
		else
		{
			sendBiteSynchroRequest();
			invalidCounter++;
			return true;
		}
	}
}

bool TimeManager::updateTime(T_TimeManager_time newTime, T_TimeManager_date newDate)
{
	current_time = newTime;
	current_date = newDate;
	synchro_done = true;
	last_sync_ts = p_global_scheduler.getPitNumber();

	return true;
}

bool TimeManager::formatBiteData(uint8_t *data, uint8_t group, uint8_t id, T_Service_value_type type)
{
	if (getValidity() && (type == CURRENT))
	{
		data[0] = current_time.hours;
		data[1] = current_time.minutes;
		data[2] = current_time.seconds;

		data[3] = current_date.day;
		data[4] = current_date.month;
		data[5] = (uint8_t) (current_date.year / (uint16_t) 100);
		data[6] = (uint8_t) (current_date.year % (uint16_t) 100);
		data[7] = current_date.dayName;

		return true;
	}
	else
		return false;
}

void TimeManager::displayFormatting(const T_TimeManager_config *config, String *str)
{
	str->clear();

	/* Status formatting */
	if (config->status && !getValidity())
	{
		str->initString((const uint8_t*) "Date invalide");
		return;
	}

	/* Day name formatting */
	if (config->dayNameOn)
		str->appendString(daysStr[current_date.dayName]);

	if (config->dateOn && config->dayNameOn)
		str->appendString((const uint8_t*) "   ");

	/* Date formatting */
	if (config->dateOn)
	{
		if (config->dateFormat == SHORT)
		{
			str->appendInteger(current_date.day, 10, 2);
			str->appendChar('/');
			str->appendInteger(current_date.month, 10, 2);
			str->appendChar('/');
			str->appendInteger(current_date.year, 10, 4);
		}
		else if (config->dateFormat == LONG)
		{
			str->appendInteger(current_date.day, 10, 2);
			str->appendChar(' ');
			str->appendString(monthsStr[current_date.month - 1]);
			str->appendChar(' ');
			str->appendInteger(current_date.year, 10, 4);
		}
	}

	if (config->dateOn && config->timeOn)
		str->appendString((const uint8_t*) "   ");

	if (config->timeOn)
	{
		str->appendInteger(current_time.hours, 10, 2);
		str->appendChar(':');
		str->appendInteger(current_time.minutes, 10, 2);

		if (config->secondsOn)
		{
			str->appendChar(':');
			str->appendInteger(current_time.seconds, 10, 2);
		}
	}
}
