/*!
 * @file TimeManager_cnf.h
 *
 * @brief TimeManager module configuration file
 *
 * @date Wed Feb 16 20:06:00     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Wed Feb 16 20:06:00     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 21:17:15     2022 : 51-optimisation-des-typages 
 * 
 */

#ifndef SRC_ASW_TIME_MANAGER_CONFIG_TIMEMANAGER_CNF_H_
#define SRC_ASW_TIME_MANAGER_CONFIG_TIMEMANAGER_CNF_H_

/*!< Task period in ms */
const t_time_ms TIMEMANAGER_TASK_PERIOD = 1000;

/*!< Timer prescaler value : biggest available value */
const uint16_t TIMEMANAGER_TIMER_PRESCALER_VALUE = 1024;

/*!< Synchronisation period with host computer (ms) */
const t_time_ms TIMEMANAGER_DATE_SYNCHRO_PERIOD = 60000;

/*!< Delay in hours after which date becomes invalid if no sync has been done with host */
const uint32_t TIMEMANAGER_UNSYNC_DELAY_HOURS = 12;

/*!< Debug for TIME module activation flag */
//#define TIME_DEBUG_ENABLED


#endif /* SRC_ASW_TIME_MANAGER_CONFIG_TIMEMANAGER_CNF_H_ */
