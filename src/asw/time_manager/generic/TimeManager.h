/*!
 * @file TimeManager.h
 *
 * @brief Class TimeManager header file
 *
 * @date Fri Jul  1 18:06:59     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Jul  1 18:06:59     2022 : 74-affichage-valeurs-mix-max-capteurs 
 * Fri May  6 19:10:51     2022 : 69-synchro-jour-de-la-semaine 
 * Fri May  6 17:34:34     2022 : 67-rendre-generique-affichage-ecran 
 * Tue Apr  5 21:17:21     2022 : 63-rendre-generique-le-formatage-des-donnees-periodiques 
 * Tue Apr  5 20:18:15     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Wed Feb 16 20:05:06     2022 : 59-passage-en-allocation-statique 
 * Fri Feb  4 08:51:56     2022 : 57-corrections-bugs-bite 
 * Thu Dec 23 20:44:57     2021 : 50-corrections-sonar 
 * 
 */

#ifndef SRC_ASW_TIME_MANAGER_GENERIC_TIMEMANAGER_H_
#define SRC_ASW_TIME_MANAGER_GENERIC_TIMEMANAGER_H_

#include "../../../lib/string/generic/String.h"

typedef enum
{
	SHORT,
	LONG
} T_TimeManager_dateFormat;

/**
 * @brief Date and time configuration structure
 *
 */
typedef struct
{
	bool dayNameOn;
	bool dateOn;
	T_TimeManager_dateFormat dateFormat;
	bool timeOn;
	bool secondsOn;
	bool status;
} T_TimeManager_config;

/**
 * @brief Structure representing time : hours, minutes, seconds
 */
typedef struct
{
	uint8_t hours;
	uint8_t minutes;
	uint8_t seconds;
} T_TimeManager_time;

/**
 * @brief Structure representing date : day, month, year
 */
typedef struct
{
	uint8_t dayName;
	uint8_t day;
	uint8_t month;
	uint16_t year;
} T_TimeManager_date;

/**
 * @brief Date and Time computation class
 * @details This class is in charge of computing date and time using CPU timers
 */
class TimeManager : public PeriodicService
{

public:

	/**
	 * @brief Class constructor
	 * @details This function is in charge of initializing the class and the timer
	 *
	 * @return Nothing
	 */
	TimeManager();

	/**
	 * @brief Service initialization function
	 * @details This function initializes the class and the associated drivers
	 *
	 * @return True if the initialization has been correctly performed, False otherwise
	 */
	virtual bool initService();

	/**
	 * @brief 	Time computation function
	 * @details This function computes date and time.
	 * 			Current timer value is retrieved using timer driver and timer is reset.
	 * 			Then time and date are computed.
	 *
	 * 	@return Nothing
	 */
	void run();

	/**
	 * @brief Service pausing function
	 * @details This function is used to pause the service.
	 *
	 * @return True is the service has been correctly paused, false otherwise.
	 */
	bool pause();

	/*!
	 * @brief Service start function
	 * @details This function is used to start the service after it has been paused or created.
	 *
	 * @return True is the service has been correctly started, false otherwise.
	 */
	bool start();

	/*!
	 * @brief Time getting function
	 * @details This function returns the current time structure
	 *
	 * @return Current time structure
	 */
	inline T_TimeManager_time getTime() const
	{
		return current_time;
	}

	/*!
	 * @brief Date getting function
	 * @details This function returns the current date structure
	 *
	 * @return Current date structure
	 */
	inline T_TimeManager_date getDate() const
	{
		return current_date;
	}

	/**
	 * @brief Data validity function
	 * @details This function returns if the time and date data are valid or not.
	 *          Validity is based on the service status (running/not running) and the syncho with host
	 *
	 * @return TRUE (valid) if the service is running, FALSE otherwise
	 */
	bool getValidity();

	/*!
	 * @brief Time update function
	 * @details This function updates the current date and time according to the given parameters
	 *
	 * @param [in] newTime New time value
	 * @param [in] newDate New date value
	 * @return TRUE if the time and date have been correctly updated, FALSE otherwise
	 */
	bool updateTime(T_TimeManager_time newTime, T_TimeManager_date newDate);

	/*!
	 * @brief BITE data formatting
	 * @details This function formats data for BITE transmission.
	 *
	 * @param [out] data Formatted data
	 * @param [in] group Not used
	 * @param [in] id Not used
	 * @param [in] type Not used
	 * @return True if the formatted data is valid, False otherwise
	 */
	bool formatBiteData(uint8_t *data, uint8_t group, uint8_t id, T_Service_value_type type);

	/*!
	 * @brief Display formatting function
	 * @details This function formats date and time data for display according to the given configuration
	 *
	 * @param [in] config Configuration structure
	 * @param [out] str Formatted string
	 *
	 * @return Nothing
	 */
	void displayFormatting(const T_TimeManager_config *config, String *str);

private:

	T_TimeManager_time current_time; /**< Current time structure */
	T_TimeManager_date current_date; /**< Current date structure */

	uint32_t elapsed_us; /**< Number of elapsed �s since last call */

	uint16_t synchro_counter; /*!< Number of cycles since last synchro */
	bool synchro_done; /*!< Synchro with host flag */
	uint32_t last_sync_ts; /*!< Timestamp of last synchro with host */
	uint8_t invalidCounter; /*!< Invalid cycles counter */

	/**
	 * @brief BITE synchro request function
	 * @details This function sends a synchro request to host through the Bite interactive
	 *
	 * @return Nothing
	 *
	 */
	void sendBiteSynchroRequest();

};

#endif /* SRC_ASW_TIME_MANAGER_GENERIC_TIMEMANAGER_H_ */
