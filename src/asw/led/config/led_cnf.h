/*!
 * @file led_cnf.h
 *
 * @brief LED service configuration file
 *
 * @date Tue Feb  8 17:23:15     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Feb  8 17:23:16     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 20:56:16     2022 : 51-optimisation-des-typages 
 * 
 */

#ifndef WORK_ASW_LED_CONFIG_LED_CNF_H_
#define WORK_ASW_LED_CONFIG_LED_CNF_H_


/*!< Recurrence of periodic task */
const t_time_ms PERIOD_MS_TASK_LED = 500;

/*!< Time in ms after reset with LED set to ON */
const t_time_ms RESET_ON_TIME_MS = 3000;

/*!< Blink period in ms */
const t_time_ms BLINK_PERIOD_MS = 5000;

/*!< LED is connected to port PB7 */
const uint8_t LED_PORT = ENCODE_PORT(PORT_B, 7);


#endif /* WORK_ASW_LED_CONFIG_LED_CNF_H_ */
