/*!
 * @file Led.h
 *
 * @brief Class Led header file
 *
 * @date Tue Apr  5 20:18:10     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:10     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Wed Feb  9 20:43:56     2022 : 59-passage-en-allocation-statique 
 * 
 */

#ifndef WORK_ASW_LED_LED_H_
#define WORK_ASW_LED_LED_H_

#include "../config/led_cnf.h"

/*!
 * @brief Class for keep-alive LED blinking
 * @details This class defines all functions to make keep-alive LED blink
 */
class Led: public PeriodicService
{
public:

	/*!
	 * @brief Class constructor
	 * @details This function initializes the class Led.
	 *
	 * @return Nothing
	 */
	Led();

	/*!
	 * @brief Service initialization function
	 * @details This function initializes the class Led. If needed, it creates a DIO driver object.
	 *
	 * @return Nothing
	 */
	virtual bool initService();

	/*!
	 * @brief Task for LED blinking
	 * @details This function is inserted into the scheduler.
	 * 			This function makes the LED blink according to the configured schedule : stays ON after reset and then
	 * 			one cycle ON and X cycles OFF
	 *
	 * @return Nothing
	 */
	void run();

	/*!
	 * @brief LED service pausing function
	 * @details This function is used to pause the service LED.
	 * 			It removes the periodic task from scheduler.
	 *
	 * @return True is the service has been correctly paused, false otherwise.
	 */
	bool pause();

	/*!
	 * @brief Led service start function
	 * @details This function is used to start the service LED after it has been paused.
	 * 			It adds the periodic task inside the scheduler.
	 *
	 * @return True is the service has been correctly started, false otherwise.
	 */
	bool start();

private:

	bool reset;
	uint8_t count;
};

#endif /* WORK_ASW_LED_LED_H_ */
