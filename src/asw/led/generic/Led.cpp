/*!
 * @file Led.cpp
 *
 * @brief Definition of function for class Led
 *
 * @date Fri Apr 29 11:34:43     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Apr 29 11:34:43     2022 : 60-modification-gestion-periode-des-services 
 * Tue Apr  5 20:18:09     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Feb  8 17:23:16     2022 : 59-passage-en-allocation-statique 
 * Thu Dec 23 16:39:59     2021 : 50-corrections-sonar 
 * 
 */

#include <avr/io.h>

#include "../../../os/service/generic/PeriodicService.h"
#include "../../../os/scheduler/generic/Scheduler.h"

#include "../../../bsw/bsw_manager/generic/bsw_manager.h"
#include "../../asw_manager/generic/asw_manager.h"

#include "Led.h"

Led::Led() : PeriodicService(PERIOD_MS_TASK_LED)
{
	reset = true;
	count = 0;
}

bool Led::initService()
{
	bool retval;

	reset = true;
	count = 0;

	retval = p_global_BSW_manager.initializeDriver(DIO);
	retval &= initPeriodicService(PERIOD_MS_TASK_LED);

	/* Start the service */
	start();

	return retval;
}

void Led::run()
{
	if(reset)
	{
		if(count >= (RESET_ON_TIME_MS/PERIOD_MS_TASK_LED))
		{
			reset = false;
			count = 1;

			/* Set LED to OFF state */
			((Dio*) (p_global_BSW_manager.getDriverPointer(DIO)))->dio_setPort(LED_PORT, false);
		}
		else
			count++;
	}
	else
	{
		if(count >= (BLINK_PERIOD_MS/PERIOD_MS_TASK_LED))
		{
			count = 0;

			/* Set LED to ON state */
			((Dio*) (p_global_BSW_manager.getDriverPointer(DIO)))->dio_setPort(LED_PORT, true);
		}
		else if (count == 1)
		{
			/* Set LED to OFF state */
			((Dio*) (p_global_BSW_manager.getDriverPointer(DIO)))->dio_setPort(LED_PORT, false);
			count++;
		}
		else
			count++;
	}
}




bool Led::pause()
{
	if (isServiceRunning())
	{
		if (p_global_scheduler.removePeriodicTask(this))
		{
			((Dio*) (p_global_BSW_manager.getDriverPointer(DIO)))->dio_setPort(LED_PORT, false);
			setServiceRunning(false);
			return true;
		}
		else
			return false;
	}
	else
		return false;
}

bool Led::start()
{
	/* Set LED to ON state */
	((Dio*) (p_global_BSW_manager.getDriverPointer(DIO)))->dio_setPort(LED_PORT, true);

	if (!isServiceRunning())
	{
		p_global_scheduler.addPeriodicTask(this);
		setServiceRunning(true);
		return true;
	}
	else
		return false;
}
