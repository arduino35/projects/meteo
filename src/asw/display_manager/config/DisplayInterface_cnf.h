/*!
 * @file DisplayInterface_cnf.h
 *
 * @brief Display Interface configuration file
 *
 * @date Wed Feb  9 20:43:12     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Wed Feb  9 20:43:12     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 20:55:53     2022 : 51-optimisation-des-typages 
 * 
 */

#ifndef WORK_ASW_DISPLAY_IFT_CONFIG_DISPLAYINTERFACE_CNF_H_
#define WORK_ASW_DISPLAY_IFT_CONFIG_DISPLAYINTERFACE_CNF_H_


/*!< In "line shift" mode for line display, line is shifted every 500 ms */
const t_time_ms DISPLAY_LINE_SHIFT_PERIOD_MS = 500;

/*!< In "line shift" mode for line display, a temporization of 6 periods is added at the end and the beginning of the lines */
const uint8_t DISPLAY_LINE_SHIFT_TEMPO_TIME = 6;


#endif /* WORK_ASW_DISPLAY_IFT_CONFIG_DISPLAYINTERFACE_CNF_H_ */
