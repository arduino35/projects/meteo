/*!
 * @file DisplayManagement_cnf.cpp
 *
 * @brief Display management configuration file
 *
 * @date Fri Jul  1 18:29:51     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Jul  1 18:29:51     2022 : 76-temps-avant-changement-de-page-variable 
 * Fri Jul  1 18:07:15     2022 : 74-affichage-valeurs-mix-max-capteurs 
 * Fri May  6 19:07:07     2022 : 69-synchro-jour-de-la-semaine 
 * Fri May  6 18:32:52     2022 : 67-rendre-generique-affichage-ecran 
 * Tue Apr  5 20:18:08     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Wed Feb  9 20:43:13     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 20:55:54     2022 : 51-optimisation-des-typages 
 * 
 */

#include <stdlib.h>
#include <avr/io.h>

#include "../../../lib/common_lib/generic/common_types.h"
#include "../../../lib/string/generic/String.h"

#include "../../../os/service/generic/PeriodicService.h"
#include "../../../bsw/i2c/generic/I2C.h"
#include "../../../bsw/lcd/generic/LCD.h"

#include "../../time_manager/generic/TimeManager.h"
#include "../../sensors/sensors_mgt/generic/SensorManagement.h"
#include "../generic/DisplayInterface.h"
#include "../generic/DisplayManagement.h"

/*!
 * @brief LCD configuration structure
 * @details This structure defines the initial configuration of the LCD screen.
 */
const T_LCD_conf_struct LCD_init_cnf = {
	DISPLAY_MGT_I2C_BITRATE,
	DISPLAY_MGT_LCD_I2C_ADDR,
	LCD_CNF_BACKLIGHT_ON,
	LCD_CNF_TWO_LINE,
	LCD_CNF_FONT_5_8,
	LCD_CNF_DISPLAY_ON,
	LCD_CNF_CURSOR_OFF,
	LCD_CNF_CURSOR_BLINK_OFF,
	LCD_CNF_ENTRY_MODE_DIRECTION_RIGHT,
	LCD_CNF_ENTRY_MODE_DISPLAY_SHIFT_OFF
};


const uint8_t welcomeMessageString[] = "Bienvenue !"; /*!< String displayed on the screen at startup */
const uint8_t sizeof_welcomeMessageString = sizeof(welcomeMessageString);

const T_SensorManagement_DataConfig sensor1config = { 1, 1, CURRENT };
const T_SensorManagement_DataConfig sensor2config = { 1, 2, CURRENT };
const T_SensorManagement_DataConfig sensor3config = { 1, 3, CURRENT };

const T_LineCfgStruct page2linesCfg[] = { { DATA_SENSORS, 0, 0, &sensor1config, 0, LINE_SHIFT, LEFT }, { DATA_SENSORS,
        0, 0, &sensor2config, 1, LINE_SHIFT, LEFT }, { DATA_SENSORS, 0, 0,
        &sensor3config, 2,
        LINE_SHIFT, LEFT } };

const T_SensorManagement_DataConfig sensor1configMinMax = { 1, 1, MIN };
const T_SensorManagement_DataConfig sensor2configMinMax = { 1, 2, MIN };
const T_SensorManagement_DataConfig sensor3configMinMax = { 1, 3, MIN };

const T_LineCfgStruct page3linesCfg[] = { { DATA_SENSORS, 0, 0, &sensor1configMinMax, 0, LINE_SHIFT, LEFT }, {
        DATA_SENSORS, 0, 0, &sensor2configMinMax, 1, LINE_SHIFT, LEFT }, { DATA_SENSORS, 0, 0, &sensor3configMinMax, 2,
        LINE_SHIFT, LEFT } };

const T_TimeManager_config line1DateCfg =
{
		true, false, SHORT, false, false, false };

const T_TimeManager_config line2DateCfg = {
	false, true,
	LONG,
	false,
	false,
	false
};

const T_TimeManager_config line3DateCfg =
{
		false,
	false,
	SHORT,
	true,
	true, false
};

const T_TimeManager_config line4DateCfg = { false, false, SHORT, false, false, true };

const T_LineCfgStruct page1linesCfg[] = { { DATA_TIME, 0, &line1DateCfg, 0, 0, GO_TO_NEXT_LINE, CENTER }, { DATA_TIME,
		0, &line2DateCfg, 0, 1, GO_TO_NEXT_LINE, CENTER},
        { DATA_TIME, 0, &line3DateCfg, 0, 2, GO_TO_NEXT_LINE, CENTER },
	{	DATA_TIME, 0, &line4DateCfg, 0, 3, GO_TO_NEXT_LINE, CENTER}};

const T_ScreenCfgStruct screensCfg[] = {
	{ page1linesCfg, sizeof(page1linesCfg) / sizeof(T_LineCfgStruct), 6000 }, {
        page2linesCfg, sizeof(page2linesCfg) / sizeof(T_LineCfgStruct), 6000 }, { page3linesCfg, sizeof(page3linesCfg)
        / sizeof(T_LineCfgStruct), 3000 }
};

const T_MainDisplayCfgStruct mainDisplayCfgStruct = { screensCfg, sizeof(screensCfg) / sizeof(T_ScreenCfgStruct), true };
