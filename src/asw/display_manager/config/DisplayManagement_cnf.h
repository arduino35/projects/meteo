/*!
 * @file DisplayManagement_cnf.h
 *
 * @brief Display management configuration header file
 *
 * @date Fri Jul  1 16:58:29     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Jul  1 16:58:29     2022 : 75-changement-de-page-sur-pushbutton 
 * Thu May 26 16:35:30     2022 : 71-changement-periode-de-l-autoswitch-de-l-affichage 
 * Fri May  6 17:16:03     2022 : 67-rendre-generique-affichage-ecran 
 * Tue Mar  1 18:42:44     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 20:55:55     2022 : 51-optimisation-des-typages 
 * 
 */

#ifndef WORK_ASW_DISPLAY_MGT_CONFIG_DISPLAYMANAGEMENT_CNF_H_
#define WORK_ASW_DISPLAY_MGT_CONFIG_DISPLAYMANAGEMENT_CNF_H_


/*!< I2C address of the screen */
const uint8_t DISPLAY_MGT_LCD_I2C_ADDR = 0x27;

/*!< Display is updated every 1s */
const t_time_ms DISPLAY_MGT_PERIOD_TASK = 1000;

/*!< Time after which one the welcome message is removed */
const t_time_ms DISPLAY_MGT_PERIOD_WELCOME_MSG_REMOVAL = 5000;

/*!< Sensors data are displayed starting on line 0 */
const uint8_t DISPLAY_MGT_FIRST_LINE_SENSORS = 0;

/*!< I2C bus bitrate is 100 kHz */
const uint32_t DISPLAY_MGT_I2C_BITRATE = 100000;

/*!< External interrupt for manual switching is on INT4 */
const uint8_t DISPLAY_MGT_EXT_INT_PIN = 4;

/*!< Definition of the sensor group used for display */
#define DISPLAY_MGT_SENSOR_GROUP_ID SENSOR_MGT_METEO_GROUP_ID /*!< Definition of the sensor group used for display */

extern const T_LCD_conf_struct LCD_init_cnf;
extern const uint8_t welcomeMessageString[];
extern const uint8_t sizeof_welcomeMessageString;

extern const T_MainDisplayCfgStruct mainDisplayCfgStruct;

#endif /* WORK_ASW_DISPLAY_MGT_CONFIG_DISPLAYMANAGEMENT_CNF_H_ */
