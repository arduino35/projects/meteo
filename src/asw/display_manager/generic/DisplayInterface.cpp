/*!
 * @file DisplayInterface.cpp
 *
 * @brief Source code file for display services
 *
 * @date Fri May  6 19:25:10     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri May  6 19:25:11     2022 : 69-synchro-jour-de-la-semaine 
 * Tue Apr  5 20:18:08     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Apr  1 17:20:45     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 20:55:53     2022 : 51-optimisation-des-typages 
 * Wed Jan  5 18:34:37     2022 : 50-corrections-sonar 
 * 
 */

#include <stdlib.h>
#include <avr/io.h>
#include <util/delay.h>

#include "../../../lib/common_lib/generic/mem_tools.h"

#include "../../../os/service/generic/PeriodicService.h"
#include "../../../os/scheduler/generic/Scheduler.h"

#include "../../../bsw/bsw_manager/generic/bsw_manager.h"
#include "../../asw_manager/generic/asw_manager.h"

#include "DisplayInterface.h"


DisplayInterface::DisplayInterface() : PeriodicService(DISPLAY_LINE_SHIFT_PERIOD_MS)
{
	isShiftInProgress = false;
	p_lcd = 0;
}

bool DisplayInterface::initService(const T_LCD_conf_struct *LCD_cnf)
{
	bool status = initPeriodicService(DISPLAY_LINE_SHIFT_PERIOD_MS);
	status &= p_global_BSW_manager.initializeDriver(LCD_SCREEN);

	if (status)
	{
		p_lcd = (LCD*) (p_global_BSW_manager.getDriverPointer(LCD_SCREEN));
		p_lcd->configure(LCD_cnf);

		/* Initialize display data */
		for (uint8_t i = 0; i < LCD_SIZE_NB_LINES; i++)
		{
			display_data[i].mode = NORMAL;
			display_data[i].isEmpty = true;
			display_data[i].alignment = LEFT;

			clearStringInDataStruct(i);

			display_data[i].shift_data.str_cur_ptr = 0;
			display_data[i].shift_data.str.initString();
			display_data[i].shift_data.temporization = 0;
		}

		isShiftInProgress = false;
	}

	return status;
}

bool DisplayInterface::displayFullLine(uint8_t* str, uint8_t size, uint8_t line, T_DisplayInterface_LineDisplayMode mode, T_DisplayInterface_LineAlignment alignment)
{
	/* Check that the line number is correct, if it's incorrect, exit the function */
	if (line >= LCD_SIZE_NB_LINES)
		return false;

	/* Size can't be zero */
	if(size == 0)
	{
		clearLine(line);
		return false;
	}

	/* If all characters can be displayed on one line, switch mode to NORMAL */
	if (size <= LCD_SIZE_NB_CHAR_PER_LINE)
		mode = NORMAL;
	/* If there are too many characters and mode is NORMAL, switch mode to LINE_SHIFT */
	else if ((size > LCD_SIZE_NB_CHAR_PER_LINE) && (mode == NORMAL))
		mode = LINE_SHIFT;
	/* If next line mode is requested and we are on the last line, switch mode to line shift */
	else if((mode == GO_TO_NEXT_LINE) && (line == LCD_SIZE_NB_LINES - 1))
		mode = LINE_SHIFT;


	/* If the requested mode is LINE SHIFT and the line is already in LINE SHIFT mode,
	 * do not execute the function completely : only the string in shift data structure shall be updated */
	if((mode == LINE_SHIFT) && (display_data[line].mode == LINE_SHIFT))
	{
		display_data[line].shift_data.str.initString(str);

		/* The address of the string may have changed, replace pointer inside the range */
		if (display_data[line].shift_data.str_cur_ptr
		        > (display_data[line].shift_data.str.getString() + display_data[line].shift_data.str.getSize()
		                - LCD_SIZE_NB_CHAR_PER_LINE))
			display_data[line].shift_data.str_cur_ptr = display_data[line].shift_data.str.getString()
			        + display_data[line].shift_data.str.getSize() - LCD_SIZE_NB_CHAR_PER_LINE;
		else if (display_data[line].shift_data.str_cur_ptr < display_data[line].shift_data.str.getString())
			display_data[line].shift_data.str_cur_ptr = display_data[line].shift_data.str.getString();

		return true;
	}

	/* Clear the line to avoid issues in case a line shift is in progress on this line */
	clearLine(line);

	/* Update data */
	display_data[line].mode = mode;
	display_data[line].isEmpty = false;
	display_data[line].alignment = alignment;



	switch (mode)
	{
	case NORMAL:
	default:
		updateLineAndRefresh(str, size, line);
		break;

	case LINE_SHIFT:
		/* First write the first characters normally */
		updateLineAndRefresh(str, LCD_SIZE_NB_CHAR_PER_LINE, line);

		/* Update shift data structure */
		display_data[line].shift_data.str.initString(str);
		display_data[line].shift_data.str_cur_ptr = display_data[line].shift_data.str.getString();
		display_data[line].shift_data.temporization = DISPLAY_LINE_SHIFT_TEMPO_TIME;

		/* If no shift is in progress on another line, add periodic task to scheduler */
		if(isShiftInProgress == false)
		{
			start();
			isShiftInProgress = true;
		}

		break;

	case GO_TO_NEXT_LINE:
		/* First write the first characters normally */
		updateLineAndRefresh(str, LCD_SIZE_NB_CHAR_PER_LINE, line);

		/* Call the function recursively to write the remaining characters on the next lines */
		displayFullLine(&str[LCD_SIZE_NB_CHAR_PER_LINE], size - LCD_SIZE_NB_CHAR_PER_LINE, line + 1, GO_TO_NEXT_LINE, alignment);
		break;
	}

	return true;
}

bool DisplayInterface::displayFullLine(String* str, uint8_t line, T_DisplayInterface_LineDisplayMode mode, T_DisplayInterface_LineAlignment alignment)
{
	return displayFullLine(str->getString(), str->getSize(), line, mode, alignment);
}

void DisplayInterface::updateLineAndRefresh(const uint8_t *const str, uint8_t size, uint8_t line)
{
	/* Write each character in data structure */
	mem_copy(str, display_data[line].display_str, size);

	/* Update alignment */
	if(size < LCD_SIZE_NB_CHAR_PER_LINE)
		setLineAlignment(line);

	/* Refresh line */
	refreshLine(line);
}

void DisplayInterface::refreshLine(uint8_t line)
{
	/* Find DDRAM address of the start of the requested line */
	p_lcd->setDDRAMAddress(findFirstCharAddr(line));

	/* Write each character on the screen */
	for (uint8_t i = 0; i < LCD_SIZE_NB_CHAR_PER_LINE; i++)
		p_lcd->writeInRam(display_data[line].display_str[i], LCD_DATA_DDRAM);
}

void DisplayInterface::clearStringInDataStruct(uint8_t line)
{
	for (uint8_t i = 0; i < LCD_SIZE_NB_CHAR_PER_LINE; i++)
		display_data[line].display_str[i] = ' ';
}

uint8_t DisplayInterface::findFirstCharAddr(uint8_t line) const
{
	uint8_t start_addr = 0;
	uint8_t ram_offset;

	/* Parameter line is not checked against limits, it's assumed that the caller
	 * function has already checked the value */

	/* If we are in 1-line mode */
	if (p_lcd->GetLineNumberCnf() == LCD_CNF_ONE_LINE)
	{
		/* 1-line mode not managed for now */
	}
	else
	{
		/* First find in which line bank we are,
		 * calculate DDRAM offset and update line number to make the bank transparent */
		if ((line % 2) == 0)
		{
			/* We are in the first bank */
			ram_offset = LCD_RAM_2_LINES_MIN_1;
		}
		else
		{
			/* We are in the second bank */
			ram_offset = LCD_RAM_2_LINES_MIN_2;
		}

		line /= 2;

		/* Now we consider the RAM address goes from 0 to LCD_SIZE_NB_CHAR_PER_LINE * LCD_SIZE_NB_LINES / 2
		 * The start address is the product of the line number and the size of a line */
		start_addr = line * LCD_SIZE_NB_CHAR_PER_LINE;
		start_addr += ram_offset;
	}

	return start_addr;
}

bool DisplayInterface::clearLine(uint8_t line)
{
	bool isNextLineMode = false;

	/* Check that the line number is correct, if it's incorrect, exit the function */
	if (line >= LCD_SIZE_NB_LINES)
		return false;

	if(display_data[line].mode == GO_TO_NEXT_LINE)
		isNextLineMode = true;

	/* Free shift data string */
	if(display_data[line].mode == LINE_SHIFT)
		display_data[line].shift_data.str.clear();

	/* Set line mode to NORMAL */
	display_data[line].mode = NORMAL;

	/* Check if there is still some lines to shift, if no, remove the periodic task */
	isShiftInProgress = false;

	for (uint8_t i = 0; i < LCD_SIZE_NB_LINES; i++)
	{
		if(display_data[i].mode == LINE_SHIFT)
			isShiftInProgress = true;
	}

	if(isShiftInProgress == false)
		pause();

	/* Mark line as empty */
	display_data[line].isEmpty = true;

	/* Clear string in data structure and refresh display */
	clearStringInDataStruct(line);
	refreshLine(line);

	/* If the line was in next line mode, clear also the next line */
	if(isNextLineMode)
		clearLine(line + 1);

	return true;
}

void DisplayInterface::clearFullScreen()
{
	for (uint8_t i = 0; i < LCD_SIZE_NB_LINES; i++)
		clearLine(i);
}

bool DisplayInterface::isLineEmpty(uint8_t line) const
{
	/* Check that the line number is correct, if it's incorrect, exit the function */
	if (line >= LCD_SIZE_NB_LINES)
		return true;

	return display_data[line].isEmpty;
}

void DisplayInterface::run()
{
	T_Display_shift_data* display_shift_data_ptr;

	/* Process all lines of the screen */
	for (uint8_t i = 0; i < LCD_SIZE_NB_LINES; i++)
	{
		/* Check if the line shall be shifted */
		if (display_data[i].mode == LINE_SHIFT)
		{
			/* Update shift data pointer */
			display_shift_data_ptr = &(display_data[i].shift_data);

			/* Increment pointer and if we are at the end of the line, go back at the beginning */
			if (display_shift_data_ptr->str_cur_ptr
			        >= (display_shift_data_ptr->str.getString() + display_shift_data_ptr->str.getSize()
			                - LCD_SIZE_NB_CHAR_PER_LINE))
			{
				if(display_shift_data_ptr->temporization == 0)
				{
					display_shift_data_ptr->str_cur_ptr = display_shift_data_ptr->str.getString();
					display_shift_data_ptr->temporization = DISPLAY_LINE_SHIFT_TEMPO_TIME;
				}
				else
					display_shift_data_ptr->temporization--;
			}
			else if (display_shift_data_ptr->str_cur_ptr == display_shift_data_ptr->str.getString())
			{
				if(display_shift_data_ptr->temporization == 0)
				{
					display_shift_data_ptr->str_cur_ptr ++;
					display_shift_data_ptr->temporization = DISPLAY_LINE_SHIFT_TEMPO_TIME;
				}
				else
					display_shift_data_ptr->temporization--;
			}
			else
				display_shift_data_ptr->str_cur_ptr ++;

			/* Display the line */
			updateLineAndRefresh(display_shift_data_ptr->str_cur_ptr, LCD_SIZE_NB_CHAR_PER_LINE, i);
		}

	}

}


void DisplayInterface::setLineAlignment(uint8_t line)
{
	uint8_t char_idx;
	uint8_t new_str_idx;
	uint8_t new_str_idx_mem;
	uint8_t first_char_idx;
	uint8_t last_char_idx;
	uint8_t size;
	uint8_t tmp_str[LCD_SIZE_NB_CHAR_PER_LINE];

	uint8_t* str = display_data[line].display_str;

	switch(display_data[line].alignment)
	{
	case LEFT:
	default:
		/* Find the first character displayed */
		char_idx = 0;
		while((str[char_idx] == ' ') && (char_idx < LCD_SIZE_NB_CHAR_PER_LINE))
			char_idx++;

		/* No character have been found or the string is already aligned */
		if((char_idx >= LCD_SIZE_NB_CHAR_PER_LINE) || (char_idx == 0))
			return;

		/* Create the new string in a temporary table */
		mem_copy(&(str[char_idx]), tmp_str, (LCD_SIZE_NB_CHAR_PER_LINE - char_idx));

		/* Complete the string with spaces */
		for(new_str_idx = (LCD_SIZE_NB_CHAR_PER_LINE - char_idx); new_str_idx < LCD_SIZE_NB_CHAR_PER_LINE; new_str_idx++)
			tmp_str[new_str_idx] = ' ';

		/* Copy the result in the displayed string */
		mem_copy(tmp_str, str, LCD_SIZE_NB_CHAR_PER_LINE);

		break;

	case CENTER:
		/* Find the first character displayed */
		first_char_idx = 0;
		while((str[first_char_idx] == ' ') && (first_char_idx < LCD_SIZE_NB_CHAR_PER_LINE))
			first_char_idx++;

		/* No character have been found */
		if(first_char_idx >= LCD_SIZE_NB_CHAR_PER_LINE)
			return;

		/* Find the last character displayed */
		last_char_idx = LCD_SIZE_NB_CHAR_PER_LINE - 1;
		while((str[last_char_idx] == ' ') && (last_char_idx >= first_char_idx))
			last_char_idx--;

		/* If all the line is written no alignment is needed */
		if ((first_char_idx == 0) && (last_char_idx == LCD_SIZE_NB_CHAR_PER_LINE - 1))
			return;

		/* Compute the number of displayed characters */
		size = last_char_idx - first_char_idx + 1;

		/* Compute the start position of the string */
		new_str_idx = (LCD_SIZE_NB_CHAR_PER_LINE - size) / 2;

		/* Fill the start of the string with spaces */
		for(char_idx = 0; char_idx < new_str_idx; char_idx++)
			tmp_str[char_idx] = ' ';

		/* Copy the string */
		for(char_idx = new_str_idx; char_idx <= new_str_idx + size; char_idx++)
			tmp_str[char_idx] = str[first_char_idx + char_idx - new_str_idx];

		/* Fill the end of the line with spaces */
		for(char_idx = new_str_idx + size + 1; char_idx < LCD_SIZE_NB_CHAR_PER_LINE; char_idx++)
			tmp_str[char_idx] = ' ';

		/* Copy the result in the displayed string */
		mem_copy(tmp_str, str, LCD_SIZE_NB_CHAR_PER_LINE);

		break;

	case RIGHT:
		/* Find the first character displayed */
		char_idx = 0;
		while((str[char_idx] == ' ') && (char_idx < LCD_SIZE_NB_CHAR_PER_LINE))
			char_idx++;

		/* No character have been found */
		if(char_idx >= LCD_SIZE_NB_CHAR_PER_LINE)
			return;

		/* Find the first space character after the text */
		else
		{
			while((str[char_idx] != ' ') && (char_idx < LCD_SIZE_NB_CHAR_PER_LINE))
				char_idx++;

			/* The text goes until the end of the screen, no alignment is needed */
			if(char_idx >= LCD_SIZE_NB_CHAR_PER_LINE)
				return;
		}

		/* Fill the start of the line with spaces */
		for(new_str_idx = 0; new_str_idx < (LCD_SIZE_NB_CHAR_PER_LINE - char_idx); new_str_idx++)
			tmp_str[new_str_idx] = ' ';

		new_str_idx_mem = new_str_idx;

		/* Copy the text in the temporary table */
		for(new_str_idx = (LCD_SIZE_NB_CHAR_PER_LINE - char_idx); new_str_idx < LCD_SIZE_NB_CHAR_PER_LINE; new_str_idx++)
			tmp_str[new_str_idx] = str[new_str_idx - new_str_idx_mem];

		/* Copy the result in the displayed string */
		mem_copy(tmp_str, str, LCD_SIZE_NB_CHAR_PER_LINE);


		break;
	}

}


void DisplayInterface::setLineAlignmentAndRefresh(uint8_t line, T_DisplayInterface_LineAlignment alignment)
{
	/* If the line is in line shift mode, do nothing
	 * Changes are made only if the new alignment is different than the current one
	 * If the line is empty, do nothing */
	if((display_data[line].mode != LINE_SHIFT)
			&& (alignment != display_data[line].alignment)
			&& (display_data[line].isEmpty == false))
	{
		/* Update data structure */
		display_data[line].alignment = alignment;

		/* Update string and refresh display */
		setLineAlignment(line);
		refreshLine(line);
	}
}



bool DisplayInterface::pauseLineShift()
{
	if(isShiftInProgress)
	{
		if (pause())
			return true;
		else
			return false;
	}
	else
		return true;
}

void DisplayInterface::restartLineShift()
{
	if(isShiftInProgress)
		start();
}
