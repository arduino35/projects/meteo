/*!
 * @file DisplayManagement.cpp
 *
 * @brief Display management source file
 *
 * @date Fri Jul  1 18:29:51     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Jul  1 18:29:52     2022 : 76-temps-avant-changement-de-page-variable 
 * Fri Jul  1 16:58:30     2022 : 75-changement-de-page-sur-pushbutton 
 * Thu May 26 16:35:30     2022 : 71-changement-periode-de-l-autoswitch-de-l-affichage 
 * Tue May 10 10:52:54     2022 : 70-ajout-parametrage-bite-pour-affichage 
 * Fri May  6 18:02:05     2022 : 67-rendre-generique-affichage-ecran 
 * Fri Apr 29 11:34:43     2022 : 60-modification-gestion-periode-des-services 
 * Tue Apr  5 20:18:09     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Apr  5 12:06:06     2022 : 63-rendre-generique-le-formatage-des-donnees-periodiques 
 * Tue Mar  1 18:42:44     2022 : 59-passage-en-allocation-statique 
 * Fri Feb  4 08:52:24     2022 : 57-corrections-bugs-bite 
 * Fri Jan 21 21:47:08     2022 : 56-correction-affichage-capteurs-desactives 
 * Wed Jan  5 20:55:54     2022 : 51-optimisation-des-typages 
 * Wed Jan  5 17:01:25     2022 : 50-corrections-sonar 
 * Mon Dec 20 11:57:30     2021 : 45-affichage-de-la-deconnexion-a-la-place-de-l-heure 
 * 
 */

#include <stdlib.h>
#include <avr/io.h>

#include "../../../lib/common_lib/generic/common_types.h"
#include "../../../lib/string/generic/String.h"



#include "../../../os/service/generic/PeriodicService.h"
#include "../../../os/scheduler/generic/Scheduler.h"

#include "../../asw_manager/generic/asw_manager.h"
#include "../../../bsw/bsw_manager/generic/bsw_manager.h"

#include "../../time_manager/generic/TimeManager.h"
#include "DisplayManagement.h"

#include "../../sensors/sensors_mgt/generic/SensorManagement.h"



DisplayManagement::DisplayManagement() : PeriodicService(DISPLAY_MGT_PERIOD_WELCOME_MSG_REMOVAL)
{
	isDisplayStarted = false;
	curScreenDisplayed = 0;
	screenSwitchCounter = 0;
	isAutoSwitchActive = false;
	autoSwitchCycles = 1;
}

bool DisplayManagement::initService()
{
	isDisplayStarted = false;
	curScreenDisplayed = 0;
	screenSwitchCounter = 0;
	isAutoSwitchActive = mainDisplayCfgStruct.autoSwitch;
	autoSwitchCycles = (uint8_t) (mainDisplayCfgStruct.screensCfg[0].autoswitchTime / DISPLAY_MGT_PERIOD_TASK);

	bool status = initPeriodicService(DISPLAY_MGT_PERIOD_WELCOME_MSG_REMOVAL);
	status &= display_ift.initService(&LCD_init_cnf);

	if (status)
	{
		display_ift.displayFullLine((uint8_t*) welcomeMessageString,
				sizeof_welcomeMessageString / sizeof(uint8_t) - 1,
		        1, NORMAL, CENTER);

		/* Initialize services */
		status &= p_global_ASW_manager.initNewService(DATE_TIME);
		status &= p_global_ASW_manager.initNewService(SENSORS);

		/* Initialize drivers */
		status &= p_global_BSW_manager.initializeDriver(INT_MGT);

		/* Start scheduling */
		if (status)
		{
			/* Configure external interrupt for manual switching */
			((IntManager*) (p_global_BSW_manager.getDriverPointer(INT_MGT)))->createNewInt(EXTERNAL,
			        ENCODE_INTX_CNF(INT_MGT_SENSECTL_INTX_RISING_EDGE, DISPLAY_MGT_EXT_INT_PIN));

			/* Start service */
			status = start();
		}
	}

	return status;
}


void DisplayManagement::run()
{
	bool emptyScreen = true;
	String str;

	if (!isDisplayStarted)
	{
		/* Clear the screen */
		display_ift.clearFullScreen();

		/* Update task period */
		setPeriod(DISPLAY_MGT_PERIOD_TASK);

		isDisplayStarted = true;
	}

	/* Automatic screen switching */
	if (isAutoSwitchActive)
	{
		if (screenSwitchCounter >= autoSwitchCycles)
		{
			curScreenDisplayed = (curScreenDisplayed + 1) % mainDisplayCfgStruct.screensCnt;
			autoSwitchCycles = (uint8_t) (mainDisplayCfgStruct.screensCfg[curScreenDisplayed].autoswitchTime
			        / DISPLAY_MGT_PERIOD_TASK);
			display_ift.clearFullScreen();
			screenSwitchCounter = 0;
		}
		else
			screenSwitchCounter++;
	}

	/* Get services pointers */
	SensorManagement *sensor_ptr = (SensorManagement*) (p_global_ASW_manager.getServicePointer(SENSORS));
	TimeManager *time_ptr = (TimeManager*) p_global_ASW_manager.getPeriodicServicePointer(DATE_TIME);
	
	/* Display current screen */
	T_ScreenCfgStruct screen = mainDisplayCfgStruct.screensCfg[curScreenDisplayed];
	for (uint8_t lineIdx = 0; lineIdx < screen.linesCnt; lineIdx++)
	{
		T_LineCfgStruct line = screen.linesCfg[lineIdx];
		str.initString();

		/* If standard text shall be displayed */
		switch (line.type)
		{
		case DATA_TEXT:
			str.initString(line.text);
			break;
		case DATA_SENSORS:
			sensor_ptr->displayFormatting(line.sensorsCfg, &str);
			break;
		case DATA_TIME:
			time_ptr->displayFormatting(line.timeCfg, &str);
			break;
		default:
			break;
		}

		display_ift.displayFullLine(&str, line.position, line.mode, line.align);
		emptyScreen = false;
	}
	
	if (emptyScreen)
	{
		str.initString((const uint8_t*) "Desole... Rien a afficher ici...");
		display_ift.displayFullLine(&str, 0, GO_TO_NEXT_LINE, CENTER);
	}
}

bool DisplayManagement::start()
{
	if (!isServiceRunning())
	{
		/* Add periodic task in scheduler */
		p_global_scheduler.addPeriodicTask(this);

		display_ift.restartLineShift();

		setServiceRunning(true);
		return true;
	}
	else
		return false;

}

bool DisplayManagement::pause()
{
	if (isServiceRunning())
	{
		/* Remove task from scheduler */
		if ((p_global_scheduler.removePeriodicTask(this)) && display_ift.pauseLineShift())
		{
			/* Clear the screen */
			display_ift.clearFullScreen();

			setServiceRunning(false);
			return true;
		}
		else
			return false;
	}
	else
		return false;

}

bool DisplayManagement::switchPage()
{
	if (mainDisplayCfgStruct.screensCnt > 1)
	{
		curScreenDisplayed = (curScreenDisplayed + 1) % mainDisplayCfgStruct.screensCnt;
		display_ift.clearFullScreen();
		run();
		return true;
	}
	else
		return false;
}

bool DisplayManagement::setAutoSwitchPeriod(t_time_ms period)
{
	if ((period % getPeriod()) == 0)
	{
		autoSwitchCycles = (uint8_t) (period / getPeriod());
		return true;
	}
	else
		return false;
}
