/*!
 * @file DisplayManagement_types.h
 *
 * @brief 
 *
 * @date Fri Jul  1 18:29:52     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Jul  1 18:29:52     2022 : 76-temps-avant-changement-de-page-variable 
 * Fri May  6 17:16:04     2022 : 67-rendre-generique-affichage-ecran 
 * 
 */

#ifndef SRC_ASW_DISPLAY_MANAGER_GENERIC_DISPLAYMANAGEMENT_TYPES_H_
#define SRC_ASW_DISPLAY_MANAGER_GENERIC_DISPLAYMANAGEMENT_TYPES_H_

/*!
 * @brief Line configuration structure
 *
 */
typedef struct
{
	const T_DataPrimaryType type;
	const uint8_t *text;
	const T_TimeManager_config *const timeCfg;
	const T_SensorManagement_DataConfig *const sensorsCfg;
	const uint8_t position;
	const T_DisplayInterface_LineDisplayMode mode;
	const T_DisplayInterface_LineAlignment align;
} T_LineCfgStruct;

/*!
 * @brief Screen configuration structure
 *
 */
typedef struct
{
	const T_LineCfgStruct *const linesCfg;
	const uint8_t linesCnt;
	const t_time_ms autoswitchTime;
} T_ScreenCfgStruct;

/*!
 * @brief Main display configuration structure
 *
 */
typedef struct
{
	const T_ScreenCfgStruct *const screensCfg;
	const uint8_t screensCnt;
	const bool autoSwitch;
} T_MainDisplayCfgStruct;



#endif /* SRC_ASW_DISPLAY_MANAGER_GENERIC_DISPLAYMANAGEMENT_TYPES_H_ */
