/*!
 * @file DisplayManagement.h
 *
 * @brief Display management class header file
 *
 * @date Thu May 26 16:35:30     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Thu May 26 16:35:30     2022 : 71-changement-periode-de-l-autoswitch-de-l-affichage 
 * Tue May 10 10:52:54     2022 : 70-ajout-parametrage-bite-pour-affichage 
 * Fri May  6 18:02:05     2022 : 67-rendre-generique-affichage-ecran 
 * Tue Apr  5 20:18:09     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Apr  1 17:20:46     2022 : 59-passage-en-allocation-statique 
 * Fri Feb  4 08:52:25     2022 : 57-corrections-bugs-bite 
 * Wed Jan  5 20:55:54     2022 : 51-optimisation-des-typages 
 * 
 */

#ifndef WORK_ASW_DISPLAY_MGT_DISPLAYMANAGEMENT_H_
#define WORK_ASW_DISPLAY_MGT_DISPLAYMANAGEMENT_H_

#include "DisplayManagement_types.h"
#include "../config/DisplayManagement_cnf.h"



/*!
 * @brief Display management class
 * @details This class manages all displays. It is a top-level class.
 * 			It retrieves the data computed by other ASW classes and displays them.
 * 			It is interfaced with DisplayInterface class to display data on screens.
 * 			One interface class is used for each screen.
 */
class DisplayManagement: public PeriodicService
{

public:

	/*!
	 * @brief Class constructor
	 * @details This class initializes display management.
	 *
	 * @return Nothing
	 */
	DisplayManagement();

	/*!
	 * @brief Service initialization function
	 * @details This function initializes the service and the associated DisplayInterface object
	 *
	 * @return TRUE is the initialization has been correctly performed, FALSE otherwise
	 */
	virtual bool initService();

	/*!
	 * @brief 	Displaying sensor data function
	 * @details This function displays the sensors data on the screen.
	 * 			Sensor data are retrieved from sensor management service using the configured group ID.
	 * 			It is called periodically by the scheduler.
	 * 			At startup, it removes the welcome message from the screen and starts displaying data.
	 *
	 * 	@return Nothing
	 */
	void run();


	/*!
	 * @brief Service pausing function
	 * @details This function is used to pause the service.
	 *
	 * @return True is the service has been correctly paused, false otherwise.
	 */
	bool pause();

	/*!
	 * @brief Service start function
	 * @details This function is used to start the service after it has been paused or created.
	 *
	 * @return True is the service has been correctly started, false otherwise.
	 */
	bool start();

	/*!
	 * @brief Manual page switching function
	 * @details This functions switches the display to the next configured page
	 *
	 * @return Always true, except if only one page (or no page) is configured, ie no switching is done
	 */
	bool switchPage();

	inline bool isIsAutoSwitchActive() const
	{
		return isAutoSwitchActive;
	}

	/*!
	 * @brief Update of auto-switch mode
	 * @details This function updates the auto-switch status : activation or deactivation
	 *
	 * @return Nothing
	 */
	inline void setAutoSwitchStatus()
	{
		isAutoSwitchActive = !isAutoSwitchActive;
	}

	/*!
	 * @brief Auto-switch period update
	 * @details This function updates the screen switching period with the given value, if it is a multiple of the task period
	 *
	 * @param period
	 * @return True if the period has been updated, False otherwise
	 */
	bool setAutoSwitchPeriod(t_time_ms period);


private:

	DisplayInterface display_ift; /*!< Display interface object */

	bool isDisplayStarted; /*!< Flag indicating whether we are at startup or not +*/
	uint8_t curScreenDisplayed; /*!< Index of the current displayed screen */
	uint8_t screenSwitchCounter; /*!< Cycle counter for screen switching */
	bool isAutoSwitchActive; /*!< Auto-switching activation flag*/
	uint8_t autoSwitchCycles; /*!< Period of auto-switching in cycles */

};


#endif /* WORK_ASW_DISPLAY_MGT_DISPLAYMANAGEMENT_H_ */
