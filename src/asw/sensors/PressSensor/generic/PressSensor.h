/*!
 * @file PressSensor.h
 *
 * @brief Class PressSensor header file
 *
 * @date Tue Apr  5 20:18:12     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:13     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Apr  1 17:20:47     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 21:16:37     2022 : 51-optimisation-des-typages 
 * Thu Dec 23 20:44:54     2021 : 50-corrections-sonar 
 * 
 */

#ifndef WORK_ASW_SENSORS_PRESSSENSOR_PRESSSENSOR_H_
#define WORK_ASW_SENSORS_PRESSSENSOR_PRESSSENSOR_H_


/*!
 * @brief Class for pressure sensor
 * @details This class defines all functions used to read data from pressure sensor and monitor it.
 * 			It is inherited from class Sensor.
 */
class PressSensor : public Sensor
{
public:
	/*!
	 * @brief Class constructor
	 * @details This function initializes all data of the class PressSensor.
	 *
	 * @return Nothing
	 */
	PressSensor();

	/*!
	 * @brief Sensor initialization function
	 * @details This function initializes all data of the class TempSensor. It sets validity timeout and task period to the given value.
	 * 			If needed, it initializes the BMP180 sensor driver.
	 *
	 * @param [in] val_tmo Validity timeout
	 * @param [in] period Task period
	 * @param [in] multiplier Multiplier value
	 *
	 * @return True if the initialization have been performed correctly, False otherwise
	 */
	virtual bool initService(t_time_ms val_tmo, t_time_ms period, uint16_t multiplier);

	/*!
	 * @brief Pressure sensor reading function
	 * @details This function reads pressure sensor data using sensor driver.
	 * 			It is called periodically by the scheduler.
	 *
	 * @return Nothing
	 */
	virtual void run();

	/*!
	 * @brief Task period update
	 * @details This function updates the period of the pressure task.
	 *
	 * @param [in] period New period of the task
	 * @return True if the period has been updated, false otherwise
	 */
	virtual bool updateTaskPeriod(t_time_ms period);

};

#endif /* WORK_ASW_SENSORS_PRESSSENSOR_PRESSSENSOR_H_ */
