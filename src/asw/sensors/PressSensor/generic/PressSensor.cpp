/*!
 * @file PressSensor.cpp
 *
 * @brief Defines function of class PressSensor
 *
 * @date Tue Apr  5 20:18:12     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:12     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Apr  1 16:55:07     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 20:57:02     2022 : 51-optimisation-des-typages 
 * Thu Dec 23 20:44:54     2021 : 50-corrections-sonar 
 * 
 */

#include <avr/io.h>
#include <stdlib.h>


#include "../../../../lib/common_lib/generic/common_types.h"

#include "../../../../os/service/generic/PeriodicService.h"
#include "../../sensor/config/Sensor_cnf.h"
#include "../../../time_manager/generic/TimeManager.h"
#include "../../sensor/generic/Sensor.h"
#include "../../../../os/scheduler/generic/Scheduler.h"

#include "../../../../bsw/bsw_manager/generic/bsw_manager.h"
#include "../../../asw_manager/generic/asw_manager.h"

#include "../../sensors_mgt/config/sensor_configuration.h"
#include "../../sensors_mgt/generic/SensorManagement.h"

#include "PressSensor.h"

PressSensor::PressSensor() : Sensor()
{

}

bool PressSensor::initService(t_time_ms val_tmo, t_time_ms period, uint16_t multiplier)
{
	bool status = initSensor(val_tmo, period, multiplier);

	/* Initialize BMP180 driver */
	if (status)
		status = p_global_BSW_manager.initializeDriver(BMP180);

	if (status)
	{
		((Bmp180*) p_global_BSW_manager.getDriverPointer(BMP180))->activatePressureConversion(period);

		/* Add task to scheduler */
		status = start();
	}

	return status;
}

void PressSensor::run()
{
	/* Start new conversion */
	((Bmp180*) p_global_BSW_manager.getDriverPointer(BMP180))->bmp180Monitoring_Task();

	/* Update sensor value  */
	uint16_t press;
	setLastValidity(((Bmp180*) p_global_BSW_manager.getDriverPointer(BMP180))->getPressureValue(&press));
	setRawValue(press);
	updateValidData();
}


bool PressSensor::updateTaskPeriod(t_time_ms period)
{
	//task_period = period;
	//return p_global_scheduler->updateTaskPeriod((TaskPtr_t)(&PressSensor::readPressSensor_task), task_period);
	return false;
}


