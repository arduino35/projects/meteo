/*!
 * @file sensor_configuration.cpp
 *
 * @brief Sensor configuration file
 *
 * @date Tue Apr  5 20:18:14     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:14     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Apr  1 16:55:07     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 21:17:03     2022 : 51-optimisation-des-typages 
 * 
 */

#include <stdlib.h>
#include <avr/io.h>

#include "../../../../lib/string/generic/String.h"
#include "../../../../lib/common_lib/generic/common_types.h"

#include "../../../../os/service/generic/PeriodicService.h"
#include "../../../time_manager/generic/TimeManager.h"

#include "sensor_configuration.h"
#include "../generic/SensorManagement_private.h"
#include "../generic/SensorManagement.h"

/*!< Default period for sensors task */
const t_time_ms SENSOR_MGT_CNF_DEFAULT_PERIOD = 1000;

/*!< Default timeout value for sensors */
const t_time_ms SENSOR_MGT_CNF_DEFAULT_TMO = 15000;



const T_SensorManagement_SensorGroup_Config_List SensorManagement_cnf_SensorGroup_meteo_Config_list[] =
{
		{
				TEMPERATURE,
				SENSOR_MGT_METEO_GROUP_TEMP_SENSOR_ID,
				SENSOR_MGT_CNF_DEFAULT_PERIOD,
				SENSOR_MGT_CNF_DEFAULT_TMO,
				(const uint8_t*) "T",
        (const uint8_t*) "degC",
				10
		},
		{ HUMIDITY, SENSOR_MGT_METEO_GROUP_HUM_SENSOR_ID,
        SENSOR_MGT_CNF_DEFAULT_PERIOD, SENSOR_MGT_CNF_DEFAULT_TMO, (const uint8_t*) "H", (const uint8_t*) "%", 10 },
		{
        PRESSURE, SENSOR_MGT_METEO_GROUP_PRESS_SENSOR_ID, SENSOR_MGT_CNF_DEFAULT_PERIOD, SENSOR_MGT_CNF_DEFAULT_TMO,
        (const uint8_t*) "P", (const uint8_t*) "hPa", 10 }
};

const T_SensorManagement_SensorGroup_Config_List SensorManagement_cnf_SensorGroup_CpuLoad_Config_list[] =
{
//		{
//				CPU_LOAD_CUR,
//				SENSOR_MGT_SWDATA_GROUP_CURLOAD_SENSOR_ID,
//				SENSOR_MGT_CNF_DEFAULT_PERIOD,
//				SENSOR_MGT_CNF_DEFAULT_TMO,
//				(uint8_t*)"",
//				(uint8_t*)"%",
//				1
//		},
		{
				CPU_LOAD_AVG,
				SENSOR_MGT_SWDATA_GROUP_AVGLOAD_SENSOR_ID,
				SENSOR_MGT_CNF_DEFAULT_PERIOD,
				SENSOR_MGT_CNF_DEFAULT_TMO,
                (const uint8_t*) "", (const uint8_t*) "%",
				1
		},
		{
				CPU_LOAD_MAX,
				SENSOR_MGT_SWDATA_GROUP_MAXLOAD_SENSOR_ID,
				SENSOR_MGT_CNF_DEFAULT_PERIOD,
				SENSOR_MGT_CNF_DEFAULT_TMO,
                (const uint8_t*) "", (const uint8_t*) "%",
				1
		},
};


const T_SensorManagement_SensorGroup_Config SensorManagement_cnf_SensorGroup_Config_table[] = {
		{
				SENSOR_MGT_METEO_GROUP_ID, &SensorManagement_cnf_SensorGroup_meteo_Config_list[0],
                sizeof(SensorManagement_cnf_SensorGroup_meteo_Config_list)
                        / sizeof(T_SensorManagement_SensorGroup_Config_List) }, {
				SENSOR_MGT_SWDATA_GROUP_ID,
				&SensorManagement_cnf_SensorGroup_CpuLoad_Config_list[0],
				sizeof(SensorManagement_cnf_SensorGroup_CpuLoad_Config_list)/sizeof(T_SensorManagement_SensorGroup_Config_List)
		}
};

const T_SensorManagement_Main_Config_Struct SensorManagement_cnf_Main_Config_Struct =
{
		&SensorManagement_cnf_SensorGroup_Config_table[0],
		sizeof(SensorManagement_cnf_SensorGroup_Config_table)
                / sizeof(T_SensorManagement_SensorGroup_Config)
};


bool SensorManagement::initSensor(T_SensorManagement_sensor_type type, t_time_ms val_tmo, t_time_ms period,
        uint16_t multiplier)
{
	bool status = false;

	switch (type)
	{
	case CPU_LOAD_AVG:
		sensors_list.cpuloadavgInit = sensors_list.cpuLoadAvg.initService(val_tmo, period, multiplier);
		status = sensors_list.cpuloadavgInit;
		break;
	case CPU_LOAD_MAX:
		sensors_list.cpuloadmaxInit = sensors_list.cpuLoadMax.initService(val_tmo, period, multiplier);
		status = sensors_list.cpuloadmaxInit;
		break;
	case TEMPERATURE:
		sensors_list.tempInit = sensors_list.temp.initService(val_tmo, period, multiplier);
		status = sensors_list.tempInit;
		break;
	case HUMIDITY:
		sensors_list.humInit = sensors_list.humidity.initService(val_tmo, period, multiplier);
		status = sensors_list.humInit;
		break;
	case PRESSURE:
		sensors_list.pressInit = sensors_list.press.initService(val_tmo, period, multiplier);
		status = sensors_list.pressInit;
		break;
	default:
		status = false;
		break;
	}

	return status;
}

Sensor* SensorManagement::getPointer(T_SensorManagement_sensor_type type) const
{
	Sensor *sensorPtr = 0;

	switch (type)
	{
	case CPU_LOAD_AVG:
		sensorPtr = (Sensor*) (&(sensors_list.cpuLoadAvg));
		break;
	case CPU_LOAD_MAX:
		sensorPtr = (Sensor*) (&(sensors_list.cpuLoadMax));
		break;
	case TEMPERATURE:
		sensorPtr = (Sensor*) (&(sensors_list.temp));
		break;
	case HUMIDITY:
		sensorPtr = (Sensor*) (&(sensors_list.humidity));
		break;
	case PRESSURE:
		sensorPtr = (Sensor*) (&(sensors_list.press));
		break;
	default:
		sensorPtr = 0;
		break;
	}

	return sensorPtr;
}

bool SensorManagement::isSensorInitialized(T_SensorManagement_sensor_type type) const
{
	bool status = false;

	switch (type)
	{
	case CPU_LOAD_AVG:
		status = sensors_list.cpuloadavgInit;
		break;
	case CPU_LOAD_MAX:
		status = sensors_list.cpuloadmaxInit;
		break;
	case TEMPERATURE:
		status = sensors_list.tempInit;
		break;
	case HUMIDITY:
		status = sensors_list.humInit;
		break;
	case PRESSURE:
		status = sensors_list.pressInit;
		break;
	default:
		status = false;
		break;
	}

	return status;
}

void SensorManagement::resetSensorStruct()
{
	sensors_list.cpuloadavgInit = false;
	sensors_list.cpuloadmaxInit = false;
	sensors_list.tempInit = false;
	sensors_list.humInit = false;
	sensors_list.pressInit = false;
}
