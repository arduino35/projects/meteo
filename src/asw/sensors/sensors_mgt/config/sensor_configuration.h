/*!
 * @file sensor_configuration.h
 *
 * @brief Sensors configuration header file
 *
 * @date Tue Apr  5 20:18:14     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:14     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Apr  1 16:55:08     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 21:17:03     2022 : 51-optimisation-des-typages 
 * 
 */

#ifndef WORK_ASW_SENSORS_MGT_SENSOR_CONFIGURATION_H_
#define WORK_ASW_SENSORS_MGT_SENSOR_CONFIGURATION_H_

#include "../../../../bsw/dio/generic/Dio.h"

#include "../../sensor/generic/Sensor.h"
#include "../../CpuLoadSensor/generic/CpuLoadSensorAvg.h"
#include "../../CpuLoadSensor/generic/CpuLoadSensorCurrent.h"
#include "../../CpuLoadSensor/generic/CpuLoadSensorMax.h"
#include "../../HumSensor/generic/HumSensor.h"
#include "../../PressSensor/generic/PressSensor.h"
#include "../../TempSensor/generic/TempSensor.h"

/*
 * Definition of sensors and groups IDs
 */
/*!< Unique ID of meteo sensor group */
const uint8_t SENSOR_MGT_METEO_GROUP_ID = 1;

/*!< Unique ID for temperature sensor */
const uint8_t SENSOR_MGT_METEO_GROUP_TEMP_SENSOR_ID = 1;

/*!< Unique ID for humidity sensor */
const uint8_t SENSOR_MGT_METEO_GROUP_HUM_SENSOR_ID = 2;

/*!< Unique ID for pressure sensor */
const uint8_t SENSOR_MGT_METEO_GROUP_PRESS_SENSOR_ID = 3;

/*!< Unique ID of SW data sensor group */
const uint8_t SENSOR_MGT_SWDATA_GROUP_ID = 2;

/*!< Unique ID for current CPU load sensor */
const uint8_t SENSOR_MGT_SWDATA_GROUP_CURLOAD_SENSOR_ID = 1;

/*!< Unique ID for average CPU load sensor */
const uint8_t SENSOR_MGT_SWDATA_GROUP_AVGLOAD_SENSOR_ID = 2;

/*!< Unique ID for maximum CPU load sensor */
const uint8_t SENSOR_MGT_SWDATA_GROUP_MAXLOAD_SENSOR_ID = 3;

/*!< Unique ID for RAM usage sensor */
const uint8_t SENSOR_MGT_SWDATA_GROUP_RAM_SENSOR_ID = 4;


/*!
 * @details Enumeration defining all existing sensor types
 */
typedef enum
{
	UNDEFINED, /*!< Undefined type */
	TEMPERATURE,	/*!< Temperature sensor */
	PRESSURE,		/*!< Pressure sensor */
	HUMIDITY,		/*!< Humidity sensor */
	CPU_LOAD_CUR,	/*!< CPU load current value sensor */
	CPU_LOAD_AVG,	/*!< CPU load average value sensor */
	CPU_LOAD_MAX,	/*!< CPU load maximum value sensor */
}
T_SensorManagement_sensor_type;

/*!
 * @brief Structure containing all sensors objects
 */
typedef struct
{
	CpuLoadSensorAvg cpuLoadAvg;
	bool cpuloadavgInit;
	CpuLoadSensorMax cpuLoadMax;
	bool cpuloadmaxInit;
	TempSensor temp;
	bool tempInit;
	HumSensor humidity;
	bool humInit;
	PressSensor press;
	bool pressInit;

} T_SensorManagement_Sensors_struct;



#endif /* WORK_ASW_SENSORS_MGT_SENSOR_CONFIGURATION_H_ */
