/*!
 * @file SensorManagement_private.h
 *
 * @brief SensorManagement module private declarations file
 *
 * @date Fri Jul  1 18:07:16     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Jul  1 18:07:16     2022 : 74-affichage-valeurs-mix-max-capteurs 
 * Fri May  6 17:16:05     2022 : 67-rendre-generique-affichage-ecran 
 * Tue Apr  5 20:18:15     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Feb 22 14:49:25     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 21:17:03     2022 : 51-optimisation-des-typages 
 * Wed Dec 22 21:22:22     2021 : 50-corrections-sonar 
 * 
 */

#ifndef WORK_ASW_SENSORS_MGT_GENERIC_SENSORMANAGEMENT_PRIVATE_H_
#define WORK_ASW_SENSORS_MGT_GENERIC_SENSORMANAGEMENT_PRIVATE_H_

/*!
 * @brief Sensors data configuration structure
 *
 */
typedef struct
{
	uint8_t group; /*!< Sensor group */
	uint8_t id; /*!< Sensor id */
	T_Service_value_type type; /*!< Sensor value type */
} T_SensorManagement_DataConfig;

/*!
 * @details This structure contains all configuration informations needed for each used sensor in a group.
 */
typedef struct
{
	T_SensorManagement_sensor_type type; /*!< Sensor type */
	uint8_t sensor_id; /*!< Unique sensor ID within the group */
	t_time_ms period; /*!< Period of the sensor periodic task */
	t_time_ms validity_tmo; /*!< Validity timeout */
	const uint8_t *data_name_str; /*!< Pointer to the string containing the data name */
	const uint8_t *unit_str; /*!< Pointer to the string containing the unit of the sensor data */
	uint16_t multiplier; /*!< Multiplier value */
}
T_SensorManagement_SensorGroup_Config_List;


/*!
 * @details Structure defining all available sensor groups
 */
typedef struct
{
	uint8_t group_id;											/*!< Unique ID of the group */
	const T_SensorManagement_SensorGroup_Config_List *sensor_list; /*!< Pointer to the sensors table */
	uint8_t nb_sensors;											/*!< Number of sensors in the group */
}
T_SensorManagement_SensorGroup_Config;

/*!
 * @details Main configuration structure of module SensorManagement
 */
typedef struct
{
	const T_SensorManagement_SensorGroup_Config *sensorsGroup_cnf; /*!< Pointer to the sensors groups configuration */
	uint8_t nb_groups; 											/*!< Number of groups */
}
T_SensorManagement_Main_Config_Struct;


/*
 * Main configuration structure
 */
extern const T_SensorManagement_Main_Config_Struct SensorManagement_cnf_Main_Config_Struct;

#endif /* WORK_ASW_SENSORS_MGT_GENERIC_SENSORMANAGEMENT_PRIVATE_H_ */
