/*!
 * @file SensorManagement.cpp
 *
 * @brief SensorManagement class source code file
 *
 * @date Fri Jul  1 18:07:15     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Jul  1 18:07:15     2022 : 74-affichage-valeurs-mix-max-capteurs 
 * Fri May  6 17:16:04     2022 : 67-rendre-generique-affichage-ecran 
 * Tue Apr  5 20:18:14     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Apr  5 14:23:26     2022 : 63-rendre-generique-le-formatage-des-donnees-periodiques 
 * Fri Feb 25 14:30:17     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 21:17:02     2022 : 51-optimisation-des-typages 
 * Wed Jan  5 17:01:26     2022 : 50-corrections-sonar 
 * 
 */

#include <stdlib.h>
#include <avr/io.h>

#include "../../../../lib/common_lib/generic/common_types.h"
#include "../../../../lib/common_lib/generic/mem_tools.h"
#include "../../../../lib/string/generic/String.h"

#include "../../../../os/service/generic/PeriodicService.h"
#include "../../../time_manager/generic/TimeManager.h"

#include "../../sensor/generic/Sensor.h"

#include "SensorManagement.h"


const uint8_t errorStr[] = "Erreur";
const uint8_t invalidStr[] = "Invalide";
const uint8_t invalidStrShort[] = "Inv";

SensorManagement::SensorManagement()
{

}

bool SensorManagement::initService()
{
	bool status = true;

	resetSensorStruct();
	
	/* For each group initialize the sensors */
	for(uint8_t i = 0; i < SensorManagement_cnf_Main_Config_Struct.nb_groups; i++)
		status &= initializeSensorGroup(SensorManagement_cnf_Main_Config_Struct.sensorsGroup_cnf[i].group_id);

	return status;
}

bool SensorManagement::updateTaskPeriod(t_time_ms period, uint8_t group)
{
	uint8_t grp_idx;
	bool status = true;

	if (findGroupIndex(group, &grp_idx))
	{
		/* Update each sensor in group */
		for (uint8_t i = 0; i < SensorManagement_cnf_Main_Config_Struct.sensorsGroup_cnf[grp_idx].nb_sensors; i++)
		{
			Sensor *sensorPtr = getSensorObjectPtr(group,
			        SensorManagement_cnf_Main_Config_Struct.sensorsGroup_cnf[grp_idx].sensor_list[i].sensor_id);

			if (sensorPtr != 0)
				status &= sensorPtr->updateTaskPeriod(period);
		}
		return status;
	}
	else
		return false;
}

void SensorManagement::displayFormatting(const T_SensorManagement_DataConfig *config, String *str)
{
	uint8_t grp_idx;
	uint8_t sns_idx;
		
	/* Clear the string */
	str->clear();

	/* If the sensor has been found */
	if (findSensorIndex(config->group, config->id, &grp_idx, &sns_idx))
	{
		/* Create the string */
		str->appendString(
		        SensorManagement_cnf_Main_Config_Struct.sensorsGroup_cnf[grp_idx].sensor_list[sns_idx].data_name_str);
		str->appendChar((uint8_t) ':');
		str->appendSpace();

		const Sensor *sensorPtr = getSensorObjectPtr(config->group, config->id);

		if (sensorPtr == 0)
		{
			str->appendString(errorStr);
			return;
		}

		uint16_t value;
		uint16_t multiplier = sensorPtr->getMultiplier();
		T_TimeManager_time ts;

		switch (config->type)
		{
		case CURRENT:
			if (sensorPtr->getValue(&value))
			{
				str->appendInteger(value / multiplier, 10);
				str->appendChar((uint8_t) '.');
				str->appendInteger(value % multiplier, 10);
				str->appendSpace();
				str->appendString(
				        SensorManagement_cnf_Main_Config_Struct.sensorsGroup_cnf[grp_idx].sensor_list[sns_idx].unit_str);
			}
			else
				str->appendString(invalidStr);
			break;
		case MIN:
		case MAX:
			if (sensorPtr->getMinValue(&value, &ts))
			{
				str->appendInteger(value / multiplier, 10);
				str->appendChar((uint8_t) '.');
				str->appendInteger(value % multiplier, 10);
			}
			else
				str->appendString(invalidStrShort);
			str->appendChar((uint8_t) '/');
			if (sensorPtr->getMaxValue(&value, &ts))
			{
				str->appendInteger(value / multiplier, 10);
				str->appendChar((uint8_t) '.');
				str->appendInteger(value % multiplier, 10);
			}
			else
				str->appendString(invalidStrShort);

			str->appendSpace();
			str->appendString(
			        SensorManagement_cnf_Main_Config_Struct.sensorsGroup_cnf[grp_idx].sensor_list[sns_idx].unit_str);
			break;
		}

	}
	else
		str->appendString(errorStr);
}

bool SensorManagement::initializeSensorGroup(uint8_t group)
{
	uint8_t grp_idx;
	bool status = true;

	if (findGroupIndex(group, &grp_idx))
	{
		/* Get type of each sensor in the group and initialize it */
		for (uint8_t i = 0; i < SensorManagement_cnf_Main_Config_Struct.sensorsGroup_cnf[grp_idx].nb_sensors; i++)
		{
			status &= initializeSensor(group,
			        SensorManagement_cnf_Main_Config_Struct.sensorsGroup_cnf[grp_idx].sensor_list[i].sensor_id);
		}
		return status;
	}
	return false;
}

bool SensorManagement::initializeSensor(uint8_t group, uint8_t sensor)
{
	bool status;

	T_SensorManagement_sensor_type type = getSensorType(group, sensor);
	t_time_ms tmo = getValTmo(group, sensor);
	t_time_ms period = getPeriod(group, sensor);
	uint16_t mul = getMultiplier(group, sensor);

	if ((type != UNDEFINED) && (tmo != 0) && (period != 0) && (mul != 0))
	{
		if (!isSensorInitialized(type))
			status = initSensor(type, tmo, period, mul);
		else
			status = true;
	}
	else
		status = false;

	return status;
}

Sensor* SensorManagement::getSensorObjectPtr(uint8_t group, uint8_t sensor)
{
	T_SensorManagement_sensor_type type = getSensorType(group, sensor);

	if ((type != UNDEFINED) && (isSensorInitialized(type)))
		return getPointer(type);
	else
		return 0;
}

uint8_t SensorManagement::getSensorCountInGroup(uint8_t group) const
{
	/* Get index the requested group of sensors */
	uint8_t idx = 0;
	if (findGroupIndex(group, &idx))
		return SensorManagement_cnf_Main_Config_Struct.sensorsGroup_cnf[idx].nb_sensors;
	else
		return 0;
}

T_SensorManagement_sensor_type SensorManagement::getSensorType(uint8_t group, uint8_t sensor) const
{
	uint8_t grp_idx;
	uint8_t sns_idx;

	if (findSensorIndex(group, sensor, &grp_idx, &sns_idx))
		return SensorManagement_cnf_Main_Config_Struct.sensorsGroup_cnf[grp_idx].sensor_list[sns_idx].type;
	else
		return UNDEFINED;
}

bool SensorManagement::findGroupIndex(uint8_t group, uint8_t *grp_idx) const
{
	*grp_idx = 0;

	/* Get index of the requested group of sensors */
	while ((*grp_idx < SensorManagement_cnf_Main_Config_Struct.nb_groups)
	        && (SensorManagement_cnf_Main_Config_Struct.sensorsGroup_cnf[*grp_idx].group_id != group))
		(*grp_idx)++;

	/* If the group has been found */
	if (*grp_idx < SensorManagement_cnf_Main_Config_Struct.nb_groups)
		return true;
	else
		return false;
}

bool SensorManagement::findSensorIndex(uint8_t group, uint8_t sensor, uint8_t *grp_idx, uint8_t *sns_idx) const
{
	bool status;

	status = findGroupIndex(group, grp_idx);

	if (status)
	{
		*sns_idx = 0;

		/* Find the index of the corresponding sensor */
		while ((*sns_idx < SensorManagement_cnf_Main_Config_Struct.sensorsGroup_cnf[*grp_idx].nb_sensors)
		        && (SensorManagement_cnf_Main_Config_Struct.sensorsGroup_cnf[*grp_idx].sensor_list[*sns_idx].sensor_id
		                != sensor))
			(*sns_idx)++;

		/* If the sensor has been found */
		if (*sns_idx < SensorManagement_cnf_Main_Config_Struct.sensorsGroup_cnf[*grp_idx].nb_sensors)
			return true;
		else
			return false;
	}
	else
		return false;
}

t_time_ms SensorManagement::getValTmo(uint8_t group, uint8_t sensor) const
{
	uint8_t grp_idx;
	uint8_t sns_idx;

	if (findSensorIndex(group, sensor, &grp_idx, &sns_idx))
		return SensorManagement_cnf_Main_Config_Struct.sensorsGroup_cnf[grp_idx].sensor_list[sns_idx].validity_tmo;
	else
		return 0;
}

t_time_ms SensorManagement::getPeriod(uint8_t group, uint8_t sensor) const
{
	uint8_t grp_idx;
	uint8_t sns_idx;

	if (findSensorIndex(group, sensor, &grp_idx, &sns_idx))
		return SensorManagement_cnf_Main_Config_Struct.sensorsGroup_cnf[grp_idx].sensor_list[sns_idx].period;
	else
		return 0;
}

uint16_t SensorManagement::getMultiplier(uint8_t group, uint8_t sensor) const
{
	uint8_t grp_idx;
	uint8_t sns_idx;

	if (findSensorIndex(group, sensor, &grp_idx, &sns_idx))
		return SensorManagement_cnf_Main_Config_Struct.sensorsGroup_cnf[grp_idx].sensor_list[sns_idx].multiplier;
	else
		return 0;
}

bool SensorManagement::formatBiteData(uint8_t *data, uint8_t group, uint8_t id, T_Service_value_type type)
{
	uint16_t value;
	bool validity;

#ifdef SENSOR_TIME_ACTIVE
	T_TimeManager_time ts = { 0, 0, 0 };
#endif

	switch (type)
	{
	case CURRENT:
	default:
		validity = getSensorObjectPtr(group, id)->getValue(&value);
		break;
#ifdef SENSOR_TIME_ACTIVE
	case MAX:
		validity = getSensorObjectPtr(group, id)->getMaxValue(&value, &ts);
		break;
	case MIN:
		validity = getSensorObjectPtr(group, id)->getMinValue(&value, &ts);
		break;
#endif
	}

	if (validity)
	{
		mem_copy((uint8_t*) &value, data, 2);

#ifdef SENSOR_TIME_ACTIVE
		if (type != CURRENT)
		{
			data[2] = ts.hours;
			data[3] = ts.minutes;
			data[4] = ts.seconds;
		}
#endif
	}

	return validity;
}
