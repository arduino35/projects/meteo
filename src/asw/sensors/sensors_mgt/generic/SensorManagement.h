/*!
 * @file SensorManagement.h
 *
 * @brief SensorManagement class header file
 *
 * @date Fri Jul  1 18:06:58     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Jul  1 18:06:58     2022 : 74-affichage-valeurs-mix-max-capteurs 
 * Fri May  6 17:16:05     2022 : 67-rendre-generique-affichage-ecran 
 * Tue Apr  5 21:17:21     2022 : 63-rendre-generique-le-formatage-des-donnees-periodiques 
 * Tue Apr  5 20:18:15     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Feb 25 14:30:18     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 21:17:02     2022 : 51-optimisation-des-typages 
 * Thu Dec 23 20:44:56     2021 : 50-corrections-sonar 
 * 
 */

#ifndef WORK_ASW_SENSORS_MGT_SENSORMANAGEMENT_H_
#define WORK_ASW_SENSORS_MGT_SENSORMANAGEMENT_H_

#include "../config/sensor_configuration.h"
#include "SensorManagement_private.h"


/*!
 * @brief Sensor management class
 * @details This class manages all sensors present in the SW. It manages sensor activation and deactivation,
 * 			has a periodic task to retrieve sensor data. It also creates the string with sensors values used by display services.
 */
class SensorManagement : public Service {

public:
	/*!
	 * @brief Class constructor
	 * @details This function initializes the class. It calls the sensor initialization function present in the configuration.
	 *
	 * @return Nothing
	 */
	SensorManagement();

	/*!
	 * @brief Service initialization function
	 * @details This function initializes the class SensorManagement and all the configured sensors
	 *
	 * @return TRUE if all the sensors have been correctly initialized, FALSE otherwise
	 */
	virtual bool initService();

	/*!
	 * @brief Sensor count function
	 * @details This function returns the number of sensors present in the selected group.
	 *
	 * @param [in] group Group ID
	 *
	 * @return Number of sensors in the group.
	 */
	uint8_t getSensorCountInGroup(uint8_t group) const;

	/*!
	 * @brief Group count function
	 * @details This function returns the number of sensors group.
	 *
	 * @return Number of groups.
	 */
	inline uint8_t getGroupCount() const
	{
		return SensorManagement_cnf_Main_Config_Struct.nb_groups;
	}


	/*!
	 * @brief Sensors tasks period update
	 * @details This function updates the period of of all sensors tasks inside a group. The function updateTaskPeriod is called for each sensor object.
	 *
	 * @param [in] period New period.
	 * @param [in] group Group index
	 *
	 * @return True if the period has been updated, false otherwise.
	 */
	bool updateTaskPeriod(t_time_ms period, uint8_t group);

	/*!
	 * @brief Sensor value display function.
	 * @details This function gets the value of the selected sensor in the selected group and formats it
	 *          for display into a string using the data name string defined in the configuration.
	 *
	 * @param [in] config COnfiguration structure (group & id)
	 * @param [out] str Pointer to the formatted string
	 *
	 * @return Nothing
	 */
	void displayFormatting(const T_SensorManagement_DataConfig *config, String *str);

	/*!
	 * @brief Sensor object pointer get function
	 * @details This function finds the pointer to the requested sensor object in the given group inside sensor_ptr_table and returns this pointer.
	 *
	 * @param [in] group Group ID
	 * @param [in] sensor Sensor ID
	 *
	 * @return Pointer to the sensor object
	 */
	Sensor* getSensorObjectPtr(uint8_t group, uint8_t sensor);

	/*!
	 * @brief BITE data formatting
	 * @details This function gets value and validity of the requested sensor
	 *          and formats it for BITE transmission.
	 *
	 * @param [out] data Formatted data
	 * @param [in] group Group of the requested sensor
	 * @param [in] id ID of the requested sensor
	 * @param [in] type Type of the data to format
	 * @return True if the formatted data is valid, False otherwise
	 */
	bool formatBiteData(uint8_t *data, uint8_t group, uint8_t id, T_Service_value_type type);

private:

	T_SensorManagement_Sensors_struct sensors_list; /*!< Structure containing pointers to all sensors objects */

	/*!
	 * @brief Sensor group initialization function
	 * @details This function initializes each sensor existing in the requested group.
	 *
	 * @param [in] group ID of the requested group
	 * @return True if all initializations have been performed correctly, False otherwise
	 */
	bool initializeSensorGroup(uint8_t group);

	/*!
	 * @brief Single sensor initialization function
	 * @details This function initializes the selected sensor
	 *
	 * @param [in] group Group id of the selected sensor
	 * @param [in] sensor Sensor id
	 * @return True if the sensor initialization has been performed correctly, False otherwise
	 */
	bool initializeSensor(uint8_t group, uint8_t sensor);

	/*!
	 * @brief Sensor type get function
	 * @details This function finds the type of the sensor identified by its group and id
	 *
	 * @param [in] group Group of the sensor
	 * @param [in] sensor ID of the sensor
	 * @return Sensor type
	 */
	T_SensorManagement_sensor_type getSensorType(uint8_t group, uint8_t sensor) const;

	/*!
	 * @brief Sensor group finder
	 * @details This function finds the index of the requested group in the table
	 *
	 * @param [in] group Group id to find
	 * @param [out] grp_idx index in the table
	 * @return True if the group has been found, False otherwise
	 */
	bool findGroupIndex(uint8_t group, uint8_t *grp_idx) const;

	/*!
	 * @brief Sensor finder
	 * @details This function finds the index of the requested sensor in the table
	 *
	 * @param [in] group Group of the sensor to find
	 * @param [in] sensor Id of the sensor to find
	 * @param [out] sns_idx index in the table
	 * @return True if the sensor has been found, False otherwise
	 */
	bool findSensorIndex(uint8_t group, uint8_t sensor, uint8_t *grp_idx, uint8_t *sns_idx) const;

	/*!
	 * @brief Sensor pointer get function
	 * @details This function returns the pointer to the requested sensor object
	 *
	 * @param type Type of the requested sensor
	 * @return Sensor pointer
	 */
	Sensor* getPointer(T_SensorManagement_sensor_type type) const;

	/*!
	 * @brief Sensor status function
	 * @details  This function returns the status of the requested sensor
	 *
	 * @param type Type of the requested sensor
	 * @return True if the sensor is initialized, False otherwise
	 */
	bool isSensorInitialized(T_SensorManagement_sensor_type type) const;

	/*!
	 * @brief Sensors structure reset function
	 * @details This function resets the status of all sensors by setting initialization status to False
	 *
	 * @return Nothing
	 */
	void resetSensorStruct();

	/*!
	 * @brief Sensor initialization function
	 * @details This function initializes the requested sensor and sets the status flag to True
	 *
	 * @param [in] type Type of the requested sensor
	 * @param [in] val_tmo Validity timeout
	 * @param [in] period Sensor period
	 * @param [in] multiplier Sensor value multiplier
	 *
	 * @return True if the initialization has been performed correctly, False otherwise
	 */
	bool initSensor(T_SensorManagement_sensor_type type, t_time_ms val_tmo, t_time_ms period, uint16_t multiplier);

	/*!
	 * @brief Sensor validity timeout get function
	 * @details This function retrieves the validity timeout for the selected sensor
	 *
	 * @param [in] group Group of the sensor to find
	 * @param [in] sensor Id of the sensor to find
	 * @return Validity timeout
	 */
	t_time_ms getValTmo(uint8_t group, uint8_t sensor) const;

	/*!
	 * @brief Sensor period get function
	 * @details This function retrieves the period for the selected sensor
	 *
	 * @param [in] group Group of the sensor to find
	 * @param [in] sensor Id of the sensor to find
	 * @return Sensor period
	 */
	t_time_ms getPeriod(uint8_t group, uint8_t sensor) const;

	/*!
	 * @brief Sensor multiplier value get function
	 * @details This function retrieves the multiplier value for the selected sensor
	 *
	 * @param [in] group Group of the sensor to find
	 * @param [in] sensor Id of the sensor to find
	 * @return Multiplier value
	 */
	uint16_t getMultiplier(uint8_t group, uint8_t sensor) const;

};

#endif /* WORK_ASW_SENSORS_MGT_SENSORMANAGEMENT_H_ */
