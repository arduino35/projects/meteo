/*!
 * @file TempSensor.cpp
 *
 * @brief Defines function of class TempSensor
 *
 * @date Tue Apr  5 20:18:13     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:13     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Apr  1 11:43:19     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 20:57:03     2022 : 51-optimisation-des-typages 
 * Thu Dec 23 20:44:55     2021 : 50-corrections-sonar 
 * Mon Oct 25 20:10:38     2021 : 38-temperature-trop-souvent-invalide 
 * 
 */

#include <avr/io.h>
#include <stdlib.h>

#include "../../../../os/service/generic/PeriodicService.h"
#include "../../sensor/config/Sensor_cnf.h"
#include "../../../time_manager/generic/TimeManager.h"
#include "../../sensor/generic/Sensor.h"

#include "../../../../bsw/dio/generic/Dio.h"
#include "../config/TempSensor_cnf.h"
#include "TempSensor.h"

#include "../../../../os/scheduler/generic/Scheduler.h"

#include "../../../../bsw/bsw_manager/generic/bsw_manager.h"

#include "../../../asw_manager/generic/asw_manager.h"

#include "../../sensors_mgt/config/sensor_configuration.h"
#include "../../sensors_mgt/generic/SensorManagement.h"

#ifdef TEMP_SENSOR_BMP180_SENSOR_ACTIVE
#include "../../../../bsw/bmp180/generic/Bmp180.h"
#endif
#ifdef TEMP_SENSOR_DHT22_SENSOR_ACTIVE
#include "../../../../bsw/dht22/generic/Dht22.h"
#endif


TempSensor::TempSensor() : Sensor()
{

}

bool TempSensor::initService(t_time_ms val_tmo, t_time_ms period, uint16_t multiplier)
{
	bool status = initSensor(val_tmo, period, multiplier);

#ifdef TEMP_SENSOR_DHT22_SENSOR_ACTIVE
	/* Initialize DHT22 sensor driver */
	if (status)
		status = p_global_BSW_manager.initializeDriver(DHT22);

	if (status)
		((Dht22*) p_global_BSW_manager.getDriverPointer(DHT22))->ConfigurePort(DHT22_PORT);
#endif

#ifdef TEMP_SENSOR_BMP180_SENSOR_ACTIVE
	/* Initialize BMP180 sensor driver */
	if (status)
		status = p_global_BSW_manager.initializeDriver(BMP180);

	if (status)
		((Bmp180*) p_global_BSW_manager.getDriverPointer(BMP180))->activateTemperatureConversion(period);
#endif

	/* Add task to scheduler */
	if (status)
		start();

	return status;
}




void TempSensor::run()
{
	/* Get temperature values from both sensors */
	uint16_t temperature1 = 0;
	uint16_t temperature2 = 0;
	bool val1 = false;
	bool val2 = false;

#ifdef TEMP_SENSOR_DHT22_SENSOR_ACTIVE
	val1 = ((Dht22*) p_global_BSW_manager.getDriverPointer(DHT22))->getTemperature(&temperature1);
#endif

#ifdef TEMP_SENSOR_BMP180_SENSOR_ACTIVE
	((Bmp180*) p_global_BSW_manager.getDriverPointer(BMP180))->bmp180Monitoring_Task();
	val2 = ((Bmp180*) p_global_BSW_manager.getDriverPointer(BMP180))->getTemperatureValue(&temperature2);
#endif


	/* If both sensors are valid, check if the values are similar */
	if(val1 && val2)
	{
		if (((temperature1 > temperature2) && ((temperature1 - temperature2) < TEMP_SENSOR_VALUE_DIFFERENCE_STEP_2))
		        || ((temperature1 <= temperature2)
		                && ((temperature2 - temperature1) < TEMP_SENSOR_VALUE_DIFFERENCE_STEP_2)))
		{
			if (((temperature1 > temperature2) && ((temperature1 - temperature2) < TEMP_SENSOR_VALUE_DIFFERENCE_STEP_1))
			        || ((temperature1 <= temperature2)
			                && ((temperature2 - temperature1) < TEMP_SENSOR_VALUE_DIFFERENCE_STEP_1)))
			{
				/* Sensors return similar values, set validity to true and update sensor value with mean of both sensors */
				setLastValidity(true);
				setRawValue((temperature1 + temperature2) / 2);
			}
			else
			{
				/* Only use main sensor */
				setLastValidity(true);
				setRawValue(temperature1);
			}
		}
		else
		{
			/* Else set validity to false */
			setLastValidity(false);
		}
	}
	/* If only one sensor is valid, use this value */
	else if(val1)
	{
		setLastValidity(true);
		setRawValue(temperature1);
	}
	else if(val2)
	{
		setLastValidity(true);
		setRawValue(temperature2);
	}
	/* If no sensor is valid, validity is set to false */
	else
		setLastValidity(false);

	/* Update validity data */
	updateValidData();
}



bool TempSensor::updateTaskPeriod(t_time_ms period)
{
	//task_period = period;
	//return p_global_scheduler->updateTaskPeriod((TaskPtr_t)(&TempSensor::readTempSensor_task), task_period);
	return false;
}


