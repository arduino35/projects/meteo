/*!
 * @file TempSensor_cnf.h
 *
 * @brief Temperature sensor configuration file
 *
 * @date Fri Apr  1 11:43:18     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Apr  1 11:43:18     2022 : 59-passage-en-allocation-statique 
 * Wed Dec 22 21:22:14     2021 : 50-corrections-sonar 
 * Mon Oct 25 20:10:38     2021 : 38-temperature-trop-souvent-invalide 
 * 
 */


#ifndef WORK_ASW_SENSORS_CONFIG_TEMPSENSOR_CNF_H_
#define WORK_ASW_SENSORS_CONFIG_TEMPSENSOR_CNF_H_

/*!< DHT22 is connected to port PB6 */
const uint8_t DHT22_PORT = ENCODE_PORT(PORT_B, 6);

/*!< Sensors value can have a difference of 1 degrees before discarding one sensor */
const uint16_t TEMP_SENSOR_VALUE_DIFFERENCE_STEP_1 = 10;

/*!< Sensors value can have a difference of 3 degrees before becoming invalid */
const uint16_t TEMP_SENSOR_VALUE_DIFFERENCE_STEP_2 = 30;

/*!< Activation of DHT22 temperature sensor */
#define TEMP_SENSOR_DHT22_SENSOR_ACTIVE
const bool TEMP_SENSOR_USE_DHT22_SENSOR = true;

/*!< Activation of BMP180 temperature sensor */
#define TEMP_SENSOR_BMP180_SENSOR_ACTIVE
const bool TEMP_SENSOR_USE_BMP180_SENSOR = true;

#endif /* WORK_ASW_SENSORS_CONFIG_TEMPSENSOR_CNF_H_ */
