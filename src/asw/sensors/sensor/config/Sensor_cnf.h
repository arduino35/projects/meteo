/*!
 * @file Sensor_cnf.h
 *
 * @brief Generic sensor configuration file
 *
 * @date Tue Feb 22 14:49:43     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Feb 22 14:49:43     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 20:56:38     2022 : 51-optimisation-des-typages 
 * 
 */

#ifndef WORK_ASW_SENSORS_CONFIG_SENSOR_CNF_H_
#define WORK_ASW_SENSORS_CONFIG_SENSOR_CNF_H_

/*!< Default sensor task period : 1s */
const t_time_ms TASK_PERIOD_DEFAULT = 1000;

/*!< Sensor data are declared invalid after 30s */
const t_time_ms VALIDITY_TIMEOUT_MS_DEFAULT = 30000;

/*!< Activation flag for time data*/
#define SENSOR_TIME_ACTIVE


#endif /* WORK_ASW_SENSORS_CONFIG_SENSOR_CNF_H_ */
