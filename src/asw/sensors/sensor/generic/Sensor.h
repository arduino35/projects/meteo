/*!
 * @file Sensor.h
 *
 * @brief Sensor class header file
 *
 * @date Fri Jul  1 18:05:57     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Jul  1 18:05:57     2022 : 74-affichage-valeurs-mix-max-capteurs 
 * Tue Apr  5 20:18:14     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Feb 25 14:30:16     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 20:56:37     2022 : 51-optimisation-des-typages 
 * Thu Dec 23 20:44:56     2021 : 50-corrections-sonar 
 * 
 */


#ifndef WORK_ASW_SENSORS_SENSOR_H_
#define WORK_ASW_SENSORS_SENSOR_H_


#define ENCODE_SENSOR_ID(group, sensor) (uint16_t)((group << 8) + sensor) 	/*!< Macro encoding group and sensor ID into one 16-bits word */
#define DECODE_SENSOR_ID(code) 			(uint8_t)(code & 0xff)				/*!< Macro decoding sensor ID from a 16-bits word */
#define DECODE_GROUP_ID(code) 			(uint8_t)((code >> 8) & 0xff) 		/*!< Macro decoding sensor ID from a 16-bits word */

#include "../config/Sensor_cnf.h"

/*!
 * @brief Generic class for sensor device
 * @details This class defines a generic sensor, as handled by class SensorManagement. It should not be instantiated.
 * 			Only inherited classes shall be instantiated.
 * 			It inherits from class PeriodicService
 */
class Sensor : public PeriodicService
{
public:

	/*!
	 * @brief Sensor class constructor
	 * @details This function initializes the class.
	 *
	 * @return Nothing
	 */
	Sensor();

	/*!
	 * @brief Sensor initialization function
	 * @details This function initializes the sensor with its period, timeout and multiplier values. It must be called in initService function of each sensor.
	 *
	 * @param [in] val_tmo Validity timeout
	 * @param [in] period Task period
	 * @param [in] mult Multiplier value
	 * @return True if the initialization have been performed correctly, False otherwise
	 */
	bool initSensor(t_time_ms val_tmo, t_time_ms period, uint16_t mult);


	/*!
	 * @brief Class destructor
	 * @details Default destructor for the class Sensor. It does nothing.
	 *
	 * @return Nothing.
	 */
	virtual ~Sensor(){}



	/*!
	 * @brief Get sensor value function
	 * @details This function returns the value of sensor data. If the official value is not valid, the function return false.
	 * @param [out] value Sensor value
	 * @return Validity
	 */
	inline bool getValue(uint16_t *value) const
	 {
		 *value = valid_value;
		 return validity;
	 }

#ifdef SENSOR_TIME_ACTIVE
	/*!
	 * @brief Get sensor max value function
	 * @details This function returns the maximum value of sensor data. If the value is not valid, the function return false.
	 * @param [out] value Maximum sensor value
	 * @param [out] ts Timestamp for max value
	 * @return Validity
	 */
	inline bool getMaxValue(uint16_t *const value, T_TimeManager_time *const ts) const
	{
		*value = max_value;
		*ts = max_ts;
		return validity_max;
	}

	/*!
	 * @brief Get sensor min value function
	 * @details This function returns the minimum value of sensor data. If the value is not valid, the function return false.
	 * @param [out] value Minimum sensor value
	 * @param [out] ts Timestamp for min value
	 * @return Validity
	 */
	inline bool getMinValue(uint16_t *const value, T_TimeManager_time *const ts) const
	{
		*value = min_value;
		*ts = min_ts;
		return validity_min;
	}
#endif



	/*!
	 * @brief Data validity get function
	 * @details This function returns the validity of the sensor data
	 *
	 * @returns True if the sensor values are valid, false otherwise
	 */
	inline bool getValidity() const
	{
		return validity;
	}

	/*!
	 * @brief Task period update
	 * @details This function updates the period of the sensor task. It shall be re-written in each inherited class.
	 *
	 * @param [in] period New period of the task
	 * @return True if the period has been updated, false otherwise
	 */
	virtual bool updateTaskPeriod(t_time_ms period)
	{
		return false;
	}

	/*!
	 * @brief Task period get function
	 * @details This function returns the period of the sensor task
	 *
	 * @return Period of the task (ms)
	 */
	inline t_time_ms getTaskPeriod() const
	{
		return getPeriod();
	}

	/*!
	 * @brief Validity timeout setting function
	 * @details This function sets the validity timeout.
	 *
	 * @param [in] timeout New value of timeout.
	 * @return Nothing
	 */
	inline void setValidityTMO(t_time_ms timeout)
	{
		validity_tmo = timeout;
	}

	inline uint16_t getMultiplier() const
	{
		return multiplier;
	}

protected:

	/*!
	 * @brief Raw sensor value get function
	 *
	 * @param Raw value
	 */
	inline uint16_t getRawValue() const
	{
		return raw_data;
	}

	/*!
	 * @brief Raw sensor value set function
	 *
	 * @param raw_value [in] Raw sensor value
	 * @return Nothing
	 */
	inline void setRawValue(uint16_t raw_value)
	{
		raw_data = raw_value;
	}

	/*!
	 * @brief Validity setting function
	 * @details This function sets the class member validity_last_read
	 * @param [in] validity Value of validity
	 * @return Nothing
	 */
	inline void setLastValidity(bool l_validity)
	{
		validity_last_read = l_validity;
	}

	/*!
	 * @brief Updates last valid values of sensor data
	 * @details This function updates official values of sensor data to the last read values if they are valid.
	 * 			If the sensor value is valid, min and max values are also updated.
	 *          If the read values are not valid for more than 1 minute, official values are set invalid.
	 * @return Nothing
	 */
	void updateValidData();

private:

	uint16_t raw_data; /*!< Raw value of sensor data (directly coming from driver */

	bool validity; /*!< Validity of sensor data */
	bool validity_last_read; /*!< Validity of last read sensor data */

	uint32_t valid_pit; /*!< pit number of the last time when data were valid */
	t_time_ms validity_tmo; /*!< Number of PITs after which the sensor value is declared invalid */

	uint16_t valid_value; /*!< Valid value of sensor data */

	uint16_t multiplier;  /*!< Multiplier value used to get actual value during display process */

#ifdef SENSOR_TIME_ACTIVE
	uint16_t max_value; /*!< Maximum recorded value */
	bool validity_max; /*!< Validity flag for max value */
	T_TimeManager_time max_ts; /*!< Timestamp for max value */
	uint16_t min_value; /*!< Minimum recorded value */
	bool validity_min; /*!< Validity flag for min value */
	T_TimeManager_time min_ts; /*!< Timestamp for min value */
#endif

};

#endif /* WORK_ASW_SENSORS_SENSOR_H_ */
