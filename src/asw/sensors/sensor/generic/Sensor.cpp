/*!
 * @file Sensor.cpp
 *
 * @brief Sensor class source code file
 *
 * @date Tue Apr  5 20:18:13     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:13     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Apr  5 12:06:06     2022 : 63-rendre-generique-le-formatage-des-donnees-periodiques 
 * Fri Feb 25 14:30:16     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 20:56:37     2022 : 51-optimisation-des-typages 
 * Thu Dec 23 20:44:55     2021 : 50-corrections-sonar 
 * 
 */

#include <stdlib.h>
#include <avr/io.h>

#include "../../../../os/service/generic/PeriodicService.h"
#include "../../../../os/scheduler/generic/Scheduler.h"

#include "../config/Sensor_cnf.h"

#ifdef SENSOR_TIME_ACTIVE
#include "../../../time_manager/generic/TimeManager.h"
#endif

#include "../../sensor/generic/Sensor.h"
#include "../../../asw_manager/generic/asw_manager.h"

#include "../../sensors_mgt/generic/SensorManagement.h"





Sensor::Sensor() : PeriodicService(TASK_PERIOD_DEFAULT)
{

	/* Initialize class members */
	raw_data = 0;
	valid_value = 0;

	max_value = 0;
	min_value = 0xFFFF;

	validity_max = false;
	validity_min = false;

	max_ts.hours = 0;
	max_ts.minutes = 0;
	max_ts.seconds = 0;
	min_ts.hours = 0;
	min_ts.minutes = 0;
	min_ts.seconds = 0;

	validity = false;
	validity_last_read = false;

	valid_pit = 0;
	validity_tmo = VALIDITY_TIMEOUT_MS_DEFAULT/SW_PERIOD_MS;

	multiplier = 1;


}

bool Sensor::initSensor(t_time_ms val_tmo, t_time_ms period, uint16_t mult)
{
	/* Initialize class members */
	raw_data = 0;
	valid_value = 0;

	max_value = 0;
	min_value = 0xFFFF;

	validity_max = false;
	validity_min = false;

	max_ts.hours = 0;
	max_ts.minutes = 0;
	max_ts.seconds = 0;
	min_ts.hours = 0;
	min_ts.minutes = 0;
	min_ts.seconds = 0;

	validity = false;
	validity_last_read = false;

	valid_pit = 0;
	validity_tmo = val_tmo/SW_PERIOD_MS;

	multiplier = mult;

	return initPeriodicService(period);
}

void Sensor::updateValidData()
{
	if (validity_last_read == true)
	{
		valid_value = raw_data;
		validity = true;
		valid_pit = p_global_scheduler.getPitNumber();

#ifdef SENSOR_TIME_ACTIVE
		TimeManager *time_ptr = (TimeManager*) p_global_ASW_manager.getPeriodicServicePointer(DATE_TIME);

		if ((time_ptr != 0) && (time_ptr->getValidity()))
		{
			/* Update min and max values */
			if (valid_value < min_value)
			{
				min_value = valid_value;
				min_ts = time_ptr->getTime();

				validity_min = true;
			}

			if (valid_value > max_value)
			{
				max_value = valid_value;
				max_ts = time_ptr->getTime();

				validity_max = true;
			}
		}
#endif
	}
	else if ((p_global_scheduler.getPitNumber() - valid_pit) > validity_tmo)
	{
		validity = false;
	}
}


