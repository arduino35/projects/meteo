/*!
 * @file CpuLoadSensorMax.cpp
 *
 * @brief CPU load maximum value source file
 *
 * @date Tue Apr  5 20:18:11     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:11     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Feb 25 14:30:16     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 20:57:01     2022 : 51-optimisation-des-typages 
 * Thu Dec 23 20:44:53     2021 : 50-corrections-sonar 
 * 
 */

#include <avr/io.h>
#include <stdlib.h>

#include "../../../../os/service/generic/PeriodicService.h"
#include "../../sensor/config/Sensor_cnf.h"
#include "../../../time_manager/generic/TimeManager.h"
#include "../../sensor/generic/Sensor.h"

#include "CpuLoadSensorMax.h"

#include "../../../../os/scheduler/generic/Scheduler.h"

#include "../../../../bsw/bsw_manager/generic/bsw_manager.h"
#include "../../../asw_manager/generic/asw_manager.h"

#include "../../sensors_mgt/config/sensor_configuration.h"
#include "../../sensors_mgt/generic/SensorManagement.h"


CpuLoadSensorMax::CpuLoadSensorMax() : Sensor()
{

}

bool CpuLoadSensorMax::initService(t_time_ms val_tmo, t_time_ms period, uint16_t multiplier)
{
	bool status = initSensor(val_tmo, period, multiplier);

	/* Initialize CPULoad driver object */
	if (status)
		status = p_global_BSW_manager.initializeDriver(CPULOAD);

	/* Add task to scheduler */
	if (status)
		start();

	return status;
}


void CpuLoadSensorMax::run()
{
	setRawValue(((CpuLoad*) p_global_BSW_manager.getDriverPointer(CPULOAD))->getMaxCPULoad());
	setLastValidity(true);
	updateValidData();
}

bool CpuLoadSensorMax::updateTaskPeriod(t_time_ms period)
{
	//task_period = period;
	//return p_global_scheduler->updateTaskPeriod((TaskPtr_t)(&TempSensor::readTempSensor_task), task_period);
	return false;
}


