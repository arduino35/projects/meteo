/*!
 * @file CpuLoadSensorMax.h
 *
 * @brief CPU load maximum value header file
 *
 * @date Tue Apr  5 20:18:11     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:11     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Apr  1 17:20:46     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 21:16:36     2022 : 51-optimisation-des-typages 
 * Thu Dec 23 20:44:53     2021 : 50-corrections-sonar 
 * 
 */

#ifndef WORK_ASW_SENSORS_DEVICES_GENERIC_CPULOADSENSOR_CPULOADSENSORMAX_H_
#define WORK_ASW_SENSORS_DEVICES_GENERIC_CPULOADSENSOR_CPULOADSENSORMAX_H_




/*!
 * @brief Class defining CPU load maximum value virtual sensor
 * @details This class defines all functions used to read the maximum CPU load value and monitor it.
 * 			It is inherited from class Sensor.
 */
class CpuLoadSensorMax  : public Sensor
{
public:

	/*!
	 * @brief Class constructor
	 * @details This function initializes all data of the class CpuLoadSensorMax.
	 *
	 * @return Nothing
	 */
	CpuLoadSensorMax();

	/*!
	 * @brief Service initialization function
	 * @details This function initializes the sensor and the driver if needed. It calls the initSensor function
	 *
	 * @param val_tmo Timeout value
	 * @param period Service period
	 * @param multiplier Sensor value multiplier
	 */
	bool initService(t_time_ms val_tmo, t_time_ms period, uint16_t multiplier);

	/*!
	 * @brief CpuLoad maximum value reading function
	 * @details This function reads current value of CPU load using driver.
	 * 			It is called periodically by the scheduler.
	 *
	 * @return Nothing
	 */
	virtual void run();

	/*!
	 * @brief Task period update
	 * @details This function updates the period of the periodic task.
	 *
	 * @param [in] period New period of the task
	 * @return True if the period has been updated, false otherwise
	 */
	virtual bool updateTaskPeriod(t_time_ms period);



};

#endif /* WORK_ASW_SENSORS_DEVICES_GENERIC_CPULOADSENSOR_CPULOADSENSORMAX_H_ */
