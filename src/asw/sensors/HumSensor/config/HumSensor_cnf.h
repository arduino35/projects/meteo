/*!
 * @file HumSensor_cnf.h
 *
 * @brief Humidity sensor configuration file
 *
 * @date Fri Mar  4 19:06:34     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Mar  4 19:06:35     2022 : 59-passage-en-allocation-statique 
 * 
 */

#ifndef WORK_ASW_SENSORS_CONFIG_HUMSENSOR_CNF_H_
#define WORK_ASW_SENSORS_CONFIG_HUMSENSOR_CNF_H_

/*!< DHT22 is connected to port PB6 */
#define DHT22_PORT ENCODE_PORT(PORT_B, 6)


#endif /* WORK_ASW_SENSORS_CONFIG_HUMSENSOR_CNF_H_ */
