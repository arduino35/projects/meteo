/*!
 * @file HumSensor.cpp
 *
 * @brief Defines function of class HumSensor
 *
 * @date Tue Apr  5 20:18:12     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:12     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Mar  4 19:06:35     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 20:57:01     2022 : 51-optimisation-des-typages 
 * Thu Dec 23 20:44:53     2021 : 50-corrections-sonar 
 * 
 */

#include <avr/io.h>
#include <stdlib.h>

#include "../../../../os/service/generic/PeriodicService.h"
#include "../../sensor/config/Sensor_cnf.h"
#include "../../../time_manager/generic/TimeManager.h"
#include "../../sensor/generic/Sensor.h"

#include "HumSensor.h"

#include "../../../../os/scheduler/generic/Scheduler.h"

#include "../../../../bsw/bsw_manager/generic/bsw_manager.h"
#include "../../../asw_manager/generic/asw_manager.h"

#include "../../sensors_mgt/config/sensor_configuration.h"
#include "../../sensors_mgt/generic/SensorManagement.h"


HumSensor::HumSensor() : Sensor()
{

}

bool HumSensor::initService(t_time_ms val_tmo, t_time_ms period, uint16_t multiplier)
{
	bool status = initSensor(val_tmo, period, multiplier);

	if (status)
		status = p_global_BSW_manager.initializeDriver(DHT22);

	if (status)
	{
		((Dht22*) (p_global_BSW_manager.getDriverPointer(DHT22)))->ConfigurePort(DHT22_PORT);
		status = start();
	}

	return status;
}


void HumSensor::run()
{
	uint16_t humidity;
	setLastValidity(((Dht22*) (p_global_BSW_manager.getDriverPointer(DHT22)))->getHumidity(&humidity));
	setRawValue(humidity);
	updateValidData();
}


bool HumSensor::updateTaskPeriod(t_time_ms period)
{
	//task_period = period;
	//return p_global_scheduler->updateTaskPeriod((TaskPtr_t)(&HumSensor::readHumSensor_task), task_period);
	return false;
}

