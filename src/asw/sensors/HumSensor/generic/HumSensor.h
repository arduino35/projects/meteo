/*!
 * @file HumSensor.h
 *
 * @brief Class HumSensor header file
 *
 * @date Tue Apr  5 20:18:12     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:12     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Mar  4 19:06:35     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 21:16:36     2022 : 51-optimisation-des-typages 
 * Thu Dec 23 20:44:54     2021 : 50-corrections-sonar 
 * 
 */

#ifndef WORK_ASW_SENSORS_HUMSENSOR_HUMSENSOR_H_
#define WORK_ASW_SENSORS_HUMSENSOR_HUMSENSOR_H_

#include "../config/HumSensor_cnf.h"

/*!
 * @brief Class for humidity sensor
 * @details This class defines all functions used to read data from humidity sensor and monitor it.
 * 			It is inherited from class Sensor.
 */
class HumSensor : public Sensor
{
public:

	/*!
	 * @brief Class constructor
	 * @details This function initializes all data of the class HumSensor.
	 *
	 * @return Nothing
	 */
	HumSensor();

	/*!
	 * @brief Sensor initialization function
	 * @details This function initializes all data of the class HumSensor. It sets validity timeout and task period to the given value.
	 * 			It initializes the DHT22 sensor object.
	 *
	 * @param [in] val_tmo Validity timeout
	 * @param [in] period Task period
	 * @param [in] multiplier Multiplier value
	 * @return Nothing
	 */
	bool initService(t_time_ms val_tmo, t_time_ms period, uint16_t multiplier);


	/*!
	 * @brief Humidity sensor reading function
	 * @details This function reads humidity sensor data using sensor driver.
	 * 			It is called periodically by the scheduler.
	 *
	 * @return Nothing
	 */
	virtual void run();

	/*!
	 * @brief Task period update
	 * @details This function updates the period of the temperature task.
	 *
	 * @param [in] period New period of the task
	 * @return True if the period has been updated, false otherwise
	 */
	virtual bool updateTaskPeriod(t_time_ms period);


};

#endif /* WORK_ASW_SENSORS_HUMSENSOR_HUMSENSOR_H_ */
