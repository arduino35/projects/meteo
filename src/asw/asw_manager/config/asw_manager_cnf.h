/*!
 * @file asw_manager_cnf.h
 *
 * @brief ASW manager configuration header file
 *
 * @date Tue Apr  5 20:18:04     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:04     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Apr  5 12:06:05     2022 : 63-rendre-generique-le-formatage-des-donnees-periodiques 
 * Tue Feb 22 14:49:24     2022 : 59-passage-en-allocation-statique 
 * 
 */

#ifndef WORK_ASW_ASW_MANAGER_CONFIG_ASW_MANAGER_CNF_H_
#define WORK_ASW_ASW_MANAGER_CONFIG_ASW_MANAGER_CNF_H_

/*!
 * @brief Enumeration defining all available ASW services.
 */
typedef enum
{
	BITE_PERIODIC, /*!< Service for BITE periodic data transmission */
	BITE_INTERACTIVE, /*!< Service for interactive BITE management */
	BITE, /*!< Service for BITE management */
	LCD_DISPLAY, /*!< Service for LCD display */
	LED, /*!< Service for LED blinking */
	SENSORS, /*!< Service for sensors management */
	DATE_TIME, /*!< Service for time and date management */
} T_ASW_manager_service_list;


#include "../../../lib/string/generic/String.h"

#include "../../time_manager/generic/TimeManager.h"

#include "../../../bsw/dio/generic/Dio.h"
#include "../../sensors/sensors_mgt/generic/SensorManagement.h"

#include "../../../lib/fifo/generic/Fifo.h"
#include "../../../bsw/usart/generic/Usart.h"
#include "../../bite/generic/BiteFrame.h"
#include "../../bite/generic/BiteManager.h"
#include "../../bite/generic/BitePeriodicData.h"
#include "../../bite/generic/BiteInteractive.h"

#include "../../../bsw/i2c/generic/I2C.h"
#include "../../../bsw/lcd/generic/LCD.h"
#include "../../display_manager/generic/DisplayInterface.h"
#include "../../display_manager/generic/DisplayManagement.h"

#include "../../led/generic/Led.h"



/*!
 * @brief Structure containing all services objects
 */
typedef struct
{
	Led led;
	bool ledInit;
	DisplayManagement display;
	bool displayInit;
	BiteManager bite;
	bool biteInit;
	BiteInteractive biteInter;
	bool biteInterInit;
	BitePeriodicData bitePeriod;
	bool bitePeriodInit;
	TimeManager time;
	bool timeInit;
	SensorManagement sensors;
	bool sensorsInit;
} T_ASW_Manager_service_struct;


#include "../generic/asw_manager.h"


#endif /* WORK_ASW_ASW_MANAGER_CONFIG_ASW_MANAGER_CNF_H_ */
