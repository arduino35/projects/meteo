/*!
 * @file asw_manager_cnf.cpp
 *
 * @brief ASW manager configuration file
 *
 * @date Tue Apr  5 20:18:04     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:04     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Apr  5 12:06:05     2022 : 63-rendre-generique-le-formatage-des-donnees-periodiques 
 * Tue Feb 22 14:49:23     2022 : 59-passage-en-allocation-statique 
 * Thu Dec 23 18:17:10     2021 : 50-corrections-sonar 
 * 
 */

#include <avr/io.h>

#include "../../../lib/common_lib/generic/common_types.h"

#include "../../../os/service/generic/PeriodicService.h"
#include "asw_manager_cnf.h"

#include "asw_manager_cnf.h"

//@formatter:off
const T_ASW_manager_service_config_struct ASW_manager_service_config_struct[] = {
{ BITE_PERIODIC, PERIODIC, (const uint8_t*) "BITE Periodic" },
{ BITE_INTERACTIVE, STATIC, (const uint8_t*) "BITE Interactif" },
{ BITE, PERIODIC, (const uint8_t*) "BITE" },
{ LCD_DISPLAY, PERIODIC, (const uint8_t*) "LCD" },
{ LED, PERIODIC, (const uint8_t*) "LED" },
{ SENSORS, STATIC, (const uint8_t*) "Capteurs" },
{ DATE_TIME, PERIODIC, (const uint8_t*) "Date/Heure" },
};
//@formatter:on


uint8_t ASW_manager::getAvailableServicesCount() const
{
	return sizeof(ASW_manager_service_config_struct) / sizeof(T_ASW_manager_service_config_struct);
}

Service* ASW_manager::getPointer(T_ASW_manager_service_list driver_type)
{
	Service *srv_ptr = 0;

	switch (driver_type)
	{
	case LED:
		srv_ptr = (Service*) &(servicesList.led);
		break;
	case LCD_DISPLAY:
		srv_ptr = (Service*) &(servicesList.display);
		break;
	case BITE:
		srv_ptr = (Service*) &(servicesList.bite);
		break;
	case BITE_PERIODIC:
		srv_ptr = (Service*) &(servicesList.bitePeriod);
		break;
	case BITE_INTERACTIVE:
		srv_ptr = (Service*) &(servicesList.biteInter);
		break;
	case DATE_TIME:
		srv_ptr = (Service*) &(servicesList.time);
		break;
	case SENSORS:
		srv_ptr = (Service*) &(servicesList.sensors);
		break;
	default:
		srv_ptr = 0;
		break;
	}

	return srv_ptr;
}

bool ASW_manager::initializeService(T_ASW_manager_service_list service_type)
{
	bool retval;

	switch (service_type)
	{
	case BITE_PERIODIC:
		retval = servicesList.bitePeriod.initService();
		servicesList.bitePeriodInit = true;
		break;
	case BITE_INTERACTIVE:
		retval = servicesList.biteInter.initService();
		servicesList.biteInterInit = true;
		break;
	case BITE:
		retval = servicesList.bite.initService();
		servicesList.biteInit = true;
		break;
	case LCD_DISPLAY:
		retval = servicesList.display.initService();
		servicesList.displayInit = true;
		break;
	case LED:
		retval = servicesList.led.initService();
		servicesList.ledInit = true;
		break;
	case SENSORS:
		retval = servicesList.sensors.initService();
		servicesList.sensorsInit = true;
		break;
	case DATE_TIME:
		retval = servicesList.time.initService();
		servicesList.timeInit = true;
		break;
	default:
		retval = false;
		break;
	}

	return retval;
}

bool ASW_manager::isServiceInitialized(T_ASW_manager_service_list srv_type) const
{
	bool retval;

	switch (srv_type)
	{
	case LED:
		retval = servicesList.ledInit;
		break;
	case LCD_DISPLAY:
		retval = servicesList.displayInit;
		break;
	case BITE_PERIODIC:
		retval = servicesList.bitePeriodInit;
		break;
	case BITE_INTERACTIVE:
		retval = servicesList.biteInterInit;
		break;
	case BITE:
		retval = servicesList.biteInit;
		break;
	case DATE_TIME:
		retval = servicesList.timeInit;
		break;
	case SENSORS:
		retval = servicesList.sensorsInit;
		break;
	default:
		retval = false;
		break;
	}

	return retval;
}

void ASW_manager::resetServiceStruct()
{
	servicesList.ledInit = false;
	servicesList.displayInit = false;
	servicesList.biteInit = false;
	servicesList.biteInterInit = false;
	servicesList.bitePeriodInit = false;
	servicesList.timeInit = false;
	servicesList.sensorsInit = false;
}
