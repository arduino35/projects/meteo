/*!
 * @file asw_manager.cpp
 * @brief ASW manager source file
 *
 *  @date Tue Apr  5 20:18:04     2022
 *  @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:05     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Feb 22 14:50:41     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 17:01:24     2022 : 50-corrections-sonar 
 * 
 */



#include <avr/io.h>
#include <stdlib.h>

#include "../../../os/service/generic/PeriodicService.h"

#include "asw_manager.h"


ASW_manager p_global_ASW_manager;

ASW_manager::ASW_manager()
{
	initASW();
}

bool ASW_manager::initASW()
{
	resetServiceStruct();
	return true;
}



bool ASW_manager::initNewServicesList(const T_ASW_manager_service_list *srv_list, uint8_t srv_count)
{
	bool retval = true;

	for (uint8_t i=0; i<srv_count; i++)
	{
		if (!initNewService(srv_list[i]))
			retval = false;
	}

	return retval;
}

bool ASW_manager::initNewService(T_ASW_manager_service_list srv_type)
{
	/* Only if the service is not yet initialized */
	if (!isServiceInitialized(srv_type))
		return initializeService(srv_type);
	else
		return true;
}



Service* ASW_manager::getServicePointer(T_ASW_manager_service_list srv_type)
{
	/* Check if the service is initialized, if yes, get the pointer */
	if (isServiceInitialized(srv_type))
		return getPointer(srv_type);
	else
		return 0;
}

PeriodicService* ASW_manager::getPeriodicServicePointer(
		T_ASW_manager_service_list srv_type)
{
	/* Check if the requested service is periodic, if yes get the pointer */
	if (getServiceNature(srv_type) == PERIODIC)
		return (PeriodicService*)getServicePointer(srv_type);
	else
		return 0;
}




T_ASW_manager_service_nature ASW_manager::getServiceNature(
		T_ASW_manager_service_list type) const
{
	uint8_t i = 0;
	T_ASW_manager_service_nature retval = STATIC;

	while (i < getAvailableServicesCount()
			&& ASW_manager_service_config_struct[i].serviceID != type)
		i++;


	if (i < getAvailableServicesCount())
		retval = ASW_manager_service_config_struct[i].srv_nature;

	return retval;
}


