/*!
 * @file asw_manager.h
 * @brief ASW manager class header file
 *
 * @date Tue Apr  5 20:18:05     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:05     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Feb 22 14:50:41     2022 : 59-passage-en-allocation-statique 
 * Thu Dec 23 20:44:51     2021 : 50-corrections-sonar 
 * 
 */

#ifndef WORK_ASW_ASW_H_
#define WORK_ASW_ASW_H_

#include "../config/asw_manager_cnf.h"

/*!
 * @brief Enumeration defining service status
 */
typedef enum
{
	RUNNING,				/*!< Service is running */
	PAUSED,					/*!< Service is paused */
	ERR_NOT_CREATED,		/*!< Error : Service is not created */
	ERR_ALREADY_RUNNING,	/*!< Error : Service already running */
	ERR_NOT_RUNNING,		/*!< Error : Service not running */
	ERR_ALREADY_CREATED, /*!< Error : Service already created */
	ERR_NOT_PERIODIC /*!< Error : Service not periodic */
}
T_ASW_Manager_Service_Status;

/*!
 * @brief Enumeration defining the 2 main natures of service
 */
typedef enum
{
	STATIC, /*!< Static service */
	PERIODIC /*!< Periodic service */
} T_ASW_manager_service_nature;

/*!
 * @brief Structure defining the name of each service
 * @details This structure links the service ID (defined as an enumeration T_ASW_manager_service_list) to a string
 */
typedef struct
{
	const T_ASW_manager_service_list serviceID; /*!< ID of the service */
	const T_ASW_manager_service_nature srv_nature; /*!< Service nature */
	const uint8_t *serviceName; /*!< Name of the service */
} T_ASW_manager_service_config_struct;

/*!
 *  @details Table containing the correspondence between a service ID and its name.
 *           The number of elements must correspond to the number of services defined in T_ASW_manager_service_list.
 */
extern const T_ASW_manager_service_config_struct ASW_manager_service_config_struct[];


/*!
 * @brief ASW manager class
 * @details This class manages ASW services
 */
class ASW_manager
{
public:
	/*!
	 * @brief Class constructor
	 * @details This function initializes the class ASW_manager.\n
	 *
	 * @return Nothing
	 */
	ASW_manager();

	/*!
	 * @brief Class initialization function
	 * @details This function initializes ASW manager
	 *
	 * @return Always TRUE
	 */
	bool initASW();


	/*!
	 * @brief Services initialization function
	 * @details This function is used to initialize all the services given in the list in parameter.
	 * 			It calls the function initNewService to initialize the services individually.
	 *
	 * @param [in] srv_list List of services to create
	 * @param [in] srv_count Number of services in the list
	 *
	 * @return True is the services list has been correctly initialized, false otherwise.
	 */
	bool initNewServicesList(const T_ASW_manager_service_list *srv_list, uint8_t srv_count);

	/*!
	 * @brief Service initialization function
	 * @details This function is used to initialize a new service in the SW.
	 * 			First the function checks if the requested service is already initialized or not.
	 * 			If not, this service is initialized by function InitializeService.
	 *
	 * @param [in] srv_type Type of service to create.
	 * @return True is the service has been correctly initialized, false otherwise.
	 */
	bool initNewService(T_ASW_manager_service_list srv_type);


	/*!
	 * @brief Service pointer get function
	 * @details This function finds the address of the given service object and returns it.
	 * 			If the requested service doesn't exist, the returned value is zero.
	 *
	 * @param [in] srv_type Service type to find
	 * @return Pointer to the requested service object
	 */
	Service* getServicePointer(T_ASW_manager_service_list srv_type);

	/*!
	 * @brief PeriodicService pointer get function
	 * @details This function finds the address of the given periodic service object and returns it.
	 * 			If the requested service doesn't exist or if it is not a periodic service, the returned value is zero.
	 *
	 * @param [in] srv_type Service type to find
	 * @return Pointer to the requested periodic service object
	 */
	PeriodicService* getPeriodicServicePointer(
			T_ASW_manager_service_list srv_type);

	/*!
	 * @brief Service init status function
	 * @details This returns the initialization status of the requested service (true/false)
	 *
	 * @param [in] srv_type Service type to find
	 * @return Init status
	 */
	bool isServiceInitialized(T_ASW_manager_service_list srv_type) const;


private:

	T_ASW_Manager_service_struct servicesList; /*!< Structure containing the service objects */

	/*!
	 * @brief Service initialization function
	 * @details This function shall be coded in configuration file.
	 * 			It initializes the requested service and returns the pointer to the requested object.
	 *
	 * @param [in] service_type Type of service to create
	 * @return True if the service has been correctly initialized, False otherwise
	 */
	bool initializeService(T_ASW_manager_service_list service_type);

	/*!
	 * @brief Service nature getting function
	 * @details This function returns the nature of the requested service, STATIC or PERIODIC.
	 *
	 * @param [in] type Type of the service
	 * @return Nature of the service
	 */
	T_ASW_manager_service_nature getServiceNature(
			T_ASW_manager_service_list type) const;

	/*!
	 * @brief Services structure reset function
	 * @details This function resets the service structure by setting all init flags to false
	 *
	 * @return Nothing
	 */
	void resetServiceStruct();

	/*!
	 * @brief Available services count get function
	 * @details This function returns the number of available services (including running, paused and stopped services).
	 *
	 * @return Number of available services
	 */
	uint8_t getAvailableServicesCount() const;

	/*!
	 * @brief Service pointer get function
	 * @details This function returns the pointer to the requested service object
	 *
	 * @param [in] srv_type Service type to find
	 * @return Service pointer
	 */
	Service* getPointer(T_ASW_manager_service_list srv_type);

};

extern ASW_manager p_global_ASW_manager; /*!< ASW manager object */

#endif /* WORK_ASW_ASW_H_ */
