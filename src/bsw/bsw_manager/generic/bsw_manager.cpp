/*!
 * @file bsw_manager.cpp
 * @brief BSW manager source file
 *
 *  @date Tue Apr  5 20:18:17     2022
 *  @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:17     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Thu Feb 10 10:49:24     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 17:01:27     2022 : 50-corrections-sonar 
 * 
 */



#include <avr/io.h>
#include <stdlib.h>
#include <avr/wdt.h>

#include "../../../os/service/generic/Service.h"
#include "../../watchdog/generic/Watchdog.h"

#include "bsw_manager.h"

BSW_manager p_global_BSW_manager;

BSW_manager::BSW_manager()
{
	initBSW();
}

bool BSW_manager::initBSW()
{
	bool status = true;

	resetDriversStruct();

	/* Initialize watchdog if needed */
	/* Watchdog shall be initialized first, to provoke a watchdog reset as early as possible */
	if(WATCHDOG_ACTIVE)
	{
		status &= initializeDriver(WDG);
		drivers.wdg.timeoutUpdate(WATCHDOG_TMO_VALUE);
	}

	/* Initialize drivers */
	status &= initializeDriver(TIMER);
	status &= initializeDriver(DIO);
	status &= initializeDriver(INT_MGT);

	return status;
}

bool BSW_manager::initializeDriver(T_BSW_manager_driver_list driver_type, uint8_t cnf)
{
	if (!isDriverInitialized(driver_type))
		return initDriver(driver_type, cnf);
	else
		return true;
}

Service* BSW_manager::getDriverPointer(T_BSW_manager_driver_list drv_type)
{
	if (isDriverInitialized(drv_type))
		return getPointer(drv_type);
	else
		return 0;
}




