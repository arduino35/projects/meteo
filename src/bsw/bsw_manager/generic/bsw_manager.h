/*!
 * @file bsw_manager.h
 * @brief BSW manager class header file
 *
 * @date Tue Apr  5 20:18:17     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:17     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Thu Feb 10 10:49:24     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 17:01:28     2022 : 50-corrections-sonar 
 * 
 */

#ifndef WORK_BSW_BSW_H_
#define WORK_BSW_BSW_H_

#include "../config/bsw_manager_cnf.h"

/*!
 * @brief BSW manager class
 * @details This class manages BSW
 */
class BSW_manager
{
public:
	/*!
	 * @brief Class constructor
	 * @details This function initializes the class BSW_manager.\n
	 *
	 * @return Nothing
	 */
	BSW_manager();

	/*!
	 * @brief BSW initialization function
	 * @details This function initializes the class. Drivers Dio, Int, Timer and Watchdog are initialized.
	 *
	 * @return TRUE if initialization has been correctly performed, FALSE otherwise
	 */
	bool initBSW();

	/*!
	 * @brief Driver pointer get function
	 * @details This function finds the address of the given driver object and returns it, only if the driver is correctly initialized.
	 *
	 * @param [in] drv_type Driver type to find
	 * @return Pointer to the requested driver object, 0 if not initialized
	 */
	Service* getDriverPointer(T_BSW_manager_driver_list drv_type);

	/*!
	 * @brief Driver initialization function
	 * @details This function initializes the requested driver, only if it is still uninitialized
	 *
	 * @param [in] drv_type Driver type to find
	 * @param [in] cnf Driver configuration parameter
	 * @return Nothing
	 */
	bool initializeDriver(T_BSW_manager_driver_list drv_type, uint8_t cnf = 0);

	/*!
	 * @brief Driver init status function
	 * @details This returns the initialization status of the requested driver (true/false)
	 *
	 * @param [in] drv_type Driver type to find
	 * @return Init status
	 */
	bool isDriverInitialized(T_BSW_manager_driver_list drv_type) const;

private:

	T_BSW_manager_driver_struct drivers; /*!< Structure containing all driver objects */

	/*!
	 * @brief Drivers structure reset function.
	 * @details This function resets the drivers structure by setting all init flags to false
	 *
	 * @return Nothing
	 *
	 */
	void resetDriversStruct();

	/*!
	 * @brief Driver initialization function
	 * @details This function initializes the requested driver
	 *
	 * @param [in] drv_type Driver type to find
	 * @param [in] cnf Driver configuration parameter
	 * @return Nothing
	 */
	bool initDriver(T_BSW_manager_driver_list drv_type, uint8_t cnf = 0);

	/*!
	 * @brief Driver pointer get function
	 * @details This function finds the address of the given driver object and returns it.
	 *
	 * @param [in] drv_type Driver type to find
	 * @return Pointer to the requested driver object
	 */
	Service* getPointer(T_BSW_manager_driver_list drv_type);

};

extern BSW_manager p_global_BSW_manager; /*!< BSW manager object */

#endif /* WORK_BSW_BSW_H_ */
