/*!
 * @file bsw_manager_cnf.cpp
 *
 * @brief BSW manager configuration file
 *
 * @date Tue Apr  5 20:18:16     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:16     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Apr  1 10:47:17     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 17:01:27     2022 : 50-corrections-sonar 
 * 
 */

#include <avr/io.h>

#include "../../../os/service/generic/Service.h"
#include "../generic/bsw_manager.h"


Service* BSW_manager::getPointer(T_BSW_manager_driver_list driver_type)
{
	Service *drv_ptr = 0;

	switch(driver_type)
	{
	case BMP180:
		drv_ptr = (Service*) &(drivers.bmp180);
		break;
	case CPULOAD:
		drv_ptr = (Service*) &(drivers.cpuLoad);
		break;
	case DHT22:
		drv_ptr = (Service*) &(drivers.dht22);
		break;
	case DIO:
		drv_ptr = (Service*) &(drivers.dio);
		break;
	case I2C_COMM:
		drv_ptr = (Service*) &(drivers.i2c);
		break;
	case INT_MGT:
		drv_ptr = (Service*) &(drivers.interrupts);
		break;
	case LCD_SCREEN:
		drv_ptr = (Service*) &(drivers.lcd);
		break;
	case TIMER:
		drv_ptr = (Service*) &(drivers.timer);
		break;
	case USART_COMM:
		drv_ptr = (Service*) &(drivers.usart);
		break;
	case WDG:
		drv_ptr = (Service*) &(drivers.wdg);
		break;
	default:
		drv_ptr = 0;
		break;
	}

	return drv_ptr;
}

bool BSW_manager::initDriver(T_BSW_manager_driver_list driver_type, uint8_t cnf)
{
	bool retval;

	switch (driver_type)
	{
	case BMP180:
		drivers.bmp180Init = drivers.bmp180.initService();
		retval = drivers.bmp180Init;
		break;
	case CPULOAD:
		drivers.cpuloadInit = drivers.cpuLoad.initService();
		retval = drivers.cpuloadInit;
		break;
	case DHT22:
		drivers.dht22Init = drivers.dht22.initService();
		retval = drivers.dht22Init;
		break;
	case DIO:
		drivers.dioInit = drivers.dio.initService();
		retval = drivers.dioInit;
		break;
	case I2C_COMM:
		drivers.i2cInit = drivers.i2c.initService();
		retval = drivers.i2cInit;
		break;
	case INT_MGT:
		drivers.interruptsInit = drivers.interrupts.initService();
		retval = drivers.interruptsInit;
		break;
	case LCD_SCREEN:
		drivers.lcdInit = drivers.lcd.initService();
		retval = drivers.lcdInit;
		break;
	case TIMER:
		drivers.timerInit = drivers.timer.initService();
		retval = drivers.timerInit;
		break;
	case USART_COMM:
		drivers.usartInit = drivers.usart.initService(cnf);
		retval = drivers.usartInit;
		break;
	case WDG:
		drivers.wdgInit = drivers.wdg.initService();
		retval = drivers.wdgInit;
		break;
	default:
		retval = false;
		break;
	}

	return retval;

}

bool BSW_manager::isDriverInitialized(T_BSW_manager_driver_list driver_type) const
{
	bool retval;

	switch (driver_type)
	{
	case BMP180:
		retval = drivers.bmp180Init;
		break;
	case CPULOAD:
		retval = drivers.cpuloadInit;
		break;
	case DHT22:
		retval = drivers.dht22Init;
		break;
	case DIO:
		retval = drivers.dioInit;
		break;
	case I2C_COMM:
		retval = drivers.i2cInit;
		break;
	case INT_MGT:
		retval = drivers.interruptsInit;
		break;
	case LCD_SCREEN:
		retval = drivers.lcdInit;
		break;
	case TIMER:
		retval = drivers.timerInit;
		break;
	case USART_COMM:
		retval = drivers.usartInit;
		break;
	case WDG:
		retval = drivers.wdgInit;
		break;
	default:
		retval = false;
		break;
	}

	return retval;
}

void BSW_manager::resetDriversStruct()
{
	drivers.dioInit = false;
	drivers.interruptsInit = false;
	drivers.timerInit = false;
	drivers.wdgInit = false;
	drivers.i2cInit = false;
	drivers.lcdInit = false;
	drivers.usartInit = false;
	drivers.cpuloadInit = false;
	drivers.dht22Init = false;
	drivers.bmp180Init = false;
}
