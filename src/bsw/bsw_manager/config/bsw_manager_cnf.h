/*!
 * @file bsw_manager_cnf.h
 *
 * @brief BSW manager configuration header file
 *
 * @date Tue Apr  5 20:18:17     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:17     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Apr  1 16:55:08     2022 : 59-passage-en-allocation-statique 
 * 
 */

#ifndef WORK_BSW_BSW_MANAGER_CONFIG_BSW_MANAGER_CNF_H_
#define WORK_BSW_BSW_MANAGER_CONFIG_BSW_MANAGER_CNF_H_

#include "../../../lib/common_lib/generic/common_types.h"

#include "../../i2c/generic/I2C.h"
#include "../../bmp180/generic/Bmp180.h"
#include "../../cpuLoad/generic/CpuLoad.h"
#include "../../dht22/generic/Dht22.h"
#include "../../dio/generic/Dio.h"
#include "../../int/generic/IntManager.h"
#include "../../lcd/generic/LCD.h"
#include "../../timer/generic/Timer.h"
#include "../../usart/generic/Usart.h"
#include "../../watchdog/generic/Watchdog.h"


/*!< Watchdog shall be activated */
const bool WATCHDOG_ACTIVE = true;

/*!< Watchdog shall be configured with a timeout of 1s */
const uint8_t WATCHDOG_TMO_VALUE = WDG_TMO_1S;

/*!
 * @brief Enumeration defining all available drivers
 */
typedef enum
{
	BMP180,			/*!< Driver for BMP180 sensor */
	CPULOAD,			/*!< Driver for CPU load computation */
	DHT22,			/*!< Driver for DHT22 sensor */
	DIO,			/*!< Driver for DIO ports management */
	I2C_COMM,			/*!< Driver for I2C communication */
	INT_MGT,			/*!< Driver for interrupts management */
	LCD_SCREEN,			/*!< Driver for LCD display */
	MEM_MGT,			/*!< Driver for memory management */
	TIMER,			/*!< Driver for timers management */
	USART_COMM,			/*!< Driver for USART communication */
	WDG,			/*!< Driver for watchdog management */
}
T_BSW_manager_driver_list;

/*!
 * @brief Structure containing all driver objects and their initialization status
 */
typedef struct
{
	Dio dio;
	bool dioInit;
	IntManager interrupts;
	bool interruptsInit;
	Timer timer;
	bool timerInit;
	Watchdog wdg;
	bool wdgInit;
	I2C i2c;
	bool i2cInit;
	LCD lcd;
	bool lcdInit;
	Usart usart;
	bool usartInit;
	CpuLoad cpuLoad;
	bool cpuloadInit;
	Dht22 dht22;
	bool dht22Init;
	Bmp180 bmp180;
	bool bmp180Init;
} T_BSW_manager_driver_struct;


#endif /* WORK_BSW_BSW_MANAGER_CONFIG_BSW_MANAGER_CNF_H_ */
