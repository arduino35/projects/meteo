/*!
 * @file timer.cpp
 *
 * @brief Defines function for class timer.
 *
 * @date Tue Apr  5 20:18:21     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:21     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Feb  8 16:02:22     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 17:01:30     2022 : 50-corrections-sonar 
 * 
 */

#include <avr/io.h>

#include "../../../os/service/generic/Service.h"
#include "Timer.h"

Timer::Timer()
{
	prescaler1 = (uint8_t) 0b100;
	prescaler3 = (uint8_t) 0b100;
	prescaler4 = (uint8_t) 0b100;
}

bool Timer::initService()
{
	prescaler1 = (uint8_t)0b100;
	prescaler3 = (uint8_t)0b100;
	prescaler4 = (uint8_t)0b100;

	return true;
}

void Timer::configureTimer1(uint16_t a_prescaler, uint16_t a_ctcValue)
{
	/* Configure the Timer/Counter1 Control Register A */
	TCCR1A = 0 ;
	/* Configure the Timer/Counter1 Control Register B */
	TCCR1B = (1 << WGM12) ; // Configure timer 1 for CTC mode

	/* Enable CTC interrupt */
	TIMSK1 = (1 << OCIE1A);

	/* Set CTC compare value */
	OCR1A = a_ctcValue;

	/* memorize prescaler value */
	switch(a_prescaler)
	{
	case 1:
		prescaler1 = (uint8_t)0b001;
		break;
	case 8:
		prescaler1 = (uint8_t)0b010;
		break;
	case 64:
		prescaler1 = (uint8_t)0b011;
		break;
	case 256:
		prescaler1 = (uint8_t)0b100;
		break;
	case 1024:
		prescaler1 = (uint8_t)0b101;
		break;
	default:
		/* Keep default value */
		break;
	}
}

void Timer::configureTimer3(uint16_t a_prescaler, uint16_t a_ctcValue)
{
	/* Configure the Timer/Counter3 Control Register A */
	TCCR3A = 0 ;
	/* Configure the Timer/Counter3 Control Register B */
	TCCR3B = (1 << WGM12);     // Configure timer 3 for CTC mode

	/* Enable CTC interrupt */
	TIMSK3 = (1 << OCIE3A);

	/* Set CTC compare value */
	OCR3A = a_ctcValue;

	/* memorize prescaler value */
	switch(a_prescaler)
	{
	case 1:
		prescaler3 = (uint8_t)0b001;
		break;
	case 8:
		prescaler3 = (uint8_t)0b010;
		break;
	case 64:
		prescaler3 = (uint8_t)0b011;
		break;
	case 256:
		prescaler3 = (uint8_t)0b100;
		break;
	case 1024:
		prescaler3 = (uint8_t)0b101;
		break;
	default:
		/* Keep default value */
		break;
	}
}

void Timer::configureTimer4(uint16_t a_prescaler, uint16_t a_ctcValue)
{
	/* Configure the Timer/Counter4 Control Register A */
	TCCR4A = 0 ;
	/* Configure the Timer/Counter4 Control Register B */
	TCCR4B = 0;     // Configure timer 4 for normal mode

	/* No interrupt */
	TIMSK4 = 0;

	/* memorize prescaler value */
	switch(a_prescaler)
	{
	case 1:
		prescaler4 = (uint8_t)0b001;
		break;
	case 8:
		prescaler4 = (uint8_t)0b010;
		break;
	case 64:
		prescaler4 = (uint8_t)0b011;
		break;
	case 256:
		prescaler4 = (uint8_t)0b100;
		break;
	case 1024:
		prescaler4 = (uint8_t)0b101;
		break;
	default:
		/* Keep default value */
		break;
	}
}


void Timer::startTimer1() const
{
	uint8_t mask = (uint8_t)0b111;

	/* Reset bits 0-2 of TCCR1 */
	TCCR1B = TCCR1B & (~mask);

	/* Set bits 0-2 of TCCR1 to prescaler value to start the timer */
	TCCR1B = TCCR1B | prescaler1;
}

void Timer::startTimer3() const
{
	uint8_t mask = (uint8_t)0b111;

	/* Reset bits 0-2 of TCCR3 */
	TCCR3B = TCCR3B & (~mask);

	/* Set bits 0-2 of TCCR3 to prescaler value to start the timer */
	TCCR3B = TCCR3B | prescaler3;
}

void Timer::startTimer4() const
{
	uint8_t mask = (uint8_t)0b111;

	/* Reset bits 0-2 of TCCR4 */
	TCCR4B = TCCR4B & (~mask);

	/* Set bits 0-2 of TCCR4 to prescaler value to start the timer */
	TCCR4B = TCCR4B | prescaler4;
}

void Timer::stopTimer1() const
{
	/* Reset bits 0-2 of TCCR1 */
	uint8_t mask = (uint8_t)0b111;
	TCCR1B = TCCR1B & (~mask);
}

void Timer::stopTimer3() const
{
	/* Reset bits 0-2 of TCCR3 */
	uint8_t mask = (uint8_t)0b111;
	TCCR3B = TCCR3B & (~mask);
}

void Timer::stopTimer4() const
{
	/* Reset bits 0-2 of TCCR4 */
	uint8_t mask = (uint8_t)0b111;
	TCCR4B = TCCR4B & (~mask);
}


