/*!
 * @file timer.h
 *
 * @brief Timer class header file
 *
 * @date Tue Apr  5 20:18:21     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:21     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Apr  1 17:20:48     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 17:01:31     2022 : 50-corrections-sonar 
 * 
 */

#ifndef WORK_BSW_TIMER_TIMER_H_
#define WORK_BSW_TIMER_TIMER_H_


/*!
 * @brief Class defining a timer
 * @details This class defines a timer/counter. The selected timer is configured in CTC mode and interrupts are enabled.
 * 			The prescaler value and CTC value can both be configured by user.
 *
 */
class Timer : public Service
{
public:
	/*!
	 * @brief Class constructor
	 * @details This function initializes class attributes
	 * @return Nothing
	 */
	Timer();

	/*!
	 * @brief Service initialization
	 * @details This function initializes the class.
	 *
	 * @return Always TRUE
	 */
	virtual bool initService();

	/*!
	 * @brief Configures Timer #1
	 * @details This function configures hardware timer #1 in CTC mode, enables its interrupts, sets prescaler to a_prescaler and CTC value to a_ctcValue
	 * @param [in] a_prescaler prescaler value
	 * @param [in] a_ctcValue Value to which the counter will compare before raising an interrupt
	 * @return Nothing
	 */
	void configureTimer1(uint16_t a_prescaler, uint16_t a_ctcValue);

	/*!
	 * @brief Configures Timer #3
	 * @details This function configures hardware timer #3 in CTC mode, enables its interrupts, sets prescaler to a_prescaler and CTC value to a_ctcValue
	 * @param [in] a_prescaler prescaler value
	 * @param [in] a_ctcValue Value to which the counter will compare before raising an interrupt
	 * @return Nothing
	 */
	void configureTimer3(uint16_t a_prescaler, uint16_t a_ctcValue);

	/*!
	 * @brief Configures Timer #4
	 * @details This function configures hardware timer #4 in CTC mode, enables its interrupts, sets prescaler to a_prescaler and CTC value to a_ctcValue
	 * @param [in] a_prescaler prescaler value
	 * @param [in] a_ctcValue Value to which the counter will compare before raising an interrupt
	 * @return Nothing
	 */
	void configureTimer4(uint16_t a_prescaler, uint16_t a_ctcValue);

	/*!
	 * @brief Start Timer #1
	 * @details This functions starts Timer #1. Timer shall be initialized before this function is called.
	 * @return Nothing
	 */
	void startTimer1() const;

	/*!
	 * @brief Start Timer #3
	 * @details This functions starts Timer #3. Timer shall be initialized before this function is called.
	 * @return Nothing
	 */
	void startTimer3() const;

	/*!
	 * @brief Start Timer #4
	 * @details This functions starts Timer #4. Timer shall be initialized before this function is called.
	 * @return Nothing
	 */
	void startTimer4() const;

	/*!
	 * @brief Stops Timer #1
	 * @details This functions stops timer #1 by resetting bits 0-2 of TCCR1B
	 * @return Nothing
	 */
	void stopTimer1() const;

	/*!
	 * @brief Stops Timer #3
	 * @details This functions stops timer #3 by resetting bits 0-2 of TCCR3B
	 * @return Nothing
	 */
	void stopTimer3() const;

	/*!
	 * @brief Stops Timer #4
	 * @details This functions stops timer #4 by resetting bits 0-2 of TCCR3B
	 * @return Nothing
	 */
	void stopTimer4() const;

	/*!
	 * @brief Reset Timer #4
	 * @details This functions resets timer #4 by resetting register TCNT4
	 * @return Nothing
	 */
	inline void resetTimer4() const
	{
		TCNT4 = (uint16_t) 0;
	}

	/*!
	 * @brief Reads current value of timer #1
	 * @details This function reads the value of of timer #1 using register TCNT1. The function is inlined to speed up SW execution.
	 *
	 * @return Current timer value
	 */
	inline uint16_t getTimer1Value() const
	{
		return TCNT1;
	}

	/*!
	 * @brief Reads current value of timer #3
	 * @details This function reads the value of of timer #3 using register TCNT3. The function is inlined to speed up SW execution.
	 *
	 * @return Current timer value
	 */
	inline uint16_t getTimer3Value() const
	{
		return TCNT3;
	}

	/*!
	 * @brief Reads current value of timer #4
	 * @details This function reads the value of of timer #4 using register TCNT4. The function is inlined to speed up SW execution.
	 *
	 * @return Current timer value
	 */
	inline uint16_t getTimer4Value() const
	{
		return TCNT4;
	}

private:
	uint8_t prescaler1;
	uint8_t prescaler3;
	uint8_t prescaler4;

};

#endif /* WORK_BSW_TIMER_TIMER_H_ */
