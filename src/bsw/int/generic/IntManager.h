/*!
 * @file IntManager.h
 *
 * @brief Interrupt manager header file
 *
 * @date Tue Apr  5 20:18:20     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:20     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Apr  1 17:20:47     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 17:01:29     2022 : 50-corrections-sonar 
 * 
 */

#ifndef WORK_BSW_INT_GENERIC_INTMANAGER_H_
#define WORK_BSW_INT_GENERIC_INTMANAGER_H_

#include "../config/int_cnf.h"

/*!
 * @brief Interrupt types enumeration
 */
typedef enum
{
	NONE, /*!< No interrupt is configured */
	EXTERNAL,	/*!< Defines an external interrupt */
	SOFTWARE	/*!< Defines a software interrupt */
}
T_Int_types;

/* Interrupt configuration encoding macro */
#define ENCODE_INTX_CNF(sense, pin)		(uint8_t)((uint8_t)((uint8_t)((uint8_t)sense & (uint8_t)0x03) << 6) + pin)
#define DECODE_SENSE_CTRL(cfg)			(uint8_t)(((uint8_t)cfg & (uint8_t)0xC0) >> 6)
#define EXTRACT_PIN(cfg)				(uint8_t)((uint8_t)cfg & (uint8_t)0x3F)

/* Definition of sense control for INTx pins */
/*!< The low level of INTx generates an interrupt request */
const uint8_t INT_MGT_SENSECTL_INTX_LOW_LVL = 0;

/*!< Any edge of INTx generates asynchronously an interrupt request */
const uint8_t INT_MGT_SENSECTL_INTX_ANY_EDGE = 1;

/*!< Falling edge of INTx generates asynchronously an interrupt request */
const uint8_t INT_MGT_SENSECTL_INTX_FALLING_EDGE = 2;

/*!< Rising edge of INTx generates asynchronously an interrupt request */
const uint8_t INT_MGT_SENSECTL_INTX_RISING_EDGE = 3;


/*!
 * @brief Interrupts management class
 * @details This class manages all interrupts that are not directly managed by a specific HW module (i.e. USART, timers...),
 * 		    for example external interrupts.
 */
class IntManager : public Service
{

public:

	/*!
	 * @brief Class constructor
	 * @details This function initializes the class IntManager
	 *
	 * @return Nothing
	 */
	IntManager();

	/*!
	 * @brief Class initialization function
	 * @details This function initializes the class
	 *
	 * @return Always TRUE
	 */
	virtual bool initService();

	/*!
	 * @brief Interrupt creation function
	 * @details This function creates a new interrupt according to the given type and configuration : \n
	 *              - For an external or software interrupt on INTx port : the selected pin to use is given
	 *              on bits 0-5 of configuration parameter, bits 6-7 are the interrupt sense control.
	 *
	 * @param [in] a_type Type of the interrupt to create
	 * @param [in] a_cfg  Configuration of the new interrupt
	 *
	 * @return ID of the created interrupt, 0xFF if the interrupt was not created
	 */
	uint8_t createNewInt(T_Int_types a_type, uint8_t a_cfg);

	/*!
	 * @brief SW interrupt trigger function
	 * @details This function triggers a software interrupt. The interrupt to trigger is given in parameter.
	 *
	 * @param [in] id ID of the interrupt to trigger
	 * @return Nothing
	 */
	void triggerSWInt(uint8_t id) const;

private:

	/*!
	 * @brief Type defining an interrupt configuration
	 * @details This structure contains all data used to manage an interrupt
	 */
	typedef struct
	{
		T_Int_types type;	/*!< Interrupt type */
		uint8_t cfg;		/*!< Interrupt configuration */
	}
	T_Int_Config;

	T_Int_Config intTable[INT_CNF_TABLE_SIZE]; /*!< Table containing the configured interrupts */
	uint8_t intCount; /*!< Number of defined interrupts */
};

#endif /* WORK_BSW_INT_GENERIC_INTMANAGER_H_ */
