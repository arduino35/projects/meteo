/*!
 * @file IntManager.cpp
 *
 * @brief Interrupts manager source code file
 *
 * @date Fri Jul  1 16:58:30     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Jul  1 16:58:30     2022 : 75-changement-de-page-sur-pushbutton 
 * Tue Apr  5 20:18:20     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Feb  8 16:02:21     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 17:01:29     2022 : 50-corrections-sonar 
 * 
 */

#include <stdlib.h>
#include <avr/io.h>


#include "../../../os/service/generic/PeriodicService.h"
#include "../../../asw/asw_manager/generic/asw_manager.h"
#include "../../../asw/sensors/sensor/generic/Sensor.h"
#include "../../bsw_manager/generic/bsw_manager.h"
#include "../../dio/generic/Dio.h"

#include "IntManager.h"


IntManager::IntManager()
{
	initService();
}

bool IntManager::initService()
{
	for (uint8_t i = 0; i < INT_CNF_TABLE_SIZE; i++)
		intTable[i].type = NONE;

	intCount = 0;

	return true;
}

uint8_t IntManager::createNewInt(T_Int_types a_type, uint8_t a_cfg)
{
	uint8_t retval = 0xFF;

	/* If the table is not full */
	if (intCount < INT_CNF_TABLE_SIZE)
	{
		/* The configuration received in parameter contains the pin INTx to use for the interrupt on bits 0-5 */
		/* The configuration received in parameter contains the sense control to use for the interrupt on bits 6-7 */
		uint8_t pinNr = EXTRACT_PIN(a_cfg);
		uint8_t senseCtrl = DECODE_SENSE_CTRL(a_cfg);

		/* Check the received configuration */
		if ((pinNr <= 7) && (senseCtrl <= 3))
		{
			/* HW configuration */
			if (pinNr <= 3)
			{
				/* Clear register for the selected pin */
				EICRA = EICRA & (~(3 << (pinNr * 2)));

				/* Set register for the selected pin */
				EICRA = EICRA | (uint8_t) (senseCtrl << (pinNr * 2));
			}
			else
			{
				/* Clear register for the selected pin */
				EICRB = EICRB & (~(3 << ((pinNr - 4) * 2)));

				/* Set register for the selected pin */
				EICRB = EICRB | (uint8_t) (senseCtrl << ((pinNr - 4) * 2));
			}

			/* Enable interrupt */
			EIMSK = EIMSK | (uint8_t) (1 << pinNr);
		}

		/* If external interrupt, configure the pin as input */
		if (a_type == EXTERNAL)
		{
			((Dio*) (p_global_BSW_manager.getDriverPointer(DIO)))->dio_changePortPinCnf(ENCODE_PORT(PORT_E, 4),
			        PORT_CNF_IN);
		}

		/* Store the new interrupt in the chain */
		intTable[intCount].type = a_type;
		intTable[intCount].cfg = a_cfg;

		retval = intCount;
		intCount++;
	}

	return retval;
}

void IntManager::triggerSWInt(uint8_t id) const
{

	/* Check that the requested interrupt exists */
	if ((id < intCount) && (intTable[id].type == SOFTWARE))
	{
		/* Extract pin sense control */
		uint8_t senseCtrl = DECODE_SENSE_CTRL(intTable[id].cfg);

		/* Select the correct DIO port */
		uint8_t port;
		if (EXTRACT_PIN(intTable[id].cfg) < 4)
			port = PORT_D;
		else
			port = PORT_E;

		if (senseCtrl == INT_MGT_SENSECTL_INTX_ANY_EDGE)
		{
			/* Toggle the INTx pin */
			((Dio*) (p_global_BSW_manager.getDriverPointer(DIO)))->dio_invertPort(
			        ENCODE_PORT(port, EXTRACT_PIN(intTable[id].cfg)));
		}

	}
}
