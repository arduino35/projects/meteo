/*!
 * @file int_cnf.cpp
 *
 * @brief Interrupt management configuration file
 *
 * @date Fri Jul  1 16:58:30     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Jul  1 16:58:30     2022 : 75-changement-de-page-sur-pushbutton 
 * Tue Apr  5 20:18:20     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Apr  1 16:55:08     2022 : 59-passage-en-allocation-statique 
 * Mon Jan 24 23:44:45     2022 : 57-corrections-bugs-bite 
 * Fri Jan 21 21:39:13     2022 : 48-synchro-host-avec-identification 
 * Fri Jan 21 20:59:42     2022 : 55-changement-chaine-tx-usart 
 * Wed Dec 22 21:51:55     2021 : 50-corrections-sonar 
 * 
 */

#include <stdlib.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "../../../os/service/generic/PeriodicService.h"
#include "../../../os/scheduler/generic/Scheduler.h"

#include "../../bsw_manager/generic/bsw_manager.h"
#include "../../../asw/asw_manager/generic/asw_manager.h"

#include "int_cnf.h"



/***************************************************************************/
/*                  EXTERNAL INTERRUPTS CONFIGURATION                      */
/***************************************************************************/


/*!
 * @brief INT5 SW interrupt
 * @details The INT5 interrupt triggers the BITE service reception handler
 *
 * @return Nothing
 */
ISR(INT5_vect)
{
	((BiteManager*) (p_global_ASW_manager.getServicePointer(BITE)))->usartReceivedDataISR();
}

/*!
 * @brief INT4 SW interrupt
 * @details The INT4 interrupt triggers the Display Management switching function
 *
 * @return Nothing
 */
ISR(INT4_vect)
{
	((DisplayManagement*) (p_global_ASW_manager.getServicePointer(LCD_DISPLAY)))->switchPage();
}



/***************************************************************************/
/*                  MODULES INTERRUPTS CONFIGURATION                       */
/***************************************************************************/

/* This section regroup all interrupts handler that are not
 * configured directly inside the modules drivers */

/*!
 * @brief Main software interrupt
 * @details This function handles the interrupt raised by Timer #1. It wakes up the software every 500 ms to perform applications.
 * @return Nothing
 */
ISR(TIMER1_COMPA_vect)
{
	sei();
	p_global_scheduler.launchPeriodicTasks();
}

/*!
 * @brief Bmp180 end of conversion interrupt
 * @details This function calls the end of conversion function of BMP180 driver.
 * @return Nothing
 */
ISR(TIMER3_COMPA_vect)
{
	((Bmp180*) (p_global_BSW_manager.getDriverPointer(BMP180)))->conversionTimerInterrupt();
}


/*!
 * @brief USART 0 Rx Complete interrupt
 * @details This function handles the interrupt raised when a frame has been received by USART. If debug mode mode is active, it calls debug mode management function.
 *
 * @return Nothing
 */
ISR(USART0_RX_vect)
{
	((Usart*) p_global_BSW_manager.getDriverPointer(USART_COMM))->receptionISRHandler();
}

/*!
 * @brief USART 1 Rx Complete interrupt
 * @details This function handles the interrupt raised when a frame has been received by USART.
 *
 * @return Nothing
 */
ISR(USART1_RX_vect)
{
//	Usart* drv_ptr = (Usart*)p_global_BSW_manager.getDriverPointer(USART_COMM, 1);

	/* If the USART driver exists */
//	if(drv_ptr != 0)
//		drv_ptr->receptionISRHandler();

}
