/*!
 * @file int_cnf.h
 *
 * @brief Interrupt configuration header file
 *
 * @date Thu Feb 10 20:27:52     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Thu Feb 10 20:27:53     2022 : 59-passage-en-allocation-statique 
 * Wed Dec 22 21:51:56     2021 : 50-corrections-sonar 
 * 
 */

#ifndef WORK_BSW_INT_CONFIG_INT_CNF_H_
#define WORK_BSW_INT_CONFIG_INT_CNF_H_

/*!< Size of interrupt table */
const uint8_t INT_CNF_TABLE_SIZE = 10;


#endif /* WORK_BSW_INT_CONFIG_INT_CNF_H_ */
