/*!
 * @file CpuLoad.h
 *
 * @brief CpuLoad class header file
 *
 * @date Tue Apr  5 20:18:18     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:18     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Feb 22 15:10:01     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 17:01:28     2022 : 50-corrections-sonar 
 * 
 */

#ifndef WORK_BSW_CPULOAD_CPULOAD_H_
#define WORK_BSW_CPULOAD_CPULOAD_H_

#include "../config/CpuLoad_cnf.h"

/*!
 * @brief Class defining CPU load libraries
 * @details This class defines tools to compute and monitor CPU load.
 */
class CpuLoad
{
public:
	/*!
	 * @brief CpuLoad class constructor
	 * @details This function initializes class CpuLoad.
	 *
	 * @return Nothing
	 */
	CpuLoad();

	/*!
	 * @brief Driver initialization function.This function initializes class CpuLoad. It also initializes Timer driver in case it is still not active.
	 * 			Normally the CpuLoad class is used by the scheduler object, which should initialize the timer object.
	 * 			Thus the initialization of Timer object in CpuLoad class should not be needed. We still do the check here to avoid any issue with null pointer.
	 *
	 * @return True if the initialization has been performed correctly, False otherwise
	 */
	bool initService();

	/*!
	 * @brief Computes current CPU load
	 * @details This function computes the current CPU load using value of the timer used by the scheduler at the end of the periodic cycle.
	 * 			This value is divided by the PIT period to obtain CPU load.
	 *
	 * @return Nothing
	 */
	void computeCPULoad();

	/*!
	 * @brief Get current CPU load value
	 * @details This function returns the current CPU load value
	 *
	 * @return Current CPU load value
	 */
	inline uint16_t getCurrrentCPULoad() const
	{
		return current_load;
	}

	/*!
	 * @brief Get average CPU load value
	 * @details This function returns the average CPU load value
	 *
	 * @return Average CPU load value
	 */
	inline uint16_t getAverageCPULoad() const
	{
		return avg_load;
	}

	/*!
	 * @brief Get maximum CPU load value
	 * @details This function returns the maximum CPU load value
	 *
	 * @return Maximum CPU load value
	 */
	inline uint16_t getMaxCPULoad() const
	{
		return max_load;
	}

private:
	uint16_t current_load; 					/*!< Current CPU load (load of last cycle) */
	uint16_t avg_load; 						/*!< Average CPU load based on the last 50 cycles */
	uint16_t max_load; 						/*!< Maximum CPU load since power on */
	uint8_t sample_cnt; 					/*!< Number of samples used to compute average load */
	uint16_t sample_mem[NB_OF_SAMPLES]; 	/*!< Memorization of the last NB_OF_SAMPLES measures */
	uint8_t sample_idx; 					/*!< Current measurement index (used to memorize the current measure at the correct location in table) */
	uint32_t last_sum_value; 				/*!< Value of the last computed sum (it will reduce the number of samples to sum and speed up execution time) */
};

#endif /* WORK_BSW_CPULOAD_CPULOAD_H_ */
