/*!
 * @file CpuLoad.cpp
 *
 * @brief Defines functions of class CpuLoad
 *
 * @date Tue Apr  5 20:18:17     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:18     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Feb 22 15:10:00     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 17:01:28     2022 : 50-corrections-sonar 
 * Wed Oct 27 17:25:55     2021 : 36-valeur-charge-cpu-incorrecte 
 * 
 */

#include <stdlib.h>
#include <avr/io.h>

#include "../../../os/service/generic/PeriodicService.h"

#include "../../../os/scheduler/generic/Scheduler.h"
#include "../../bsw_manager/generic/bsw_manager.h"

#include "CpuLoad.h"


CpuLoad::CpuLoad()
{
	/* Initialize class members */
	current_load = 0;
	avg_load = 0;
	sample_cnt = 0;
	sample_idx = 0;
	last_sum_value = 0;
	max_load = 0;
}

bool CpuLoad::initService()
{
	/* Initialize class members */
	current_load = 0;
	avg_load = 0;
	sample_cnt = 0;
	sample_idx = 0;
	last_sum_value = 0;
	max_load = 0;

	for (uint8_t i = 0; i < NB_OF_SAMPLES; i++)
		sample_mem[i] = 0;

	/* Initialize timer driver */
	return p_global_BSW_manager.initializeDriver(TIMER);
}

void CpuLoad::computeCPULoad()
{
	/* Compute current load */
	current_load =
	        (uint16_t) (((uint32_t) ((Timer*) p_global_BSW_manager.getDriverPointer(TIMER))->getTimer1Value()
	        * 100) / TIMER_CTC_VALUE);

	if(sample_cnt < NB_OF_SAMPLES)
		sample_cnt++;

	/* Compute average load */
	last_sum_value = last_sum_value - sample_mem[sample_idx] + current_load;
	avg_load = (uint16_t) (last_sum_value / sample_cnt);

	/* Memorize the value */
	sample_mem[sample_idx] = current_load;
	sample_idx = (sample_idx + 1) % NB_OF_SAMPLES;

	/* Compute maximum load */
	if(current_load > max_load)
		max_load = current_load;

}

