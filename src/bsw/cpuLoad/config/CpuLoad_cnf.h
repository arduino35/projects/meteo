/*!
 * @file CpuLoad_cnf.h
 *
 * @brief CPU load configuration file
 *
 * @date Tue Feb 22 15:10:00     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Feb 22 15:10:00     2022 : 59-passage-en-allocation-statique 
 * Fri Jan  7 20:47:56     2022 : 51-optimisation-des-typages 
 * 
 */

#ifndef WORK_BSW_CPULOAD_CONFIG_CPULOAD_CNF_H_
#define WORK_BSW_CPULOAD_CONFIG_CPULOAD_CNF_H_

/*!< Number of samples for average load computation */
const uint8_t NB_OF_SAMPLES = 100;



#endif /* WORK_BSW_CPULOAD_CONFIG_CPULOAD_CNF_H_ */
