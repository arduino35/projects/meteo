/*!
 * @file Watchdog.h
 *
 * @brief Class Watchdog header file
 *
 * @date Tue Apr  5 20:18:22     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:22     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Apr  1 17:20:48     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 17:01:32     2022 : 50-corrections-sonar 
 * 
 */

#ifndef WORK_BSW_WDT_WATCHDOG_H_
#define WORK_BSW_WDT_WATCHDOG_H_

#include <avr/wdt.h>

/*!
 * @brief Definition of available timeout values
 */
#define WDG_TMO_15MS WDTO_15MS /*!< Timeout value is 15 ms */
#define WDG_TMO_30MS WDTO_30MS /*!< Timeout value is 30 ms */
#define WDG_TMO_60MS WDTO_60MS /*!< Timeout value is 60 ms */
#define WDG_TMO_120MS WDTO_120MS /*!< Timeout value is 120 ms */
#define WDG_TMO_250MS WDTO_250MS /*!< Timeout value is 250 ms */
#define WDG_TMO_500MS WDTO_500MS /*!< Timeout value is 500 ms */
#define WDG_TMO_1S WDTO_1S /*!< Timeout value is 1 s */
#define WDG_TMO_2S WDTO_2S /*!< Timeout value is 2 s */
#define WDG_TMO_4S WDTO_4S /*!< Timeout value is 4 s */
#define WDG_TMO_8S WDTO_8S /*!< Timeout value is 8 s */

/*!
 * @brief Watchdog management class
 * @details This class provides services to manage the watchdog HW module. The watchdog shall be reset periodically to avoid a hardware reset of the system.
 */
class Watchdog : public Service
{
public:

	/*!
	 * @brief Class constructor
	 * @details This function initializes the watchdog class.
	 *
	 * @return Nothing
	 */
	Watchdog();

	/*!
	 * @brief Service initialization function
	 * @details  This function initializes the watchdog class. It enables the HW watchdog with a default timeout value.
	 *
	 * @return Always TRUE
	 */
	virtual bool initService();

	/*!
	 * @brief Watchdog reset function
	 * @details This function resets the watchdog timer by calling wdt_reset macro
	 *
	 * @return Nothing
	 */
	void reset() const;

	/*!
	 * @brief Watchdog timeout value update function
	 * @details This function updates the timeout value of the watchdog. It disables then re-enables the watchdog.
	 *
	 * @param [in] value New timeout value
	 * @return Nothing
	 */
	void timeoutUpdate(uint8_t value);

	/*!
	 * @brief System reset function
	 * @details This function provokes a system reset by going in an infinite loop. Thus the watchdog will reset the CPU when the timeout occurs.
	 *
	 * @return False if watchdog is inactive
	 */
	bool systemReset() const;

	/*!
	 * @brief Watchdog timeout get value.
	 * @details This function returns the current watchdog timeout value in ms. It has to convert the value of tmo_value into a numeric value of the timeout.
	 *
	 * @return Timeout value.
	 */
	uint16_t getTMOValue() const;

	/*!
	 * @brief Watchdog status function
	 * @details This function returns the current status of the watchdog : enabled or disabled.
	 *
	 * @returns True if the watchdog is enabled, false otherwise.
	 */
	inline bool isEnabled() const
	{
		return isActive;
	}

	/*!
	 * @brief Watchdog switching function
	 * @details This function switches the state of the watchdog. It it was enabled, the function disables the watchdog, and if it was disabled, the function enables it with the memorized timeout value.
	 * 			The function returns the new status of the watchdog.
	 *
	 * @return New status of the watchdog : True if enabled, false if disabled.
	 */
	bool switchWdg();


private:

	uint8_t tmo_value; /*!< Current timeout value */
	bool isActive; /*!< Watchdog activation flag */

	/*!
	 * @brief Watchdog disabling function
	 * @details This function disables the watchdog by calling wdt_disable macro.
	 *
	 * @return Nothing
	 */
	void disable();

	/*!
	 * @brief Watchdog enabling function
	 * @details This function enables the watchdog by calling wdt_enable macro.
	 *
	 * @param [in] value Timeout value
	 * @return Nothing
	 */
	void enable(uint8_t value);
};

#endif /* WORK_BSW_WDT_WATCHDOG_H_ */
