/*!
 * @file Watchdog.cpp
 *
 * @brief Class Watchdog source code file
 *
 * @date Tue May 10 19:21:10     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue May 10 19:21:10     2022 : 72-reset-bite-non-fonctionnel 
 * Tue Apr  5 20:18:22     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Feb  8 16:02:23     2022 : 59-passage-en-allocation-statique 
 * Fri Jan  7 20:47:58     2022 : 51-optimisation-des-typages 
 * Wed Jan  5 17:01:32     2022 : 50-corrections-sonar 
 * Wed Dec  1 16:17:03     2021 : 43-ajout-changement-valeur-wdg-dans-bite 
 * 
 */

#include <avr/wdt.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "../../../os/service/generic/Service.h"
#include "Watchdog.h"

/*!< Default timeout value is set to 500 ms */
#define WDG_TIMEOUT_DEFAULT_MS WDG_TMO_500MS

Watchdog::Watchdog()
{
	isActive = false;
	tmo_value = 0;
}

bool Watchdog::initService()
{
	isActive = false;
	enable(WDG_TIMEOUT_DEFAULT_MS);
	reset(); /* The reset is not mandatory here because the enable function already make a reset, done here by precaution */

	return true;
}


void Watchdog::enable(uint8_t value)
{
	if(!isActive)
	{
		tmo_value = value;
		isActive = true;
		wdt_enable(value);
	}
}

void Watchdog::disable()
{
	if(isActive)
	{
		isActive = false;
		wdt_disable();
	}
}

void Watchdog::reset() const
{
	wdt_reset();
}

void Watchdog::timeoutUpdate(uint8_t value)
{
	/* Check new timeout is more than 500ms (OS period) */
	if (value < 5)
		return;

	if(isActive)
	{
		disable();
		_delay_us(10);
		enable(value);
	}
	else
		tmo_value = value;
}

bool Watchdog::systemReset() const
{
	if (isActive)
	{
		cli();
		while (true);
	}
	else
		return false;
}

uint16_t Watchdog::getTMOValue() const
{
	uint16_t retval;

	switch(tmo_value)
	{
	case 0:
	default:
		retval = 15;
		break;
	case 1:
		retval = 30;
		break;
	case 2:
		retval = 60;
		break;
	case 3:
		retval = 120;
		break;
	case 4:
		retval = 250;
		break;
	case 5:
		retval = 500;
		break;
	case 6:
		retval = 1000;
		break;
	case 7:
		retval = 2000;
		break;
	case 8:
		retval = 4000;
		break;
	case 9:
		retval = 8000;
		break;
	}

	return retval;
}

bool Watchdog::switchWdg()
{
	if(isActive)
		disable();
	else
		enable(tmo_value);

	return isActive;
}
