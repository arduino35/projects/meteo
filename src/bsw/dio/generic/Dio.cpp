/*!
 * @file dio.cpp
 * @brief DIO library
 *
 * @date Tue Apr  5 20:18:18     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:19     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Feb  8 16:02:20     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 18:34:37     2022 : 50-corrections-sonar 
 * 
 */


#include <avr/io.h>

#include "../../../os/service/generic/Service.h"
#include "Dio.h"

Dio::Dio()
{
	PINx_addr_mem = 0;
	PINx_idx_mem = 0;
}

bool Dio::initService()
{
	ports_init();

	/* Initialize class members */
	PINx_addr_mem = getPINxAddress(PORT_A);
	PINx_idx_mem = (uint8_t) 0;

	return true;
}

void Dio::ports_init() const
{
	/* Initialize ports as input or output */
	DDRB = PORTB_CNF_DDRB;
	DDRE = PORTE_CNF_DDRE;

	/* Set initial value for output pins */
	PORTB = PORTB_CNF_PORTB;
	PORTE = PORTE_CNF_PORTE;
}

volatile uint8_t* Dio::getPORTxAddress(uint8_t portcode)
{
	T_DIO_PORT_CONFIG port_idx = DECODE_PORT(portcode);
	volatile uint8_t *port_addr;

	/* Find port address according to index */
	switch(port_idx)
	{
	case PORT_A:
		port_addr = PORTA_PTR;
		break;
	case PORT_B:
		port_addr = PORTB_PTR;
		break;
	case PORT_C:
		port_addr = PORTC_PTR;
		break;
	case PORT_D:
		port_addr = PORTD_PTR;
		break;
	case PORT_E:
		port_addr = PORTE_PTR;
		break;
	default:
		/* default case : normally not reachable */
		port_addr = PORTA_PTR;
		break;
	}

	return port_addr;
}

volatile uint8_t* Dio::getPINxAddress(uint8_t portcode)
{
	T_DIO_PORT_CONFIG port_idx = DECODE_PORT(portcode);
	volatile uint8_t *port_addr;

	/* Find port address according to index */
	switch(port_idx)
	{
	case PORT_A:
		port_addr = PINA_PTR;
		break;
	case PORT_B:
		port_addr = PINB_PTR;
		break;
	case PORT_C:
		port_addr = PINC_PTR;
		break;
	case PORT_D:
		port_addr = PIND_PTR;
		break;
	case PORT_E:
		port_addr = PINE_PTR;
		break;
	default:
		/* default case : normally not reachable */
		port_addr = PINA_PTR;
		break;
	}

	return port_addr;
}

volatile uint8_t* Dio::getDDRxAddress(uint8_t portcode)
{
	T_DIO_PORT_CONFIG port_idx = DECODE_PORT(portcode);
	volatile uint8_t *port_addr;

	/* Find port address according to index */
	switch(port_idx)
	{
	case PORT_A:
		port_addr = DDRA_PTR;
		break;
	case PORT_B:
		port_addr = DDRB_PTR;
		break;
	case PORT_C:
		port_addr = DDRC_PTR;
		break;
	case PORT_D:
		port_addr = DDRD_PTR;
		break;
	case PORT_E:
		port_addr = DDRE_PTR;
		break;
	default:
		/* default case : normally not reachable */
		port_addr = DDRA_PTR;
		break;
	}

	return port_addr;
}



void Dio::dio_setPort(uint8_t portcode, bool state)
{
	uint8_t mask;
	volatile uint8_t *port = getPORTxAddress(portcode);

	mask = ~((uint8_t) (1 << DECODE_PIN(portcode)));
	*port = *port & mask;
	*port = *port | (uint8_t) ((uint8_t) state << DECODE_PIN(portcode));
}

void Dio::dio_invertPort(uint8_t portcode)
{
	volatile uint8_t *port = getPORTxAddress(portcode);

	*port = *port ^ (uint8_t) (1 << DECODE_PIN(portcode));
}


bool Dio::dio_getPort(uint8_t portcode)
{
	bool res;
	uint8_t port_value = *(getPINxAddress(portcode));

	res = (port_value >> DECODE_PIN(portcode)) & 0x1;

	return res;
}

void Dio::dio_changePortPinCnf(uint8_t portcode, uint8_t cnf)
{
	uint8_t mask = ~((uint8_t) (1 << DECODE_PIN(portcode)));
	volatile uint8_t *port = getDDRxAddress(portcode);

	/* If cnf is different from 0 or 1, set it by default to 0 (input) */
	if ((cnf != PORT_CNF_OUT) && (cnf != PORT_CNF_IN))
		cnf = PORT_CNF_IN;

	/* reset selected pin */
	*port = *port & mask;

	/* set selected pin to cnf */
	*port = *port | (uint8_t) (cnf << DECODE_PIN(portcode));
}

void Dio::dio_memorizePINaddress(uint8_t portcode)
{
	PINx_addr_mem = getPINxAddress(portcode);
	PINx_idx_mem = DECODE_PIN(portcode);
}

bool Dio::dio_getPort_fast(void) const
{
	bool res;

	uint8_t port_value = *PINx_addr_mem;

	res = (port_value >> PINx_idx_mem) & 0x1;

	return res;
}
