/*!
 * @file dio_port_cnf.h
 *
 * @brief Digital ports configuration file
 *
 * @date Fri Jul  1 16:58:30     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Jul  1 16:58:30     2022 : 75-changement-de-page-sur-pushbutton 
 * Mon Jan 24 23:44:45     2022 : 57-corrections-bugs-bite 
 * Mon Jan 24 20:49:55     2022 : 48-synchro-host-avec-identification 
 * 
 */

#ifndef WORK_BSW_DIO_DIO_PORT_CNF_H_
#define WORK_BSW_DIO_DIO_PORT_CNF_H_



/*!
 * @brief Defines the configuration of DDRB register
 * @details This constant defines the direction of IO pins of PORT B. It will configure register DDRB.\n
 *          PB0 : N/A\n
 *          PB1 : N/A\n
 *          PB2 : N/A\n
 *          PB3 : N/A\n
 *          PB4 : OUT\n
 *          PB5 : N/A\n
 *          PB6 : OUT\n
 *          PB7 : OUT\n
 */
#define PORTB_CNF_DDRB (uint8_t)0b11010000

/*!
 * @brief Defines the configuration of PORTB register
 * @details This constant defines the initial state of IO pins for PORT B. It will configure register PORTB.
 * 			For outputs pins, it defines the initial level (high or low). For input pins, it defines if the pins is configured as high-Z or pull-up.\n
 *          PB0 : N/A\n
 *          PB1 : N/A\n
 *          PB2 : N/A\n
 *          PB3 : N/A\n
 *          PB4 : LOW\n
 *          PB5 : N/A\n
 *          PB6 : HIGH\n
 *          PB7 : LOW\n
 */
#define PORTB_CNF_PORTB (uint8_t)0b01000000

/*!
 * @brief Defines the configuration of DDRE register
 * @details This constant defines the direction of IO pins of PORT E. It will configure register DDRE.\n
 *          PE0 : N/A\n
 *          PE1 : N/A\n
 *          PE2 : N/A\n
 *          PE3 : N/A\n
 *          PE4 : IN\n
 *          PE5 : OUT\n
 *          PE6 : OUT\n
 *          PE7 : OUT\n
 */
#define PORTE_CNF_DDRE (uint8_t)0b11100000

/*!
 * @brief Defines the configuration of PORTE register
 * @details This constant defines the initial state of IO pins for PORT E. It will configure register PORTE.
 * 			For outputs pins, it defines the initial level (high or low). For input pins, it defines if the pins is configured as high-Z or pull-up.\n
 *          PE0 : N/A\n
 *          PE1 : N/A\n
 *          PE2 : N/A\n
 *          PE3 : N/A\n
 *          PE4 : N/A\n
 *          PE5 : N/A\n
 *          PE6 : N/A\n
 *          PE7 : N/A\n
 */
#define PORTE_CNF_PORTE (uint8_t)0b00000000



#endif /* WORK_BSW_DIO_DIO_PORT_CNF_H_ */
