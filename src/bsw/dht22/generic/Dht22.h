/*!
 * @file dht22.h
 *
 * @brief DHT22 driver header file
 *
 * @date Tue Apr  5 20:18:18     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:18     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Feb 25 15:53:40     2022 : 59-passage-en-allocation-statique 
 * 
 */

#ifndef WORK_BSW_DHT22_DHT22_H_
#define WORK_BSW_DHT22_DHT22_H_

#include "../config/dht22_cnf.h"

/*!
 * @brief DHT 22 driver class
 * @details This class defines all useful functions for DHT22 temperature and humidity sensor
 */
class Dht22 : public Service
{

public:

	/*!
	 * @brief dht22 class constructor.
	 * @details Initializes the class dht22.
	 * @return Nothing
	 */
	Dht22();

	/*!
	 * @brief DHT22 driver initialization function
	 * @details This function initializes the DHT22 driver and the associated DIO driver
	 *
	 * @return True if the initialization has been performed correctly, False otherwise
	 */
	virtual bool initService();

	/*!
	 * @brief Port configuration function
	 * @details This function configures the port used for 1-wire communication.
	 *
	 * @param [in] port Encoded configuration of the port used by the sensor
	 * @return Nothing
	 */
	void ConfigurePort(uint8_t port);

	/*!
	 * @brief Temperature get function
	 * @details This functions writes the temperature value at the given address and returns the validity of the data.
	 * 			If the values have not been refreshed during the current PIT, a read operation is performed on the DHT22 device.
	 *
	 * @param [in] temperature Address where the temperature shall be written
	 * @return Validity of the data
	 */
	bool getTemperature(uint16_t* temperature);

	/*!
	 * @brief Humidity get function
	 * @details This functions writes the humidity value at the given address and returns the validity of the data.
	 * 			If the values have not been refreshed during the current PIT, a read operation is performed on the DHT22 device.
	 *
	 * @param [in] humidity Address where the humidity shall be written
	 * @return Validity of the data
	 */
	bool getHumidity(uint16_t* humidity);


private:

	uint8_t dht22_port; /*!< Variable containing the port used for 1-wire communication */
	uint16_t mem_temperature; /*!< Memorized value of temperature */
	uint16_t mem_humidity; /*!< Memorized value of humidity */
	bool mem_validity; /*!< Memorized value of validity */
	uint32_t pit_last_read; /*!< Value of the PIT number when the last read operation has been performed */

	/*!
	 * @brief Initializes the communication
	 * @details This function initializes the communication with DHT22 using 1-wire protocol
	 * @return Nothing
	 */
	void initializeCommunication() const;

	/*!
	 * @brief Reads the data from DHT22
	 * @details This function communicates with DHT22 using 1-wire protocol to read raw values of temperature and humidity.
	 * 			A checksum check is done when communication is finished to validate the received data.
	 * 			Validity of the data, temperature and humidity values and memorized in the associated class members.
	 *
	 * @return Nothing
	 */
	void read();

};

#endif /* WORK_BSW_DHT22_DHT22_H_ */
