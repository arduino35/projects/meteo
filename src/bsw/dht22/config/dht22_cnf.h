/*!
 * @file dht22_cnf.h
 *
 * @brief DHT22 driver configuration file
 *
 * @date Fri Feb 25 15:53:39     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Feb 25 15:53:39     2022 : 59-passage-en-allocation-statique 
 * Fri Jan  7 20:47:56     2022 : 51-optimisation-des-typages 
 * 
 */

#ifndef WORK_BSW_DHT22_CONFIG_DHT22_CNF_H_
#define WORK_BSW_DHT22_CONFIG_DHT22_CNF_H_


/*!< Maximum waiting time in microseconds */
const uint8_t MAX_WAIT_TIME_US = 100;


#endif /* WORK_BSW_DHT22_CONFIG_DHT22_CNF_H_ */
