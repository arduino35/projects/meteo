/*!
 * @file Bmp180_cnf.h
 *
 * @brief BMP180 driver configuration file
 *
 * @date Fri Apr  1 10:47:16     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Apr  1 10:47:16     2022 : 59-passage-en-allocation-statique 
 * Fri Jan  7 20:47:55     2022 : 51-optimisation-des-typages 
 * 
 */

#ifndef WORK_BSW_BMP180_CONFIG_BMP180_CNF_H_
#define WORK_BSW_BMP180_CONFIG_BMP180_CNF_H_

/*!< Bitrate used for I2C communication */
const uint32_t BMP180_I2C_BITRATE = 100000;

/*!< I2C address of the sensor */
const uint8_t BMP180_I2C_ADDR = 0x77;

/*!< Chip ID EEPROM address */
const uint8_t BMP180_CHIP_ID_EEP_ADDR = 0xD0;

/*!< EEPROM calibration data start address */
const uint8_t BMP180_CHIP_ID_CALIB_EEP_START_ADDR = 0xAA;

/*!< Expected chip ID */
const uint8_t BMP180_CHIP_ID_EXPECTED = 0x55;

/*!< Address of the the measurement control register in EEPROM */
const uint8_t BMP180_CTRL_MEAS_EEP_ADDR = 0xF4;

/*!< Value of measurement control register to start a temperature conversion */
const uint8_t BMP180_CTRL_MEAS_START_TEMP_CONV = 0x2E;

/*!< Value of measurement control register to start a pressure conversion with OSS0 parameter */
const uint8_t BMP180_CTRL_MEAS_START_PRESS_CONV_OSS0 = 0x34;

/*!< Waiting time for a temperature conversion */
const uint16_t BMP180_TEMP_MEAS_WAITING_TIME = 6;



/*!< Waiting time for a pressure conversion with parameter OSS0 */
const uint16_t BMP180_PRESS_MEAS_OSS0_WAITING_TIME = 15;

/*!< Value of prescaler to use for timer */
const uint16_t BMP180_TIMER_PRESCALER_VALUE = 64;

/*!< Address of LSB out register */
const uint8_t BMP180_OUT_REG_LSB_EEPROM_ADDR = 0xF7;

/*!< Address of MSB out register */
const uint8_t BMP180_OUT_REG_MSB_EEPROM_ADDR = 0xF6;

/*!< Monitoring period is set by default to 500ms */
const t_time_ms BMP180_MONITORING_DEFAULT_PERIOD = 500;



#endif /* WORK_BSW_BMP180_CONFIG_BMP180_CNF_H_ */
