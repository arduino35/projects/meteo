/*!
 * @file Bmp180.cpp
 *
 * @brief Bmp180 class source file
 *
 * @date Tue Apr  5 20:18:15     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:16     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Apr  1 10:47:16     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 17:01:27     2022 : 50-corrections-sonar 
 * 
 */

#include <avr/io.h>

#include "../../../lib/common_lib/generic/common_types.h"

#include "../../../os/service/generic/PeriodicService.h"
#include "../../../os/scheduler/generic/Scheduler.h"

#include "../../bsw_manager/generic/bsw_manager.h"
#include "../../i2c/generic/I2C.h"
#include "../../timer/generic/Timer.h"

#include "Bmp180.h"

#define BMP180_TEMP_MEAS_TIMER_CTC_VALUE ((F_CPU/BMP180_TIMER_PRESCALER_VALUE)/(1000/BMP180_TEMP_MEAS_WAITING_TIME)) /*!< Compare value for periodic timer in case of temperature conversion */
#define BMP180_PRESS_MEAS_OSS0_TIMER_CTC_VALUE ((F_CPU/BMP180_TIMER_PRESCALER_VALUE)/(1000/BMP180_PRESS_MEAS_OSS0_WAITING_TIME)) /*!< Compare value for periodic timer in case of pressure conversion with parameter OSS0 */


Bmp180::Bmp180()
{
	/* Set status to OK */
	status = IDLE;

	/* Read chip ID */
	chip_id = 0;

	/* Initialize measurement data */
	temperature_value.ready = false;
	temperature_value.value = 0;
	temperature_value.ts = 0;
	pressure_value.ready = false;
	pressure_value.value = 0;
	pressure_value.ts = 0;

	/* Initialize activation flags */
	isTempConvActivated = false;
	isPressConvActivated = false;

	task_period_temp = BMP180_MONITORING_DEFAULT_PERIOD;
	task_period_press = BMP180_MONITORING_DEFAULT_PERIOD;

	i2c_drv_ptr = 0;
	B5_mem = 0;

}

bool Bmp180::initService()
{
	bool initStatus;

	/* Create new instance of I2C driver */
	initStatus = p_global_BSW_manager.initializeDriver(I2C_COMM);

	if (initStatus)
	{
		i2c_drv_ptr = (I2C*) p_global_BSW_manager.getDriverPointer(I2C_COMM);
		i2c_drv_ptr->setBitRate(BMP180_I2C_BITRATE);

		/* Create timer object */
		initStatus = p_global_BSW_manager.initializeDriver(TIMER);
	}

	if (initStatus)
	{
		/* Set status to OK */
		status = IDLE;

		/* Read chip ID */
		chip_id = 0;
		readChipID();

		/* Read calibration data */
		uint8_t *ptr = (uint8_t*) (&calibration_data);
		for (uint8_t i = 0; i < sizeof(T_BMP180_calib_data); i++)
			ptr[i] = 0;
		readCalibData();

		/* Initialize measurement data */
		temperature_value.ready = false;
		temperature_value.value = 0;
		temperature_value.ts = 0;
		pressure_value.ready = false;
		pressure_value.value = 0;
		pressure_value.ts = 0;

		/* Initialize activation flags */
		isTempConvActivated = false;
		isPressConvActivated = false;

		task_period_temp = BMP180_MONITORING_DEFAULT_PERIOD;
		task_period_press = BMP180_MONITORING_DEFAULT_PERIOD;
	}

	if (initStatus && (status != COMM_FAILED))
		return true;
	else
		return false;
}

void Bmp180::readCalibData()
{
	bool ret_status;

	if(status == IDLE)
	{
		uint8_t buf_calib_data[sizeof(T_BMP180_calib_data)];
		ret_status = i2c_drv_ptr->writeByte(BMP180_CHIP_ID_CALIB_EEP_START_ADDR, BMP180_I2C_ADDR, false);

		if(ret_status)
		{
			ret_status = i2c_drv_ptr->read(BMP180_I2C_ADDR, sizeof(T_BMP180_calib_data), buf_calib_data);
			if(ret_status)
				checkCalibrationData(buf_calib_data);
			else
				status = COMM_FAILED;
		}
		else
			status = COMM_FAILED;
	}
}

void Bmp180::checkCalibrationData(const uint8_t *buf)
{
	/* Check that the read has been correctly performed,
	 * none of the calibration data shall be equal to 0 or 0xffff */
	const uint16_t *cal = (const uint16_t*) buf;
	for (uint8_t i = 0; i < sizeof(T_BMP180_calib_data) / 2; i++)
	{
		if ((*cal == 0) || (*cal == 0xffff))
			status = COMM_FAILED;

		cal++;
	}

	/* If the status is still OK, copy the buffer into the calibration data structure
	 * An inversion is done between MSB and LSB during the copy because AtMega2560 is little endian */
	if (status == IDLE)
	{
		uint8_t *ptr = (uint8_t*) &calibration_data;
		for (uint8_t i = 0; i < sizeof(T_BMP180_calib_data); i = i + 2)
		{
			*ptr = buf[i + 1];
			*(ptr + 1) = buf[i];
			ptr = ptr + 2;
		}
	}
}

void Bmp180::readChipID()
{
	bool ret_status;

	if(status == IDLE)
	{
		ret_status = i2c_drv_ptr->writeByte(BMP180_CHIP_ID_EEP_ADDR, BMP180_I2C_ADDR, false);

		if(ret_status)
		{
			ret_status = i2c_drv_ptr->read(BMP180_I2C_ADDR, 1, &chip_id);
			if((chip_id != BMP180_CHIP_ID_EXPECTED) || (!ret_status))
				status = COMM_FAILED;
		}
		else
			status = COMM_FAILED;
	}
}

bool Bmp180::getTemperatureValue(uint16_t *data) const
{
	*data = temperature_value.value;

	return temperature_value.ready;
}

bool Bmp180::getPressureValue(uint16_t *data) const
{
	*data = pressure_value.value;

	return pressure_value.ready;
}

void Bmp180::startNewTemperatureConversion()
{
	bool ret_status;

	if(status == IDLE)
	{
		uint8_t data[2] = {BMP180_CTRL_MEAS_EEP_ADDR, BMP180_CTRL_MEAS_START_TEMP_CONV};
		ret_status = i2c_drv_ptr->write(data, BMP180_I2C_ADDR, 2, true);

		/* If the conversion is started, start a timer, else set the driver status as failed */
		if(ret_status)
		{
			((Timer*) p_global_BSW_manager.getDriverPointer(TIMER))->configureTimer3(BMP180_TIMER_PRESCALER_VALUE,
			        BMP180_TEMP_MEAS_TIMER_CTC_VALUE);
			((Timer*) p_global_BSW_manager.getDriverPointer(TIMER))->startTimer3();
			status = TEMP_CONV_IN_PROGRESS;
		}
		else
			status = COMM_FAILED;
	}
}

void Bmp180::startNewPressureConversion()
{
	bool ret_status;

	if(status == IDLE)
	{
		uint8_t data[2] = {BMP180_CTRL_MEAS_EEP_ADDR, BMP180_CTRL_MEAS_START_PRESS_CONV_OSS0};
		ret_status = i2c_drv_ptr->write(data, BMP180_I2C_ADDR, 2, true);

		/* If the conversion is started, start a timer, else set the driver status as failed */
		if(ret_status)
		{
			((Timer*) p_global_BSW_manager.getDriverPointer(TIMER))->configureTimer3(BMP180_TIMER_PRESCALER_VALUE,
			        BMP180_PRESS_MEAS_OSS0_TIMER_CTC_VALUE);
			((Timer*) p_global_BSW_manager.getDriverPointer(TIMER))->startTimer3();
			status = PRESSURE_CONV_IN_PROGRESS;
		}
		else
			status = COMM_FAILED;
	}
}

void Bmp180::conversionTimerInterrupt()
{
	bool ret_status;
	uint8_t lsb;
	uint8_t msb;

	/* Stop timer */
	((Timer*) p_global_BSW_manager.getDriverPointer(TIMER))->stopTimer3();

	/* If driver status is OK and a conversion is in progress */
	if (status == COMM_FAILED)
		return;

	/* Read the result in sensor EEPROM */
	ret_status = i2c_drv_ptr->writeByte(BMP180_OUT_REG_LSB_EEPROM_ADDR, BMP180_I2C_ADDR, false);

	if (ret_status)
		ret_status = i2c_drv_ptr->read(BMP180_I2C_ADDR, 1, &lsb);
	else
		status = COMM_FAILED;

	if (ret_status)
		ret_status = i2c_drv_ptr->writeByte(BMP180_OUT_REG_MSB_EEPROM_ADDR, BMP180_I2C_ADDR, false);
	else
		status = COMM_FAILED;

	if (ret_status)
		ret_status = i2c_drv_ptr->read(BMP180_I2C_ADDR, 1, &msb);
	else
		status = COMM_FAILED;

	if (ret_status)
	{
		uint16_t RawValue = (msb << 8) + lsb;
		/* Convert the value in real temperature or pressure */
		if (status == TEMP_CONV_IN_PROGRESS)
		{
			calculateTemperature(RawValue);
			status = IDLE;

			/* Start pressure conversion */
			if (isPressConvActivated)
				startNewPressureConversion();
		}
		else if (status == PRESSURE_CONV_IN_PROGRESS)
		{
			calculatePressure(RawValue);
			status = IDLE;
		}
	}
	else
		status = COMM_FAILED;

}

void Bmp180::calculateTemperature(uint16_t UT)
{
	int32_t X1 = ((int32_t) UT - (int32_t) calibration_data.AC6) * (int32_t) calibration_data.AC5 / 32768;
	int32_t X2 = (int32_t)calibration_data.MC * (int32_t)(2048) / (X1 + (int32_t)calibration_data.MD);
	B5_mem = X1+X2;

	temperature_value.value = (uint16_t) ((B5_mem + 8) / 16);
	temperature_value.ready = true;
	temperature_value.ts = p_global_scheduler.getPitNumber();
}

void Bmp180::calculatePressure(uint32_t UP)
{
	int32_t B6 = B5_mem - 4000;
	int32_t X1 = (calibration_data.B2 * (B6 * B6 / 4096))/ 2048;
	int32_t X2 = calibration_data.AC2 * B6 / 2048;
	int32_t X3 = X1 + X2;
	int32_t B3 = ((calibration_data.AC1 * 4 + X3) + 2) / 4;
	X1 = calibration_data.AC3 * B6 / 8192;
	X2 = (calibration_data.B1 * (B6 * B6 / 4096)) / 65536;
	X3 = ((X1+X2)+2)/4;
	uint32_t B4 = calibration_data.AC4 * (uint32_t)(X3+32768)/32768;
	uint32_t B7 = (UP - B3) * 50000;

	int32_t p;
	if(B7 < 0x80000000)
		p = (B7*2)/B4;
	else
		p = (B7/B4)*2;

	X1 = (p/256)*(p/256);
	X1 = (X1*3038)/65536;
	X2 = (-7357 * p)/65536;
	p = p + (X1 + X2 + 3791)/ 16;

	pressure_value.value = (uint16_t)(p/10); /* Remove last digit */
	pressure_value.ready = true;
	pressure_value.ts = p_global_scheduler.getPitNumber();

}

void Bmp180::bmp180Monitoring_Task()
{
	/* If the status is equal to IDLE, start a new temperature conversion */
	if ((status == IDLE) && isTempConvActivated)
		startNewTemperatureConversion();

	/* Monitoring of temperature and pressure value */
	temperatureMonitoring();
	pressureMonitoring();
}

void Bmp180::temperatureMonitoring()
{
	if (p_global_scheduler.getPitNumber() - temperature_value.ts > ((task_period_temp / SW_PERIOD_MS) * 2))
		temperature_value.ready = false;
}

void Bmp180::pressureMonitoring()
{
	if (p_global_scheduler.getPitNumber() - pressure_value.ts > ((task_period_press / SW_PERIOD_MS) * 2))
		pressure_value.ready = false;
}

void Bmp180::activateTemperatureConversion(uint16_t req_period)
{
	isTempConvActivated = true;
	task_period_temp = req_period;
}

void Bmp180::activatePressureConversion(uint16_t req_period)
{
	isPressConvActivated = true;
	isTempConvActivated = true; /* Temperature conversion needs to be activated to read pressure */

	task_period_press = req_period;
}

void Bmp180::stopTemperatureConversion()
{
	/* Disable temperature conversion only is pressure conversion is also disabled */
	if(!isPressConvActivated)
		isTempConvActivated = false;
}

void Bmp180::stopPressureConversion()
{
	isPressConvActivated = false;
}
