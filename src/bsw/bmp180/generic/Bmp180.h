/*!
 * @file Bmp180.h
 *
 * @brief Bmp180 class header file
 *
 * @date Tue Apr  5 20:18:16     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:16     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Apr  1 16:55:08     2022 : 59-passage-en-allocation-statique 
 * Fri Jan  7 20:47:55     2022 : 51-optimisation-des-typages 
 * Thu Dec 23 20:44:58     2021 : 50-corrections-sonar 
 * 
 */

#ifndef WORK_BSW_BMP180_BMP180_H_
#define WORK_BSW_BMP180_BMP180_H_

#include "../config/Bmp180_cnf.h"

/*!
 * @brief Enumeration defining the possible statuses for BMP180 sensor driver
 */
typedef enum
{
	IDLE, /*!< No conversion is in progress and communication is OK */
	TEMP_CONV_IN_PROGRESS, /*!< A temperature conversion is in progress */
	PRESSURE_CONV_IN_PROGRESS, /*!< A pressure conversion is in progress */
	COMM_FAILED, /*!< Communication is failed */
}
T_BMP180_status;


/*!
 * @brief BMP180 sensor class definition
 * @details This class manages BMP180 driver.
 */
class Bmp180 : public Service
{
public:

	/*!
	 * @brief Bmp180 class constructor
	 * @details This function initializes the class Bmp180.
	 *
	 * @return Nothing
	 */
	Bmp180();

	/*!
	 * @brief BMP180 driver initialization function
	 * @details This function initializes the BMP180 driver. It reads the chip ID and reads calibration data.
	 * 			If the chip ID or calibration data are incorrect, the status is set to communication failed.
	 * 			It also starts the periodic monitoring of the driver.
	 *
	 * @return True if the initialization has been performed correctly, False otherwise
	 */
	virtual bool initService();

	/*!
	 * @brief Temperature reading function
	 * @details This function is used to read the temperature value from BMP180 sensor.
	 *
	 * @param [in] data Pointer the location where the temperature data shall be stored.
	 *
	 * @return True if a temperature measurement is available, false otherwise.
	 */
	bool getTemperatureValue(uint16_t *data) const;

	/*!
	 * @brief Pressure reading function
	 * @details This function is used to read the pressure value from BMP180 sensor.
	 *
	 * @param [in] data Pointer the location where the pressure data shall be stored.
	 *
	 * @return True if a pressure measurement is available, false otherwise.
	 */
	bool getPressureValue(uint16_t *data) const;

	/*!
	 * @brief End of conversion interrupt
	 * @details This function is called by the timer interrupt at the end of the conversion. It will retrieve the
	 * 			raw temperature of pressure value according to the conversion type and then calculate true value.
	 *
	 * @return Nothing
	 */
	void conversionTimerInterrupt();

	/*!
	 * @brief BMP180 periodic monitoring function
	 * @details This function is in charge of monitoring the BMP180 sensor.
	 * 			It starts new conversions of temperature and pressure values. The result of this conversion will be
	 * 			retrieved by an interrupt after 4.5 ms.
	 * 			Temperature and pressure values time stamps are monitored and availability of measures are updated.
	 * 			It also	monitors the status of the driver, if the driver is failed, it tries to restart the sensor device.
	 *
	 * @return Nothing
	 */
	void bmp180Monitoring_Task();

	/*!
	 * @brief Temperature conversion activation function
	 * @details This function activates or the periodic start of temperature conversion. The requested period is transmitted in parameter.
	 *
	 * @param [in] req_period Requested period for temperature conversion
	 * @return Nothing
	 */
	void activateTemperatureConversion(uint16_t req_period);

	/*!
	 * @brief Pressure conversion activation function
	 * @details This function activates or the periodic start of pressure and temperature conversion. The requested period is transmitted in parameter.
	 *
	 * @param [in] req_period Requested period for pressure conversion
	 * @return Nothing
	 */
	void activatePressureConversion(uint16_t req_period);

	/*!
	 * @brief Temperature conversion stop function
	 * @details This function stops or the periodic start of temperature conversion, only if pressure conversion is also disabled.
	 *
	 * @return Nothing
	 */
	void stopTemperatureConversion();

	/*!
	 * @brief Pressure conversion stop function
	 * @details This function stops or the periodic start of pressure conversion.
	 *
	 * @return Nothing
	 */
	void stopPressureConversion();


private:

	I2C* i2c_drv_ptr; /*!< Pointer to the I2C driver object */
	uint8_t chip_id; /*!< Sensor chip ID */
	T_BMP180_status status; /*!< Sensor status */

	t_time_ms task_period_temp; /*!< Period of the temperature monitoring task */
	t_time_ms task_period_press; /*!< Period of the temperature monitoring task */

	bool isTempConvActivated; /*!< Temperature conversion activation flag */
	bool isPressConvActivated; /*!< Pressure conversion activation flag */

	/*!
	 * @brief Structure defining the calibration data of BMP180 sensor
	 */
	typedef struct
	{
		int16_t AC1;
		int16_t AC2;
		int16_t AC3;
		uint16_t AC4;
		uint16_t AC5;
		uint16_t AC6;
		int16_t B1;
		int16_t B2;
		int16_t MB;
		int16_t MC;
		int16_t MD;
	}
	T_BMP180_calib_data;

	T_BMP180_calib_data calibration_data; /*!< Calibration data of the sensor */

	/*!
	 * @brief Structure defining a sensor value and its status
	 */
	typedef struct
	{
		uint16_t value; /*!< Last measurement value */
		bool ready; /*!< Measurement readiness flag */
		uint32_t ts; /*!< Time stamp of last measurement */
	}
	T_BMP180_measurement_data;

	T_BMP180_measurement_data temperature_value; /*!< Temperature data structure */
	T_BMP180_measurement_data pressure_value; /*!< Pressure data structure */

	int32_t B5_mem; /*!< Memorization of B5 coefficient (computed by temperature formula and used for pressure) */


	/*!
	 * @brief Calibration data reading function
	 * @details This function reads the pressure and temperature calibration data in BMP180 EEPROM using I2C bus.
	 * 			It also updates the driver status if the communication with the device is failed.
	 *
	 * @return Nothing.
	 */
	void readCalibData();

	/*!
	 * @brief Chip ID read function
	 * @details This function reads the ID of the sensor chip using I2C bus. It also updates the driver status
	 * 			if the communication is failed.
	 *
	 * @return Nothing
	 */
	void readChipID();

	/*!
	 * @brief Temperature calculation function
	 * @details This function calculates the true temperature from the raw value from sensor according to the BMP180 datasheet.
	 * 			The true temperature value is stored in temperature_value structure.
	 *
	 * @param [in] UT Raw temperature value
	 * @return Nothing
	 */
	void calculateTemperature(uint16_t UT);

	/*!
	 * @brief Pressure calculation function
	 * @details This function calculates the true pressure from the raw value from sensor according to the BMP180 datasheet.
	 * 			The true pressure value is stored in pressure_value structure.
	 *
	 * @param [in] UP Raw pressure value
	 * @return Nothing
	 */
	void calculatePressure(uint32_t UP);

	/*!
	 * @brief Temperature value monitoring function
	 * @details This function monitors the temperature value.
	 * 			It the last correct measurement was done more than twice the monitoring period in the past,
	 * 			the value is declared not ready.
	 *
	 * @return Nothing
	 */
	void temperatureMonitoring();

	/*!
	 * @brief Pressure value monitoring function
	 * @details This function monitors the pressure value.
	 * 			It the last correct measurement was done more than twice the monitoring period in the past,
	 * 			the value is declared not ready.
	 *
	 * @return Nothing
	 */
	void pressureMonitoring();

	/*!
	 * @brief Starts a new temperature conversion
	 * @details This function starts a new temperature conversion by writing 0x2E into register 0xF4 of sensor.
	 * 			It also starts a timer to retrieve sensor data after the conversion time.
	 *
	 * @return Nothing
	 */
	void startNewTemperatureConversion();

	/*!
	 * @brief Starts a new pressure conversion
	 * @details This function starts a new pressure conversion by writing 0x74 into register 0xF4 of sensor.
	 * 			It also starts a timer to retrieve sensor data after the conversion time.
	 *
	 * @return Nothing
	 */
	void startNewPressureConversion();

	/*!
	 * @brief Calibration data checking function
	 * @details This function checks the validity of the previously read calibration data.
	 *          Comm status is updated if the data are not valid. Valid data are copied into calibration structure.
	 *
	 * @param [in] buf Buffered calibration data to check
	 * @return Nothing
	 */
	void checkCalibrationData(const uint8_t *buf);
};

#endif /* WORK_BSW_BMP180_BMP180_H_ */
