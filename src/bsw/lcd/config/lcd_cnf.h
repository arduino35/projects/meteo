/*!
 * @file lcd_cnf.h
 *
 * @brief LCD configuration file
 *
 * @date Tue Feb  8 20:24:01     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Feb  8 20:24:01     2022 : 59-passage-en-allocation-statique 
 * Fri Jan  7 20:47:57     2022 : 51-optimisation-des-typages 
 * 
 */

#ifndef WORK_BSW_LCD_CONFIG_LCD_CNF_H_
#define WORK_BSW_LCD_CONFIG_LCD_CNF_H_


/* Definition of LCD pins connections */
/*!< EN bit is on P2 */
const uint8_t EN_PIN = 2;

/*!< RW pin is on P1 */
const uint8_t RW_PIN = 1;

/*!< RS pin is on P0 */
const uint8_t RS_PIN = 0;

/*!< Backlight pin is on P3 */
const uint8_t BACKLIGHT_PIN = 3;


/* RAM address limits */
/*!< Minimum address value in 1-line mode */
const uint8_t LCD_RAM_1_LINE_MIN = 0;

/*!< Maximum address value in 1-line mode */
const uint8_t LCD_RAM_1_LINE_MAX = 0x4F;

/*!< Minimum address value in 2-lines mode for line 1 */
const uint8_t LCD_RAM_2_LINES_MIN_1 = 0;

/*!< Maximum address value in 2-lines mode for line 1 */
const uint8_t LCD_RAM_2_LINES_MAX_1 = 0x27;

/*!< Minimum address value in 2-lines mode for line 2 */
const uint8_t LCD_RAM_2_LINES_MIN_2 = 0x40;

/*!< Maximum address value in 2-lines mode for line 2 */
const uint8_t LCD_RAM_2_LINES_MAX_2 = 0x67;


/* Definition of waiting times after screen operations in us */
/*!< Waiting time after clear display and return home operations is at least 1520 us */
const uint16_t LCD_WAIT_CLR_RETURN = 1600;

/*!< Waiting time after all other modes is at least 38 us */
const uint16_t LCD_WAIT_OTHER_MODES = 40;

/* Definition of screen sizes */
/*!< LCD screen has 20 characters per line */
const uint8_t LCD_SIZE_NB_CHAR_PER_LINE = 20;

/*!< LCD screen has 4 lines */
const uint8_t LCD_SIZE_NB_LINES = 4;


#endif /* WORK_BSW_LCD_CONFIG_LCD_CNF_H_ */
