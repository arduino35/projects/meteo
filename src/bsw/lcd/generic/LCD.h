/*!
 * @file LCD.h
 *
 * @brief LCD class header file
 *
 * @date Tue Apr  5 20:18:20     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:20     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Fri Apr  1 17:20:48     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 17:01:30     2022 : 50-corrections-sonar 
 * 
 */

#ifndef WORK_BSW_LCD_LCD_H_
#define WORK_BSW_LCD_LCD_H_

#include "../config/lcd_cnf.h"

/* Definition of fields for entry mode set command */
/*!< Field ID (increment or decrement) of command "entry mode set" is on bit DB1 */
const uint8_t LCD_CNF_SHIFT_ID = 1;

/*!< Field SH (shift of display) of command "entry mode set" is on bit DB1 */
const uint8_t LCD_CNF_SHIFT_SH = 0;


/* Line number configuration */
/*!< One-line configuration, bit is set to 0 */
const bool LCD_CNF_ONE_LINE = false;

/*!< Two-line configuration, bit is set to 1 */
const bool LCD_CNF_TWO_LINE = true;


/* Font type configuration */
/*!< One-line configuration, bit is set to 0 */
const bool LCD_CNF_FONT_5_8 = false;

/*!< Two-line configuration, bit is set to 1 */
const bool LCD_CNF_FONT_5_11 = true;


/* Display configuration */
/*!< Display is on, bit is set to 1 */
const bool LCD_CNF_DISPLAY_ON = true;

/*!< Display is off, bit is set to 0 */
const bool LCD_CNF_DISPLAY_OFF = false;

/* Cursor configuration */
/*!< Cursor is on, bit is set to 1 */
const bool LCD_CNF_CURSOR_ON = true;

/*!< Cursor is off, bit is set to 0 */
const bool LCD_CNF_CURSOR_OFF = false;


/* Cursor blinking configuration */
/*!< Cursor blinking is on, bit is set to 1 */
const bool LCD_CNF_CURSOR_BLINK_ON = true;

/*!< Cursor blinking is off, bit is set to 0 */
const bool LCD_CNF_CURSOR_BLINK_OFF = false;


/* Entry mode direction configuration */
/*!< Direction of shift is right, bit is set to 1 */
const bool LCD_CNF_ENTRY_MODE_DIRECTION_RIGHT = true;

/*!< Direction of shift is left, bit is set to 0 */
const bool LCD_CNF_ENTRY_MODE_DIRECTION_LEFT = false;


/* Entry mode display shift configuration */
/*!< Display shift is performed, bit is set to 1 */
const bool LCD_CNF_ENTRY_MODE_DISPLAY_SHIFT_ON = true;

/*!< Display shift is not performed, bit is set to 0 */
const bool LCD_CNF_ENTRY_MODE_DISPLAY_SHIFT_OFF = false;


/* Backlight configuration */
/*!< Backlight is enabled */
const bool LCD_CNF_BACKLIGHT_ON = true;

/*!< Backlight is disabled */
const bool LCD_CNF_BACKLIGHT_OFF = false;




/*!
 * @brief LCD commands enumeration
 * @details This enumeration defines all command modes available for LCD configuration
 */
typedef enum
{
	LCD_CMD_FUNCTION_SET,
	LCD_CMD_CLEAR_DISPLAY,
	LCD_CMD_DISPLAY_CTRL,
	LCD_CMD_ENTRY_MODE_SET,
	LCD_CMD_SET_DDRAM_ADDR
}
T_LCD_command;

/*!
 * @brief LCD modes enumeration
 * @details This enumeration defines the possible modes for communication with LCD. Two modes are possible, DATA for writing data in RAM and INSTRUCTION for configuring the display
 */
typedef enum
{
	LCD_MODE_INSTRUCTION = 0,
	LCD_MODE_DATA = 1
}
T_LCD_config_mode;

/*!
 * @brief Screen RAM definition
 * @details This enumeration defines the RAM areas of the LCD screen : DDRAM for display, CGRAM for characters generation
 */
typedef enum
{
	LCD_DATA_DDRAM,
	LCD_DATA_CGRAM
}
T_LCD_ram_area;

/*!
 *  @brief Structure defining LCD configuration
 */
typedef struct
{
	uint32_t i2c_bitrate; /*!< I2C bitrate needed by the LCD screen */
	uint8_t i2c_addr; /*!< I2C address if the screen */
	bool backlight_en; /*!< Screen backlight enable flag */
	bool lineNumber_cnf; /*!< Screen line number configuration (1 or 2 lines) */
	bool fontType_cnf; /*!< Font configuration */
	bool display_en; /*!< Screen display enable flag */
	bool cursor_en; /*!< Screen cursor enable flag */
	bool cursorBlink_en; /*!< Screen cursor blinking enable flag */
	bool entryModeDir; /*!< Entry mode direction configuration */
	bool entryModeShift; /*!< Entry mode shift configuration */
}
T_LCD_conf_struct;

/*!
 * @brief Class for LCD S2004A display driver
 * @details This class handles functions managing LCD display S2004a on I2C bus
 */
class LCD : public Service
{
public:

	/*!
	 * @brief LCD class constructor
	 * @details This constructor function initializes the class LCD.
	 *
	 * @return Nothing
	 */
	LCD();

	/*!
	 * @brief LCD driver initialization function
	 * @details This function initializes the driver I2C and initializes I2C driver
	 *
	 * @return
	 */
	virtual bool initService();

	/*!
	 * @brief LCD driver configuration function
	 * @details This function configures the driver with the given configuration structure and calls screen initialization function.
	 *
	 * @param [in] init_conf Initial configuration structure
	 * @return Nothing
	 */
	void configure(const T_LCD_conf_struct *init_conf);

	/*!
	 * @brief LCD command management function
	 * @details This function sends the requested command to the LCD screen. It builds the 8-bit command word and sends it on I2C bus.
	 *
	 * @param [in] cmd Requested command
	 * @return Nothing
	 */
	void command(T_LCD_command cmd);

	/*!
	 * @brief Backlight configuration function
	 * @details This function configures the screen backlight (enable or disable) according to the parameter enable.
	 *
	 * @param [in] enable True if backlight shall be on, False otherwise
	 * @return Nothing
	 */
	inline void ConfigureBacklight(bool enable)
	{
		backlight_enable = enable;
	}

	/*!
	 * @brief Line type configuration function
	 * @details This function configures the line number configuration of the screen (1 or 2 lines mode) according to the parameter.
	 *
	 * @param [in] param Configuration value
	 * @return Nothing
	 */
	inline void ConfigureLineNumber(bool param)
	{
		cnfLineNumber = param;
	}

	/*!
	 * @brief Font configuration function
	 * @details This function configures the font type of the screen (5*8 or 5*11 dots) according to the parameter.
	 *
	 * @param [in] param Configuration value
	 * @return Nothing
	 */
	inline void ConfigureFontType(bool param)
	{
		cnfFontType = param;
	}

	/*!
	 * @brief Display configuration function
	 * @details This function configures the display (on or off mode) according to the parameter.
	 *
	 * @param [in] param Configuration value
	 * @return Nothing
	 */
	inline void ConfigureDisplayOnOff(bool param)
	{
		cnfDisplayOnOff = param;
	}

	/*!
	 * @brief Cursor configuration function
	 * @details This function configures the cursor (on or off mode) according to the parameter.
	 *
	 * @param [in] param Configuration value
	 * @return Nothing
	 */
	inline void ConfigureCursorOnOff(bool param)
	{
		cnfCursorOnOff = param;
	}

	/*!
	 * @brief Cursor blinking configuration function
	 * @details This function configures the cursor blinking (on or off mode) according to the parameter.
	 *
	 * @param [in] param Configuration value
	 * @return Nothing
	 */
	inline void ConfigureCursorBlink(bool param)
	{
		cnfCursorBlink = param;
	}

	/*!
	 * @brief Entry mode direction configuration function
	 * @details This function configures the direction of entry mode (right or left) according to the parameter.
	 *
	 * @param [in] param Configuration value
	 * @return Nothing
	 */
	inline void ConfigureEntryModeDir(bool param)
	{
		cnfEntryModeDir = param;
	}

	/*!
	 * @brief Entry mode shift configuration function
	 * @details This function configures the display shift of entry mode (enable or disable) according to the parameter.
	 *
	 * @param [in] param Configuration value
	 * @return Nothing
	 */
	inline void ConfigureEntryModeShift(bool param)
	{
		cnfEntryModeShift = param;
	}

	/*!
	 * @brief I2C address configuration function
	 * @details This function configures the I2V address of the LCD screen according to the parameter.
	 *
	 * @param [in] param I2C address
	 * @return Nothing
	 */
	inline void ConfigureI2CAddr(uint8_t param)
	{
		cnfI2C_addr = param;
	}

	/*!
	 * @brief DDRAM address setting function
	 * @details This function updates the DDRAM address according to the given parameter. The parameter is checked against limits to be sure the address stays always coherent. It also calls the command function to update screen accordingly.
	 *
	 * @param [in] addr New DDRAM address
	 * @return Nothing
	 */
	void setDDRAMAddress(uint8_t addr);

	/*!
	 * @brief DDRAM address get function
	 * @details This function return the value of the current DDRAM address stored in internal variable ddram_addr.
	 *
	 * @return Current DDRAM address
	 */
	inline uint8_t getDDRAMAddress() const
	{
		return ddram_addr;
	}

	/*!
	 * @brief Screen RAM write function
	 * @details This function writes in the memorized RAM address the character given as parameter. After a write the screen automatically increment/decrement the RAM address, so we do the same in the function to stay coherent.
	 * 			Currently only DDRAM write is implemented.
	 *
	 * 	@param [in] a_char Data byte to write in RAM
	 * 	@param [in] area Area in RAM where the data will be written : DDRAM or CGRAM
	 * 	@return Nothing
	 */
	void writeInRam(uint8_t a_char, T_LCD_ram_area area);

	/*!
	 * @brief Number of line get function
	 * @details This function returns the line number configuration of the screen : 1 or 2 lines mode.
	 *
	 * @return Line number configuration
	 */
	inline bool GetLineNumberCnf() const
	{
		return cnfLineNumber;
	}


private:

	bool backlight_enable; /*!< Backlight enable flag */
	bool cnfLineNumber; /*!< Display line number configuration, 0 = 1-line mode, 1 = 2-line mode */
	bool cnfFontType; /*!< Font type configuration, 0 = 5*8 dots, 1 = 5*11 dots */
	bool cnfDisplayOnOff; /*!< Display configuration : 1 = display on, 0 = display off */
	bool cnfCursorOnOff; /*!< Cursor configuration : 1 = cursor on, 0 = cursor off */
	bool cnfCursorBlink; /*!< Cursor blinking configuration : 1 = cursor blink is on, 0 = cursor blink is off */
	bool cnfEntryModeDir; /*!< Entry mode direction configuration : 1 = cursor moves to right when DDRAM address is incremented, 0 = cursor moves to left when DDRAM address is incremented */
	bool cnfEntryModeShift; /*!< Entry mode configuration : 0 = no display shift is performed after a DDRAM read, 1 = a shift is performed */
	uint8_t cnfI2C_addr; /*!< I2C address of the LCD screen */

	I2C* i2c_drv_ptr; /*!< Pointer to the I2C driver object */

	uint8_t ddram_addr; /*!< Screen DDRAM address */

	/*!
	 * @brief I2C write function for 4-bits mode
	 * @details This function sends the requested 8-bits data on the I2C bus. The backlight pin is also set/clear according to the configuration.
	 * 			The function sends the data a first time with EN pin set, then a second time with EN bit clear.
	 *
	 * 	@param [in] data 8-bit data to send
	 * 	@return Nothing
	 */
	void write4bits(uint8_t data) const;

	/*!
	 * @brief I2C write function
	 * @details This function writes the requested data on I2C bus. It's assumed we only perform write operation so the R/W bit is forced LOW.
	 * 			It's also assumed we work in 4-bit mode, then two calls of write4bits are performed, first with bits 4-7 of data, second with bits 0-3.
	 *
	 * @param [in] data 8-bit data for D0-7 pins of screen
	 * @param [in] mode Requested mode for LCD communication
	 *
	 * @return Nothing
	 */
	void write(uint8_t data, T_LCD_config_mode mode) const;

	/*!
	 * @brief Screen configuration function.
	 * @details This function configures the LCD screen. It's must be called during initialization phase, or the screen won't be usable. The configuration process is described in LCD datasheet J2004A-GFDN-DYNC
	 *
	 * @return Nothing
	 */
	void initializeScreen();
};


#endif /* WORK_BSW_LCD_LCD_H_ */
