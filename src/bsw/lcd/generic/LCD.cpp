/*!
 * @file LCD.cpp
 *
 * @brief LCD class source file
 *
 * @date Tue Apr  5 20:18:20     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:20     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Tue Feb  8 20:24:02     2022 : 59-passage-en-allocation-statique 
 * Fri Jan  7 20:47:57     2022 : 51-optimisation-des-typages 
 * Wed Jan  5 17:01:29     2022 : 50-corrections-sonar 
 * 
 */

#include <util/delay.h>
#include <avr/io.h>

#include "../../../os/service/generic/Service.h"

#include "../../bsw_manager/generic/bsw_manager.h"

#include "LCD.h"


/* Definition of LCD instruction codes bits */
/*!< Instruction bit for "clear display" is DB0 */
const uint8_t LCD_INST_CLR_DISPLAY_BIT = 0;

/*!< Instruction bit for "function set" is DB5 */
const uint8_t LCD_INST_FUNCTION_SET = 5;

/*!< Instruction bit for "display control" is DB3 */
const uint8_t LCD_INST_DISPLAY_CTRL = 3;

/*!< Instruction bit for "entry mode" is DB2 */
const uint8_t LCD_INST_ENTRY_MODE_SET = 2;

/*!< Instruction bit for "set DDRAM address" is DB7 */
const uint8_t LCD_INST_SET_DDRAM_ADDR = 7;


/* Definition of fields for function set command */
/*!< Field DL (data length) of command "function set" is on bit DB4 */
const uint8_t LCD_FCT_SET_FIELD_DL = 4;

/*!< Field N (number of lines) of command "function set" is on bit DB3 */
const uint8_t LCD_FCT_SET_FIELD_N = 3;

/*!< Field F (font type) of command "function set" is on bit DB2 */
const uint8_t LCD_FCT_SET_FIELD_F = 2;

/* Definition of fields for display control command */
/*!< Field D (display on/off) of command "display control" is on bit DB2 */
const uint8_t LCD_DISPLAY_CTRL_FIELD_D = 2;

/*!< Field C (cursor on/off) of command "display control" is on bit DB1 */
const uint8_t LCD_DISPLAY_CTRL_FIELD_C = 1;

/*!< Field B (cursor blink) of command "display control" is on bit DB0 */
const uint8_t LCD_DISPLAY_CTRL_FIELD_B = 0;




LCD::LCD()
{
	backlight_enable = false;
	cnfLineNumber = false;
	cnfFontType = false;
	cnfDisplayOnOff = false;
	cnfCursorOnOff = false;
	cnfCursorBlink = false;
	cnfEntryModeDir = false;
	cnfEntryModeShift = false;
	cnfI2C_addr = 0;
	i2c_drv_ptr = 0;
	ddram_addr = 0;
}

bool LCD::initService()
{
	/* Initialize class variables */
	ddram_addr = 0;

	/* Initializes I2C driver */
	bool retval = p_global_BSW_manager.initializeDriver(I2C_COMM);

	i2c_drv_ptr = (I2C*) p_global_BSW_manager.getDriverPointer(I2C_COMM);

	backlight_enable = false;
	cnfLineNumber = false;
	cnfFontType = false;
	cnfDisplayOnOff = false;
	cnfCursorOnOff = false;
	cnfCursorBlink = false;
	cnfEntryModeDir = false;
	cnfEntryModeShift = false;
	cnfI2C_addr = 0;

	return retval;
}

void LCD::configure(const T_LCD_conf_struct *init_conf)
{
	if (i2c_drv_ptr != 0)
	{
		i2c_drv_ptr->setBitRate(init_conf->i2c_bitrate);

		/* Screen default configuration */
		ConfigureBacklight(init_conf->backlight_en);
		ConfigureLineNumber(init_conf->lineNumber_cnf);
		ConfigureFontType(init_conf->fontType_cnf);
		ConfigureDisplayOnOff(init_conf->display_en);
		ConfigureCursorOnOff(init_conf->cursor_en);
		ConfigureCursorBlink(init_conf->cursorBlink_en);
		ConfigureEntryModeDir(init_conf->entryModeDir);
		ConfigureEntryModeShift(init_conf->entryModeShift);
		ConfigureI2CAddr(init_conf->i2c_addr);

		initializeScreen();
	}
}

void LCD::write4bits(uint8_t data) const
{
	if (i2c_drv_ptr != 0)
	{
		uint8_t dummy;

		/* Configure backlight pin and set EN pin */
		data |= ((uint8_t) backlight_enable << BACKLIGHT_PIN) + (1 << EN_PIN);
		dummy = i2c_drv_ptr->writeByte(data, cnfI2C_addr, true);

		_delay_us(1);

		/* Clear enable pin */
		data &= ~(1 << EN_PIN);
		dummy = i2c_drv_ptr->writeByte(data, cnfI2C_addr, true);

		_delay_us(50);
	}
}

void LCD::write(uint8_t data, T_LCD_config_mode mode) const
{
	uint8_t high = data & 0b11110000;
	uint8_t low = (data << 4) & 0b11110000;

	write4bits(high | (uint8_t) (mode << RS_PIN));
	write4bits(low | (uint8_t) (mode << RS_PIN));
}


void LCD::initializeScreen()
{
	uint8_t data;

	/* Wait for 30ms after power on */
	_delay_us(30000);

	/*
	 * Before screen is configured in 4-bits mode, the function LCD::command cannot be used. Then we build manually the data word and send it with LCD::write4bits function
	 */

	/* Function set, first go in 8-bits mode 3 times, to avoid any screen issue */
	data = (1 << LCD_INST_FUNCTION_SET) + (1 << LCD_FCT_SET_FIELD_DL);
	write4bits(data);

	/* Wait at least 38 us */
	_delay_us(40);

	data = (1 << LCD_INST_FUNCTION_SET) + (1 << LCD_FCT_SET_FIELD_DL);
	write4bits(data);

	/* Wait at least 38 us */
	_delay_us(40);

	data = (1 << LCD_INST_FUNCTION_SET) + (1 << LCD_FCT_SET_FIELD_DL);
	write4bits(data);

	/* Wait at least 38 us */
	_delay_us(40);

	/* Function set, go in 4-bits mode */
	data = (1 << LCD_INST_FUNCTION_SET);
	write4bits(data);

	/* Wait at least 38 us */
	_delay_us(40);

	/* Configure line and matrix */
	command(LCD_CMD_FUNCTION_SET);

	/* Configure display */
	command(LCD_CMD_DISPLAY_CTRL);

	/* Clear display */
	command(LCD_CMD_CLEAR_DISPLAY);

	/* Configure cursor */
	command(LCD_CMD_ENTRY_MODE_SET);
}

void LCD::command(T_LCD_command cmd)
{
	uint8_t data = 0;
	uint16_t wait_time = 0;

	switch(cmd)
	{
	case LCD_CMD_FUNCTION_SET:
		/* Data length bit is forced to 0, we can work only in 4-bits mode */
		data = (uint8_t) (1 << LCD_INST_FUNCTION_SET) + (uint8_t) ((uint8_t) cnfLineNumber << LCD_FCT_SET_FIELD_N)
		        + (uint8_t) ((uint8_t) cnfFontType << LCD_FCT_SET_FIELD_F);
		wait_time = LCD_WAIT_OTHER_MODES;
		break;

	case LCD_CMD_DISPLAY_CTRL:
		data = (uint8_t) (1 << LCD_INST_DISPLAY_CTRL)
		        + (uint8_t) ((uint8_t) cnfDisplayOnOff << LCD_DISPLAY_CTRL_FIELD_D)
		        + (uint8_t) ((uint8_t) cnfCursorOnOff << LCD_DISPLAY_CTRL_FIELD_C)
		        + (uint8_t) ((uint8_t) cnfCursorBlink << LCD_DISPLAY_CTRL_FIELD_B);
		wait_time = LCD_WAIT_OTHER_MODES;
		break;

	case LCD_CMD_CLEAR_DISPLAY:
		data = (uint8_t) (1 << LCD_INST_CLR_DISPLAY_BIT);
		ddram_addr = 0;
		wait_time = LCD_WAIT_CLR_RETURN;
		break;

	case LCD_CMD_ENTRY_MODE_SET:
		data = (uint8_t) (1 << LCD_INST_ENTRY_MODE_SET) + (uint8_t) ((uint8_t) cnfEntryModeDir << LCD_CNF_SHIFT_ID)
		        + (uint8_t) ((uint8_t) cnfEntryModeShift << LCD_CNF_SHIFT_SH);
		wait_time = LCD_WAIT_OTHER_MODES;
		break;

	case LCD_CMD_SET_DDRAM_ADDR:
		data = (uint8_t) (1 << LCD_INST_SET_DDRAM_ADDR) + ddram_addr;
		wait_time = LCD_WAIT_OTHER_MODES;
		break;

	default:
		/* Do nothing */
		break;
	}

	write(data, LCD_MODE_INSTRUCTION);
	_delay_us((double) wait_time);
}

void LCD::setDDRAMAddress(uint8_t addr)
{
	/* If 1-line mode, address shall be between 0 and 0x4F */
	if (cnfLineNumber == LCD_CNF_ONE_LINE)
	{
		if (addr > LCD_RAM_1_LINE_MAX)
			addr = LCD_RAM_1_LINE_MAX;
	}
	/* If 2-lines mode, address shall be between 0 and 0x27 or between 0x40 and 0x67 */
	else if (cnfLineNumber == LCD_CNF_TWO_LINE)
	{
		if ((addr > LCD_RAM_2_LINES_MAX_1) && (addr < LCD_RAM_2_LINES_MIN_2))
			addr = LCD_RAM_2_LINES_MAX_1;
		else if (addr > LCD_RAM_2_LINES_MAX_2)
			addr = LCD_RAM_2_LINES_MAX_2;
	}

	/* Now set the internal variable and update screen */
	ddram_addr = addr;
	command(LCD_CMD_SET_DDRAM_ADDR);
}

void LCD::writeInRam(uint8_t a_char, T_LCD_ram_area area)
{

	/* Only DDRAM is implemented for now */
	if (area == LCD_DATA_DDRAM)
	{
		/* Increment RAM address */
		if (cnfLineNumber == LCD_CNF_ONE_LINE)
			ddram_addr = (ddram_addr + 1) % (LCD_RAM_1_LINE_MAX + 1);

		else if (cnfLineNumber == LCD_CNF_TWO_LINE)
		{
			ddram_addr++;

			if (ddram_addr == (LCD_RAM_2_LINES_MAX_1 + 1))
				ddram_addr = LCD_RAM_2_LINES_MIN_2;

			else if (ddram_addr == (LCD_RAM_2_LINES_MAX_2 + 1))
				ddram_addr = LCD_RAM_2_LINES_MIN_1;
		}
	}

	write(a_char, LCD_MODE_DATA);
	_delay_us(LCD_WAIT_OTHER_MODES);
}
