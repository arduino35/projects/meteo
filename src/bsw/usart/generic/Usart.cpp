/*!
 * @file usart.cpp
 * @brief BSW library for USART
 *
 *  @date Tue Apr  5 20:18:21     2022
 *  @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:21     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Thu Feb 10 21:05:26     2022 : 59-passage-en-allocation-statique 
 * Tue Jan 25 11:39:25     2022 : 57-corrections-bugs-bite 
 * Fri Jan 21 21:39:14     2022 : 48-synchro-host-avec-identification 
 * Fri Jan 21 20:56:39     2022 : 55-changement-chaine-tx-usart 
 * Wed Jan  5 18:34:37     2022 : 50-corrections-sonar 
 * 
 */


#include <avr/io.h>
#include <avr/interrupt.h>

#include "../../../lib/common_lib/generic/common_types.h"

#include "../../../os/service/generic/PeriodicService.h"
#include "../../../asw/time_manager/generic/TimeManager.h"
#include "../../../asw/sensors/sensor/generic/Sensor.h"

#include "cpu_cnf/usart_reg_atm2560.h"
#include "Usart.h"

#include "../../bsw_manager/generic/bsw_manager.h"
#include "../../int/generic/IntManager.h"

#include "../../../asw/asw_manager/generic/asw_manager.h"



#define USART_DEFAULT_BAUDRATE 	BAUD_9600 	/*!< Default USART baudrate setting : 9600 bauds */
#define USART_DEFAULT_DATABITS	DATA_8	/*!< Default USART data bits setting : 8 bits */
#define USART_DEFAULT_PARITY	PARITY_NONE	/*!< Default USART parity setting : None */
#define USART_DEFAULT_STOPBITS	STOP_1	/*!< Default USART stop bits setting : 1 bit */

/* Definition of bits positions */
/*!< Bit used to enable Rx */
const uint8_t RXEN = 4;

/*!< Bit used to enable Tx */
const uint8_t TXEN = 3;

/*!< Bit used to enable Rx complete IT */
const uint8_t RXCIE = 7;

/*!< Bit used to configure stop bits */
const uint8_t USBS = 3;

/*!< Bit used to select data size */
const uint8_t UCSZ0 = 1;

/*!< Bit used to configure parity mode */
const uint8_t UPM0 = 4;

/*!< Bit indicating if data register is empty */
const uint8_t UDRE = 5;

/*!< Bit indicating reception complete */
const uint8_t RXC = 7;

Usart::Usart()
{
	initService(0);
}

bool Usart::initService(uint8_t bus)
{
	/* Configure HW registers to use according to selected bus */
	switch(bus)
	{
	case 0:
	default:
		UBRRxH_ptr = UBRR0H_PTR;
		UBRRxL_ptr = UBRR0L_PTR;
		UCSRxA_ptr = UCSR0A_PTR;
		UCSRxB_ptr = UCSR0B_PTR;
		UCSRxC_ptr = UCSR0C_PTR;
		UDRx_ptr   = UDR0_PTR;
		break;

	case 1:
		UBRRxH_ptr = UBRR1H_PTR;
		UBRRxL_ptr = UBRR1L_PTR;
		UCSRxA_ptr = UCSR1A_PTR;
		UCSRxB_ptr = UCSR1B_PTR;
		UCSRxC_ptr = UCSR1C_PTR;
		UDRx_ptr   = UDR1_PTR;
		break;

	case 2:
		UBRRxH_ptr = UBRR2H_PTR;
		UBRRxL_ptr = UBRR2L_PTR;
		UCSRxA_ptr = UCSR2A_PTR;
		UCSRxB_ptr = UCSR2B_PTR;
		UCSRxC_ptr = UCSR2C_PTR;
		UDRx_ptr   = UDR2_PTR;
		break;

	case 3:
		UBRRxH_ptr = UBRR3H_PTR;
		UBRRxL_ptr = UBRR3L_PTR;
		UCSRxA_ptr = UCSR3A_PTR;
		UCSRxB_ptr = UCSR3B_PTR;
		UCSRxC_ptr = UCSR3C_PTR;
		UDRx_ptr   = UDR3_PTR;
		break;
	}

	/* Initialize reception buffers */
	clearRcvBuffer(0);
	clearRcvBuffer(1);
	bufferIdx = 0;

	/* Configure HW with default values */
	configure(USART_DEFAULT_BAUDRATE, USART_DEFAULT_DATABITS, USART_DEFAULT_PARITY, USART_DEFAULT_STOPBITS);

	/* No callback configured at init */
	callbackType = NO_CALLBACK;
	callbackID = 0;
	callbackFunc = 0;

	return true;
}



void Usart::sendString(String *str)
{
	uint8_t* data;

	/* Check if input string is not empty */
	if(str->getSize() != 0)
	{
		data = str->getString();
		/* Send each character of the string on usart bus */
		for (uint16_t i = 0; i < str->getSize(); i++)
		{
			if(data[i]=='\n')
				transmit('\r');

			transmit(data[i]);
		}
	}
}

void Usart::sendString(const uint8_t *const str)
{
	String strToSend;
	strToSend.appendString(str);
	sendString(&strToSend);
	strToSend.clear();
}

void Usart::sendInteger(uint16_t data, uint8_t base)
{
	/* If the base in not between 2 and 36, 10 is used as default */
	if ((base > 36) || (base < 2))
		base = 10;

	String strToSend;

	strToSend.appendInteger(data, base);

	sendString(&strToSend);
}


void Usart::sendBool(bool data, bool isText)
{
	String str;

	str.appendBool(data, isText);

	sendString(&str);
}


void Usart::sendFrame(const uint8_t *const data, uint8_t size)
{
	for(uint8_t i = 0; i < size; i++)
		transmit(data[i]);
}


Usart* Usart::configure(T_usart_baudrates a_baudrate, T_usart_data_bits a_data_bits, T_usart_parity a_parity,
        T_usart_stop_bits a_stop_bits)
{
	baudrate = a_baudrate;
	uint32_t baud;

	switch (baudrate)
	{
	case BAUD_9600:
	default:
		baud = 9600;
		break;
	case BAUD_19200:
		baud = 19200;
		break;
	case BAUD_38400:
		baud = 38400;
		break;
	case BAUD_57600:
		baud = 57600;
		break;
	case BAUD_115200:
		baud = 115200;
		break;
	}

	uint32_t ui32UBRR = F_CPU;

	ui32UBRR >>= 4;     // (=) to ui32UBRR /= 16 but more efficient
	ui32UBRR /= baud;
	ui32UBRR -= 1;

	/* Set baud rate */
	*UBRRxH_ptr = (uint8_t) ((ui32UBRR >> 8) & 0x000000F);
	*UBRRxL_ptr = (uint8_t) (ui32UBRR & 0x000000FF);

	/* Frame data setting */
	databits = a_data_bits;
	parity = a_parity;
	stopbits = a_stop_bits;

	uint8_t confdata;
	switch (databits)
	{
	case DATA_5:
		confdata = 0;
		break;
	case DATA_6:
		confdata = 1;
		break;
	case DATA_7:
		confdata = 2;
		break;
	case DATA_8:
	default:
		confdata = 3;
		break;
	}

	uint8_t confpar;
	switch (parity)
	{
	case PARITY_NONE:
	default:
		confpar = 0;
		break;
	case PARITY_EVEN:
		confpar = 2;
		break;
	case PARITY_ODD:
		confpar = 3;
		break;
	}

	uint8_t confstop;
	switch (stopbits)
	{
	case STOP_1:
	default:
		confstop = 0;
		break;
	case STOP_2:
		confstop = 1;
		break;
	}

	*UCSRxC_ptr = (uint8_t) (confdata << UCSZ0) | (uint8_t) (confpar << UPM0) | (uint8_t) (confstop << USBS);

	/* Enable receiver and transmitter
	 * Enable Rx complete interrupt    */
	*UCSRxB_ptr = (1 << RXEN) | (1 << TXEN) | (1 << RXCIE);

	return this;
}



void Usart::transmit(uint8_t data)
{
	/* Wait for empty transmit buffer */
	while ( !( *UCSRxA_ptr & (1<<UDRE)) ) ;

	/* Put data into buffer, sends the data */
	*UDRx_ptr = data;
}

void Usart::sendByte(uint8_t data)
{
	transmit(data);
}

uint8_t Usart::read() const
{
	/* Wait for data to be received */
	while ( !(*UCSRxA_ptr & (1<<RXC)) );

	/* Get and return received data from buffer */
	return (*UDRx_ptr);
}


void Usart::receptionISRHandler()
{
	buffer[bufferIdx].buffer[buffer[bufferIdx].size] = read();
	buffer[bufferIdx].size++;

	/* If buffer is full, call callback to treat it and switch buffer to avoid any overflow */
	if (buffer[bufferIdx].size > USART_CNF_BUFFER_SIZE - 10)
	{
		callback();
		clearRcvBuffer(bufferIdx);
	}

}

void Usart::callback() const
{
	switch(callbackType)
	{
	case SW_INT:
		((IntManager*) (p_global_BSW_manager.getDriverPointer(INT_MGT)))->triggerSWInt(callbackID);
		break;

	case DIRECT:
		callbackFunc();
		break;

	default:
		break;
	}
}



Usart* Usart::setCallback(T_Usart_Callback_type type, uint8_t cfg, ASWReceptionHandler func)
{
	callbackType = type;

	switch (type)
	{
	case SW_INT:
		callbackID = ((IntManager*) (p_global_BSW_manager.getDriverPointer(INT_MGT)))->createNewInt(SOFTWARE,
		        ENCODE_INTX_CNF(INT_MGT_SENSECTL_INTX_ANY_EDGE, cfg));
		break;
	case DIRECT:
		callbackFunc = func;
		break;
	default:
		break;
	}

	return this;
}



T_usart_reception_buffer* Usart::getReceptionBuffer()
{
	uint8_t oldIdx = bufferIdx;

	/* Switch buffer in use */
	bufferIdx = (bufferIdx + 1) % 2;
	clearRcvBuffer(bufferIdx);

	return &(buffer[oldIdx]);
}

void Usart::clearRcvBuffer(uint8_t buf)
{
	if ((buf != 0) && (buf != 1))
		return;

	buffer[buf].size = 0;
}
