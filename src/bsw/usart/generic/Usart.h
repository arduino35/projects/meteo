/*!
 * @file usart.h
 * @brief Header file for USART library
 *
 * @date Tue Apr  5 20:18:22     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Tue Apr  5 20:18:22     2022 : 64-suppression-des-include-dans-les-fichiers-h 
 * Thu Feb 10 10:48:57     2022 : 59-passage-en-allocation-statique 
 * Mon Jan 24 23:44:45     2022 : 57-corrections-bugs-bite 
 * Fri Jan 21 21:39:14     2022 : 48-synchro-host-avec-identification 
 * Fri Jan 21 20:56:39     2022 : 55-changement-chaine-tx-usart 
 * Wed Jan  5 18:34:38     2022 : 50-corrections-sonar 
 * 
 */

#ifndef WORK_BSW_USART_USART_H_
#define WORK_BSW_USART_USART_H_

#include "../config/usart_cnf.h"
#include "../../../lib/string/generic/String.h"

/*!
 * @brief Available baudrates enumeration
 */
typedef enum
{
	BAUD_9600,
	BAUD_19200,
	BAUD_38400,
	BAUD_57600,
	BAUD_115200
} T_usart_baudrates;

/*!
 * @brief Available data bits settings
 */
typedef enum
{
	DATA_5,
	DATA_6,
	DATA_7,
	DATA_8
} T_usart_data_bits;

/*!
 * @brief Available parity settings
 */
typedef enum
{
	PARITY_NONE,
	PARITY_EVEN,
	PARITY_ODD
} T_usart_parity;

/*!
 * @brief Available stop bits settings
 */
typedef enum
{
	STOP_1,
	STOP_2
} T_usart_stop_bits;

/*!
 * @brief Type defining a pointer to the ASW reception handler
 */
typedef void (*ASWReceptionHandler)(void);

/*!
 * @brief Type defining received frame structure
 *
 */
typedef struct
{
	uint8_t buffer[USART_CNF_BUFFER_SIZE]; /*!< Pointer to reception buffer */
	uint8_t size; /*!< Size of the buffer */
} T_usart_reception_buffer;

/*!
 * @brief Enumeration defining the callback type
 */
typedef enum
{
	NO_CALLBACK,
	SW_INT,
	DIRECT
} T_Usart_Callback_type;

/*!
 * @brief USART serial bus class
 * @details This class defines all useful functions for USART serial bus
 */
class Usart : public Service
{
public:

	/*!
	 * @brief Class USART constructor
	 * @details Initializes the class.
	 *
	 * @return Nothing.
	 */
	Usart();

	/*!
	 * @brief Driver initialization function
	 * @details This function initializes the USART driver. It calls hardware initialization function. The USART bus to use and the requested baudrate shall be passed in parameter.
	 *
	 * @param [in] bus Index of USART bus to configure
	 * @return Always TRUE
	 */
	bool initService(uint8_t bus);

	/*! @brief Send a string on USART link
	 *  @details This function writes the string object data to the serial link using transmit function
	 *  @param [in] str Pointer to the string being sent
	 *  @return Nothing.
	 */
	void sendString(String *str);

	/*! @brief Send a chain of characters on USART link
	 *  @details This function writes the given chain of characters to the serial link using sendString function
	 *  @param [in] str Pointer to the chain to send
	 *  @return Nothing.
	 */
	void sendString(const uint8_t *const str);

	/*!
	 * @brief Send a integer data on USART link
	 * @details This function sends the requested integer on USART link by calling sendString function.
	 * 			The integer is first converted into a string and then sent
	 * @param [in] data integer data to be sent
	 * @param [in] base numerical base used to convert integer into string (between 2 and 36)
	 * @return Nothing
	 */
	void sendInteger(uint16_t data, uint8_t base);

	/*!
	* @brief Send a boolean data on USART link
	* @details 	This function sends the requested boolean on USART link by calling sendString function.
	* 			The boolean data is first converted into a string and then sent. The parameter isText defines if the data is converted into a string (true/false) or an integer (1/0).
	* @param [in] data boolean data to be sent
	* @param [in] isText String conversion configuration
	* @return Nothing
	*/
	void sendBool(bool data, bool isText);

	/*! @brief Send a single byte on USART link
	 *  @details This function writes the given byte to the serial link using usart_transmit function
	 *  @param [in] data Data byte being sent
	 *  @return Nothing.
	 */
	void sendByte(uint8_t data);

	/*! @brief Send multiple bytes on USART link
	 *  @details This function writes the given frame to the serial link using usart_transmit function.
	 *  		 It is the responsibility of the calling function to allocate correctly the data frame
	 *  		 and deallocate it after the transmission.
	 *  @param [in] data Pointer to the data frame to send
	 *  @param [in] size Size of the data frame to send
	 *  @return Nothing.
	 */
	void sendFrame(const uint8_t *const data, uint8_t size);

	/*!
	 * @brief Setting baud rate, parity and frame format
	 * @details This function configures the USART hardware according to the given parameters : baudrate, parity, stop bits and data bits
	 *
	 * @param [in] a_BaudRate Desired Baud Rate
	 * @param [in] a_data_bits Desired data bits setting
	 * @param [in] a_parity Desired parity setting
	 * @param [in] a_stop_bits Desired stop bits setting
	 *
	 * @return Pointer to the USART driver object (this)
	 */
	Usart* configure(T_usart_baudrates a_baudRate, T_usart_data_bits a_data_bits, T_usart_parity a_parity,
	        T_usart_stop_bits a_stop_bits);


	/*!
	 * @brief USART reception interrupt handler
	 * @details This function handles the reception ISR of USART bus.
	 * 			It stores the received character into the reception buffer.
	 *
	 * @return Nothing
	 */
	void receptionISRHandler();


	/*!
	 * @brief Callback configuration function
	 * @details This function is used to configure the software callback when the reception buffer is full.
	 * 			The callback can be either a direct call to a function or a software interrupt.
	 *
	 * @param [in] type Type of callback
	 * @param [in] cfg Callback configuration (INTx pin to use for SW interrupt or ID of function to use in case of direct callback)
	 *
	 * @return Pointer to the USART driver object (this)
	 */
	Usart* setCallback(T_Usart_Callback_type type, uint8_t cfg = 0, ASWReceptionHandler func = 0);

	/*!
	 * @brief Received string get function
	 * @details This function returns a pointer to the received string buffer and creates a new buffer for driver usage.
	 * 			The calling function shall unallocate the old buffer after use.
	 *
	 * @return Pointer to the reception buffer
	 */
	T_usart_reception_buffer* getReceptionBuffer();



private:

	/*! @brief USART Transmit data
	 *  @details Nothing Special. It just wait for the transmit buffer is empty before writing it again.
	 *  @param [in] Data Desired data char to transmit
	 *  @return Nothing.
	 */
	void transmit(uint8_t data);


	/*! @brief USART read function
	 *  @details This function will read reception register of USART
	 *  @return The function returns the 8 bits read from reception buffer
	 */
	uint8_t read() const;

	/*!
	 * @brief Callback function
	 * @details This function calls the configured callback interface (either a SW interrupt or a direct function call).
	 *
	 * @return Nothing
	 */
	void callback() const;

	/*!
	 * @brief Reception buffer clearing function
	 * @details This function clears the requested reception buffer
	 *
	 * @param [in] buf Buffer id (must be 0 or 1)
	 * @return Nothing
	 */
	void clearRcvBuffer(uint8_t buf);


	T_usart_baudrates baudrate; /*!< Defines the baud rate used by driver */
	T_usart_data_bits databits; /*!< Defines the data bit number used by driver */
	T_usart_parity parity; /*!< Defines the parity mode used by driver */
	T_usart_stop_bits stopbits; /*!< Defines the stop bit number used by driver */


	T_Usart_Callback_type callbackType;		/*!< Callback configuration */
	uint8_t callbackID;						/*!< Callback ID */
	ASWReceptionHandler callbackFunc; /*!< Callback function in case of direct call */

	T_usart_reception_buffer buffer[2]; /*!< Reception buffers */
	uint8_t bufferIdx; /*!< Index of currently used reception buffer */

	/* Pointer to HW registers of the selected bus */
	volatile uint8_t* UBRRxH_ptr;	/*!< Pointer to UBRR high register */
	volatile uint8_t* UBRRxL_ptr;	/*!< Pointer to UBRR low register */
	volatile uint8_t* UCSRxA_ptr;	/*!< Pointer to UCSR A register */
	volatile uint8_t* UCSRxB_ptr;	/*!< Pointer to UCSR B register */
	volatile uint8_t* UCSRxC_ptr;	/*!< Pointer to UCSR C register */
	volatile uint8_t* UDRx_ptr;		/*!< Pointer to UDR register */

};

#endif /* WORK_BSW_USART_USART_H_ */
