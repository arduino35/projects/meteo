/*!
 * @file usart_reg_atm2560.h
 *
 * @brief Defines USART register addresses for ATMEGA2560
 *
 * @date Thu Feb 10 09:09:42     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Thu Feb 10 09:09:42     2022 : 59-passage-en-allocation-statique 
 * 
 */

#ifndef WORK_BSW_USART_GENERIC_CPU_CNF_USART_REG_ATM2560_H_
#define WORK_BSW_USART_GENERIC_CPU_CNF_USART_REG_ATM2560_H_

#define UBRR0H_PTR (volatile uint8_t *)(0xC5) /*!< Macro defining pointer to UBRR0H register */
#define UBRR0L_PTR (volatile uint8_t *)(0xC4) /*!< Macro defining pointer to UBRR0L register */

#define UBRR1H_PTR (volatile uint8_t *)(0xCD) /*!< Macro defining pointer to UBRR1H register */
#define UBRR1L_PTR (volatile uint8_t *)(0xCC) /*!< Macro defining pointer to UBRR1L register */

#define UBRR2H_PTR (volatile uint8_t *)(0xD5) /*!< Macro defining pointer to UBRR2H register */
#define UBRR2L_PTR (volatile uint8_t *)(0xD4) /*!< Macro defining pointer to UBRR2L register */

#define UBRR3H_PTR (volatile uint8_t *)(0x135) /*!< Macro defining pointer to UBRR3H register */
#define UBRR3L_PTR (volatile uint8_t *)(0x134) /*!< Macro defining pointer to UBRR3L register */

#define UCSR0A_PTR (volatile uint8_t *)(0xC0) /*!< Macro defining pointer to UCSR0A register */
#define UCSR0B_PTR (volatile uint8_t *)(0xC1) /*!< Macro defining pointer to UCSR0B register */
#define UCSR0C_PTR (volatile uint8_t *)(0xC2) /*!< Macro defining pointer to UCSR0C register */

#define UCSR1A_PTR (volatile uint8_t *)(0xC8) /*!< Macro defining pointer to UCSR1A register */
#define UCSR1B_PTR (volatile uint8_t *)(0xC9) /*!< Macro defining pointer to UCSR1B register */
#define UCSR1C_PTR (volatile uint8_t *)(0xCA) /*!< Macro defining pointer to UCSR1C register */

#define UCSR2A_PTR (volatile uint8_t *)(0xD0) /*!< Macro defining pointer to UCSR2A register */
#define UCSR2B_PTR (volatile uint8_t *)(0xD1) /*!< Macro defining pointer to UCSR2B register */
#define UCSR2C_PTR (volatile uint8_t *)(0xD2) /*!< Macro defining pointer to UCSR2C register */

#define UCSR3A_PTR (volatile uint8_t *)(0x130) /*!< Macro defining pointer to UCSR3A register */
#define UCSR3B_PTR (volatile uint8_t *)(0x131) /*!< Macro defining pointer to UCSR3B register */
#define UCSR3C_PTR (volatile uint8_t *)(0x132) /*!< Macro defining pointer to UCSR3C register */

#define UDR0_PTR (volatile uint8_t *)(0xC6) /*!< Macro defining pointer to UDR0 register */
#define UDR1_PTR (volatile uint8_t *)(0xCE) /*!< Macro defining pointer to UDR1 register */
#define UDR2_PTR (volatile uint8_t *)(0xD6) /*!< Macro defining pointer to UDR2 register */
#define UDR3_PTR (volatile uint8_t *)(0x136) /*!< Macro defining pointer to UDR3 register */

#endif /* WORK_BSW_USART_GENERIC_CPU_CNF_USART_REG_ATM2560_H_ */
