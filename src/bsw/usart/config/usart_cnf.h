/*!
 * @file usart_cnf.h
 *
 * @brief USART driver configuration file
 *
 * @date Thu Feb 10 10:48:56     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Thu Feb 10 10:48:57     2022 : 59-passage-en-allocation-statique 
 * Mon Jan 24 23:44:45     2022 : 57-corrections-bugs-bite
 * Fri Jan 21 21:39:14     2022 : 48-synchro-host-avec-identification
 * Fri Jan 21 20:56:39     2022 : 55-changement-chaine-tx-usart
 * Wed Dec 22 21:22:30     2021 : 50-corrections-sonar
 * 
 */

#ifndef WORK_BSW_USART_CONFIG_USART_CNF_H_
#define WORK_BSW_USART_CONFIG_USART_CNF_H_


/*!< Size of reception buffer */
const uint8_t USART_CNF_BUFFER_SIZE = 127;


#endif /* WORK_BSW_USART_CONFIG_USART_CNF_H_ */
