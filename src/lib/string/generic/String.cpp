/*!
 * @file String.cpp
 *
 * @brief String class source file
 *
 * @date Wed Feb  9 20:42:30     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Wed Feb  9 20:42:30     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 18:34:38     2022 : 50-corrections-sonar 
 * 
 */


#include <stdlib.h>
#include <avr/io.h>

#include "../../common_lib/generic/mem_tools.h"
#include "String.h"


String::String()
{
	initString();
}

void String::initString()
{
	string[0] = '\0';
	size = 0;
}

void String::initString(const uint8_t *str)
{
	/* Compute size of the given string */
	size = computeStringSize(str);

	if (size <= MAX_STRING_SIZE)
	{
		mem_copy(str, string, size);
		string[size] = '\0';
	}
	else
		initString();

}


uint8_t String::computeStringSize(const uint8_t *str) const
{
	uint8_t l_size = 0;

	while ((*str != '\0') && (l_size < MAX_STRING_SIZE + 1))
	{
		str++;
		l_size++;
	}

	return l_size;
}

void String::appendString(const uint8_t *const str)
{
	uint8_t new_size;

	/* Compute size of the new string */
	new_size = computeStringSize(str);

	/* If the new string is not too long */
	if (size + new_size <= MAX_STRING_SIZE)
	{
		/* Copy the new string after the old one */
		mem_copy(str, &(string[size]), new_size);

		/* Update string size and add \0 at the end of the string */
		size += new_size;
		string[size] = '\0';
	}
}

void String::appendInteger(uint16_t value, uint8_t base, uint8_t digits)
{
	uint8_t int_str[8];

	/* If the base in not between 2 and 36, 10 is used as default */
	if ((base > 36) || (base < 2))
		base = 10;

	/* Find how many characters will be used by itoa function */
	uint8_t itoa_cnt = 1;
	if (digits > 0)
	{
		uint16_t value_tmp = value;
		while (value_tmp > (uint8_t) (base - 1))
		{
			value_tmp /= base;
			itoa_cnt++;
		}
	}

	/* There must be less than 8 digits */
	if (digits <= 8)
	{
		/* If requested digits count is higher than itoa count */
		if (digits > itoa_cnt)
		{
			for (uint8_t i = 0; i < (digits - itoa_cnt); i++)
				appendChar((uint8_t) '0');
		}

		/* First convert the integer value into a chain of characters */
		const uint8_t *new_str = (uint8_t*) itoa(value, (char*) int_str, base);

		/* Add the new characters to the string */
		appendString(new_str);
	}
}

void String::clear()
{
	initString();
}

void String::appendBool(bool data, bool isText)
{
	if(isText)
	{
		if(data)
			appendString((uint8_t*)"true");
		else
			appendString((uint8_t*)"false");
	}
	else
	{
		if(data)
			appendInteger(1,2);
		else
			appendInteger(0,2);
	}
}

void String::appendChar(uint8_t data)
{
	uint8_t char_str[2];

	/* First convert the new character into a chain of characters */
	char_str[0] = data;
	char_str[1] = '\0';

	/* Add the new characters to the string */
	appendString(char_str);
}

uint8_t String::getCharAtPos(uint8_t position) const
{
	if (position < size)
		return string[position];
	else
		return (uint8_t)'\0';
}

void String::setCharAtPos(uint8_t charToSet, uint8_t pos)
{
	if (pos < size)
		string[pos] = charToSet;
}
