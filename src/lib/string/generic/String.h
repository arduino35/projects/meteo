/*!
 * @file String.h
 *
 * @brief String class header file
 *
 * @date Fri Apr  1 17:20:48     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri Apr  1 17:20:49     2022 : 59-passage-en-allocation-statique 
 * Wed Jan  5 17:01:33     2022 : 50-corrections-sonar 
 * 
 */

#ifndef WORK_LIB_STRING_STRING_H_
#define WORK_LIB_STRING_STRING_H_

/*!< Maximum size of a string */
const uint8_t MAX_STRING_SIZE = 50;

/*!
 * @brief String management class
 * @details This class defines string object. It implements some functions to manage chains of characters.
 * 			The string is limited to 50 characters. It must finish by the character '\0'.
 */
class String {

public:

	/*!
	 * @brief Class constructor
	 * @details This function initializes the class.
	 *
	 * @return Nothing
	 */
	String();

	/*!
	 * @brief String initialization function
	 * @details This function initializes the string with an empty string. The size is set to 0.
	 *
	 * @return Nothing
	 */
	void initString();

	/*!
	 * @brief String initialization function
	 * @details This function initializes the string with the char table given in parameter.
	 *
	 * @param [in] str String content
	 *
	 * @return Nothing
	 */
	void initString(const uint8_t *str);

	/*!
	 * @brief String pointer get function
	 * @details This function returns the pointer to the beginning of the string.
	 *
	 * @return String pointer
	 */
	inline uint8_t* getString()
	{
		return string;
	}

	/*!
	 * @brief Size get function
	 * @details This function returns the size of the string.
	 *
	 * @return Size of the string
	 */
	inline uint8_t getSize() const
	{
		return size;
	}

	/*!
	 * @brief String adding function
	 * @details This functions adds the given string at the end of the main string. The string size is updated accordingly.
	 *
	 * @param [in] str New string to add
	 * @return Nothing
	 */
	void appendString(const uint8_t *const str);

	/*!
	 * @brief Integer adding function
	 * @details This functions adds the given integer at the end of the main string. The string size is updated accordingly.
	 * 			The integer parameter is first converted into a chain of character according to the base and then added to the string.
	 *
	 * @param [in] value Integer to add
	 * @param [in] base Base of computation of the integer (between 2 and 36)
	 * @param [in] digits Optional parameters : number of digits used to display the integer. Leading zeros are added to match the digits count.
	 *                    If no digits value is given, no leading zeros will be added.
	 * @return Nothing
	 */
	void appendInteger(uint16_t value, uint8_t base, uint8_t digits = 0);

	/*!
	 * @brief Boolean adding function
	 * @details This functions adds the given boolean data at the end of the main string. The string size is updated accordingly.
	 * 			According to the input parameter isText, the boolean parameter is converted into a string (true/false) or an integer (0/1).
	 * @param [in] data Boolean data to add
	 * @param [in] isText Defines the conversion mode : text or integer
	 *
	 * @return Nothing
	 */
	void appendBool(bool data, bool isText);

	/*!
	 * @brief Character adding function
	 * @details This functions adds the given character at the end of the main string. The string size is updated by 1.
	 *
	 * @param [in] data 1-byte character to add
	 * @return Nothing
	 */
	void appendChar(uint8_t data);

	/*!
	 * @brief String clear function
	 * @details This function clears the string. Size is set to 0.
	 *
	 * @return Nothing
	 */
	void clear();

	/*!
	 * @brief Space adding function
	 * @details This function adds a space at the end of the string. It only calls appendChar function.
	 *
	 * @return Nothing
	 */
	inline void appendSpace()
	{
		appendChar((uint8_t)' ');
	}

	/*!
	 * @brief Character get function
	 * @details This function returns the character existing at the given position.
	 * 			If the given position is higher than the string size, the character \0 is returned.
	 *
	 * @param [in] position Position of the character to return
	 * @return Character at the given position.
	 */
	uint8_t getCharAtPos(uint8_t position) const;

	/*!
	 * @brief Character setting function
	 * @details This function sets the character at the requested position of the string to the requested value.
	 * 			If the position is out of range, nothing is done.
	 *
	 * @param [in] charToSet Character value to set
	 * @param [in] pos Position of the character to set
	 *
	 * @return Nothing
	 */
	void setCharAtPos(uint8_t charToSet, uint8_t pos);


private:

	uint8_t string[MAX_STRING_SIZE + 1]; /*!< Table containing the string */
	uint8_t size; /*!< Size of the string (the '\0' at the end of the string is not taken into account */

	/*!
	 * @brief String size computation function
	 * @details This function computes the sizes of the given string. It counts the number of character between the start of the string given in parameter and the next \0 character.
	 *
	 * @param [in] str Pointer to the beginning of the string
	 * @return Number of character of the string (the \0 is excluded)
	 */
	uint8_t computeStringSize(const uint8_t *str) const;
};

#endif /* WORK_LIB_STRING_STRING_H_ */
