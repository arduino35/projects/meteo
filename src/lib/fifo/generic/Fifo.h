/*!
 * @file Fifo.h
 *
 * @brief Fifo class header file
 *
 * @date Thu May 26 23:16:35     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Thu May 26 23:16:35     2022 : 73-implementation-heritage-pour-fifo 
 * Thu Feb 10 20:25:15     2022 : 59-passage-en-allocation-statique 
 * Fri Feb  4 08:50:53     2022 : 57-corrections-bugs-bite 
 * 
 */

#ifndef SRC_LIB_FIFO_GENERIC_FIFO_H_
#define SRC_LIB_FIFO_GENERIC_FIFO_H_

/*!
 * @brief Enumeration defining FIFO statuses
 *
 */
typedef enum
{
	FIFO_OK, /*!< FIFO_OK */
	FIFO_EMPTY,/*!< FIFO_EMPTY */
	FIFO_FULL /*!< FIFO_FULL */
} T_Fifo_Status;

/*!
 * @brief FIFO memory implementation
 * @details This class implements a Fist-In First-Out memory. The data are manipulated through their pointers avoiding long copy delays and limiting memory usage.
 *
 */
class Fifo
{
public:

	/*!
	 * @brief Class constructor
	 * @details This function initializes the class
	 *
	 * @return Nothing
	 *
	 */
	Fifo();

	/*!
	 * @brief FIFO initialization function
	 * @details This function initializes the FIFO with the requested size
	 *
	 * @param size [in] Size of the FIFO
	 * @return Nothing
	 *
	 */
	void initFifo(uint8_t a_size);

	/*!
	 * @brief FIFO cleaning function
	 * @details Cleans the FIFO, all pointers are reset and status is set to CLEAN
	 *
	 * @return New FIFO status (CLEAN)
	 */
	T_Fifo_Status clean();

protected:
	/*!
	 * @brief Adds a new element in the FIFO
	 * @details An element is added in the FIFO.
	 *          If the FIFO was not full before the adding, the returned status is FIFO_OK,
	 *          else the returned status is FIFO_FULL and the element can not be added.
	 *          After the operation, the new status is computed.
	 *
	 * @param idx [out] Index of the new element to add
	 * @return FIFO status
	 */
	T_Fifo_Status addNewElementIndex(uint8_t *idx);

	/*!
	 * @brief Read element from the FIFO
	 * @details This function computes the index of the element to read if the status is not FIFO_EMPTY.
	 * 			If the status was FIFO_EMPTY, no read operation can be done.
	 * 			After the operation, the new status is computed.
	 *
	 * @param idx [out] Index of the element to read
	 * @return FIFO status
	 */
	T_Fifo_Status readElementIndex(uint8_t *idx);

private:
	uint8_t write_idx; /*!< Index of the next write position */
	uint8_t read_idx; /*!< Index of the next read position */

	uint8_t size; /*!< FIFO size */

	T_Fifo_Status status; /*!< Current status of the FIFO */
};

#endif /* SRC_LIB_FIFO_GENERIC_FIFO_H_ */
