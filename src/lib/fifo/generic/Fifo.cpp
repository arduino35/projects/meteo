/*!
 * @file Fifo.cpp
 *
 * @brief Fifo class source code file
 *
 * @date Thu May 26 23:16:35     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Thu May 26 23:16:35     2022 : 73-implementation-heritage-pour-fifo 
 * Thu Feb 10 20:25:15     2022 : 59-passage-en-allocation-statique 
 * Fri Feb  4 08:50:53     2022 : 57-corrections-bugs-bite 
 * Wed Dec 22 21:22:34     2021 : 50-corrections-sonar 
 * 
 */

#include <avr/io.h>

#include "Fifo.h"



Fifo::Fifo()
{
	size = 10;
	clean();
}

void Fifo::initFifo(uint8_t a_size)
{
	size = a_size;
	clean();
}

T_Fifo_Status Fifo::clean()
{
	write_idx = 0;
	read_idx = 0;
	status = FIFO_EMPTY;

	return status;
}

T_Fifo_Status Fifo::addNewElementIndex(uint8_t *idx)
{
	if (status != FIFO_FULL)
	{
		*idx = write_idx;
		write_idx = (write_idx + 1) % size;

		if (write_idx == read_idx)
			status = FIFO_FULL;
		else
			status = FIFO_OK;

		return FIFO_OK;
	}
	else
		return FIFO_FULL;
}

T_Fifo_Status Fifo::readElementIndex(uint8_t *idx)
{
	if (status != FIFO_EMPTY)
	{
		*idx = read_idx;
		read_idx = (read_idx + 1) % size;

		if (write_idx == read_idx)
			status = FIFO_EMPTY;
		else
			status = FIFO_OK;

		return FIFO_OK;
	}
	else
		return FIFO_EMPTY;
}
