/*!
 * @file mem_tools.cpp
 *
 * @brief Memory management tools
 *
 * @date Thu May 26 16:57:49     2022�vr. 2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Thu May 26 16:57:50     2022 : 71-changement-periode-de-l-autoswitch-de-l-affichage 
 * Tue Feb  8 16:11:14     2022 : 59-passage-en-allocation-statique 
 * 
 */


#include <stdlib.h>
#include <avr/io.h>

void mem_copy(const uint8_t *source, uint8_t *dest, const uint8_t size)
{

	for (uint8_t i = 0; i < size; i++)
	{
		*dest = *source;
		dest++;
		source++;
	}
}

void operator delete(void *ptr)
{
	free(ptr);
}

void invertEndianness(const uint8_t *data_in, uint8_t *data_out, uint8_t size)
{
	for (uint8_t i = 0; i < size - 1; i++)
		data_out[i] = data_in[3 - i];
}
