/*!
 * @file mem_tools.h
 *
 * @brief Memory management tools header file
 *
 * @date Thu May 26 16:57:50     2022�vr. 2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Thu May 26 16:57:50     2022 : 71-changement-periode-de-l-autoswitch-de-l-affichage 
 * Tue Feb  8 16:11:14     2022 : 59-passage-en-allocation-statique 
 * 
 */

#ifndef SRC_LIB_COMMON_LIB_GENERIC_MEM_TOOLS_H_
#define SRC_LIB_COMMON_LIB_GENERIC_MEM_TOOLS_H_

/*!
 * @brief Operator delete (DO NOT USE !!!)
 * @details Equivalent to free function in C
 *          Free the memory zone at address ptr and
 *          if memory monitoring is enabled, unregister the memory zone.
 *          Only calls function mem_free().
 *
 *          Do not use : only defined for compilation purpose
 *
 * @param [in] ptr Pointer to the start of memory zone to free
 * @return Nothing
 */
void operator delete(void *ptr);

/*!
 * @brief Memory copy function
 * @details This function copies the given amount of bytes from the given source to the given destination.
 * 			It is the responsibility of the calling function to allocate enough space in the destination zone.
 *
 * @param [in] source Pointer to the first byte to copy
 * @param [out] dest Pointer to the destination
 * @param [in] size Number of bytes to copy
 *
 * @return Nothing
 */
void mem_copy(const uint8_t *source, uint8_t *dest, const uint8_t size);

/*!
 * @brief Endianness invertion function
 * @details This function inverts the endianess of the given data.
 *
 * @param [in] data_in Pointer to the input data
 * @param [out] data_out Pointer to the output data
 * @param [in] size Size of the data in bytes
 *
 * @return Nothing
 */
void invertEndianness(const uint8_t *data_in, uint8_t *data_out, uint8_t size);

#endif /* SRC_LIB_COMMON_LIB_GENERIC_MEM_TOOLS_H_ */
