/*!
 * @file common_types.h
 *
 * @brief Definition of project's common types
 *
 * @date Fri May  6 17:16:06     2022
 * @author Nicolas SIMON
 * 
 *********************** 
 * File history : 
 * Fri May  6 17:16:06     2022 : 67-rendre-generique-affichage-ecran 
 * Wed Jan  5 20:57:48     2022 : 51-optimisation-des-typages 
 * 
 */

#ifndef SRC_LIB_COMMON_LIB_GENERIC_COMMON_TYPES_H_
#define SRC_LIB_COMMON_LIB_GENERIC_COMMON_TYPES_H_

#include <avr/io.h>

/*!< Type for services period */
typedef uint16_t t_time_ms;

/*!
 * @brief Primary data type, used for display, bite...
 *
 */
typedef enum
{
	DATA_TEXT, /*!< Standard text data */
	DATA_SENSORS,/*!< Sensors data */
	DATA_TIME /*!< Date and time data */
} T_DataPrimaryType;


#endif /* SRC_LIB_COMMON_LIB_GENERIC_COMMON_TYPES_H_ */
